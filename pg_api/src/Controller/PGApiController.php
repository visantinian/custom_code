<?php

namespace Drupal\pg_api\Controller;

use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\nodeviewcount\NodeViewCountRecordsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Class PGApiController.
 */
class PGApiController extends ControllerBase {

  /**
   * Entity Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Node view counter manager.
   *
   * @var \Drupal\nodeviewcount\NodeViewCountRecordsManager
   */
  protected $nodeViewCountManager;

  /**
   * Constructs a new RMApiPackage object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, NodeViewCountRecordsManager $node_view_count) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeViewCountManager = $node_view_count;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('nodeviewcount.records_manager')
    );
  }

  /**
   * Get node view count.
   */
  public function getNodeViewCount(Request $request) {

    $nid = $request->get('nid');

    // @var \Drupal\node\NodeStorage $node_storage
    $node = $this->entityTypeManager->getStorage('node')->load($nid);

    if (!$node instanceof Node) {
      throw new \Exception("Data not found.");
    }

    $node_view_count = $this->nodeViewCountManager->getNodeViewsCount($node);
    $view_count = $node_view_count[0]->expression == 0 ? 1 : $node_view_count[0]->expression;

    try {
      $response = [
        'view_count' => $view_count,
      ];
    } catch (Exception $e) {
      $response = [
        'status' => 'Error',
        'message' => $e->getMessage(),
      ];
    }

    return new JsonResponse($response);
  }

}
