<?php

namespace Drupal\pg_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class GTranslateService. Google translate service provider.
 */
class GTranslateService {

  use StringTranslationTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * NUKafkaService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Messenger\Messenger $messenger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\numeris_kafka\NUKafkaService
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * @param string $source
   * @param string $target
   * @param string|array $text
   * @param int $attempts
   *
   * @return string|array With the translation of the text in the target
   *   language
   */
  public function translate($source, $target, $text, $attempts = 5) {
    // Request translation
    if (is_array($text)) {
      // Array
      $translation = $this->requestTranslationArray($source, $target, $text, $attempts = 5);
    }
    else {
      // Single
      $translation = $this->requestTranslation($source, $target, $text, $attempts = 5);
    }

    return $translation;
  }

  /**
   * @param string $source
   * @param string $target
   * @param array $text
   * @param int $attempts
   *
   * @return array
   */
  protected function requestTranslationArray($source, $target, $text, $attempts) {
    $arr = [];
    foreach ($text as $value) {
      // timeout 0.5 sec
      usleep(2000000);
      $arr[] = $this->requestTranslation($source, $target, $value, $attempts = 5);
    }

    return $arr;
  }

  /**
   * @param string $source
   * @param string $target
   * @param string $text
   * @param int $attempts
   *
   * @return string
   */
  protected function requestTranslation($source, $target, $text, $attempts) {
    // Google translate URL
    $url = 'https://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=uk-RU&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e';

    $fields = [
      'sl' => urlencode($source),
      'tl' => urlencode($target),
      'q' => urlencode($text),
    ];

    if (strlen($fields['q']) >= 5000) {
      throw new \Exception('Maximum number of characters exceeded: 5000');
    }
    // URL-ify the data for the POST
    $fields_string = $this->fieldsString($fields);

    $content = $this->curlRequest($url, $fields, $fields_string, 0, $attempts);

    if (NULL === $content) {
      //echo $text,' Error',PHP_EOL;
      return '';
    }
    else {
      // Parse translation
      return $this->getSentencesFromJSON($content);
    }
  }

  /**
   * Dump of the JSON's response in an array.
   *
   * @param string $json
   *
   * @return string
   */
  protected function getSentencesFromJSON($json) {
    $arr = Json::decode($json, TRUE);
    $sentences = '';

    if (isset($arr['sentences'])) {
      foreach ($arr['sentences'] as $s) {
        $sentences .= isset($s['trans']) ? $s['trans'] : '';
      }
    }

    return $sentences;
  }

  /**
   * Curl Request attempts connecting on failure.
   *
   * @param string $url
   * @param array $fields
   * @param string $fields_string
   * @param int $i
   * @param int $attempts
   *
   * @return string
   */
  protected function curlRequest($url, $fields, $fields_string, $i, $attempts) {
    $i++;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    //curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($ch, CURLOPT_USERAGENT, 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1');

    $result = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (FALSE === $result || 200 !== $httpcode) {
      if ($i >= $attempts) {
        //echo 'Could not connect and get data.',PHP_EOL;
        return;
        //die('Could not connect and get data.'.PHP_EOL);
      }
      else {
        // timeout 1.5 sec
        usleep(3000000);

        return $this->curlRequest($url, $fields, $fields_string, $i, $attempts);
      }
    }
    else {
      return $result; //$this->getBodyCurlResponse();
    }
    curl_close($ch);
  }

  /**
   * Make string with post data fields.
   *
   * @param array $fields
   *
   * @return string
   */
  protected function fieldsString($fields) {
    $fields_string = '';
    foreach ($fields as $key => $value) {
      $fields_string .= $key . '=' . $value . '&';
    }

    return rtrim($fields_string, '&');
  }

}