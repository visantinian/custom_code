<?php

namespace Drupal\pg_country;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a country entity type.
 */
interface PgCountryInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the country title.
   *
   * @return string
   *   Title of the country.
   */
  public function getTitle();

  /**
   * Sets the country title.
   *
   * @param string $title
   *   The country title.
   *
   * @return \Drupal\pg_country\PgCountryInterface
   *   The called country entity.
   */
  public function setTitle($title);

  /**
   * Gets the country creation timestamp.
   *
   * @return int
   *   Creation timestamp of the country.
   */
  public function getCreatedTime();

  /**
   * Sets the country creation timestamp.
   *
   * @param int $timestamp
   *   The country creation timestamp.
   *
   * @return \Drupal\pg_country\PgCountryInterface
   *   The called country entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the country status.
   *
   * @return bool
   *   TRUE if the country is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the country status.
   *
   * @param bool $status
   *   TRUE to enable this country, FALSE to disable.
   *
   * @return \Drupal\pg_country\PgCountryInterface
   *   The called country entity.
   */
  public function setStatus($status);

}
