<?php

namespace Drupal\pg_country;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the country entity type.
 */
class PgCountryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        if (!$entity->isEnabled()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished country');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published country');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit country', 'administer country'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete country', 'administer country'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create country', 'administer country'], 'OR');
  }

}
