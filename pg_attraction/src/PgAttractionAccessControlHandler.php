<?php

namespace Drupal\pg_attraction;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the attraction entity type.
 */
class PgAttractionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        if (!$entity->isEnabled()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished attraction');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published attraction');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit attraction', 'administer attraction'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete attraction', 'administer attraction'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create attraction', 'administer attraction'], 'OR');
  }

}
