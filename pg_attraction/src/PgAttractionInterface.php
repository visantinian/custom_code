<?php

namespace Drupal\pg_attraction;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an attraction entity type.
 */
interface PgAttractionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the attraction title.
   *
   * @return string
   *   Title of the attraction.
   */
  public function getTitle();

  /**
   * Sets the attraction title.
   *
   * @param string $title
   *   The attraction title.
   *
   * @return \Drupal\pg_attraction\PgAttractionInterface
   *   The called attraction entity.
   */
  public function setTitle($title);

  /**
   * Gets the attraction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the attraction.
   */
  public function getCreatedTime();

  /**
   * Sets the attraction creation timestamp.
   *
   * @param int $timestamp
   *   The attraction creation timestamp.
   *
   * @return \Drupal\pg_attraction\PgAttractionInterface
   *   The called attraction entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the attraction status.
   *
   * @return bool
   *   TRUE if the attraction is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the attraction status.
   *
   * @param bool $status
   *   TRUE to enable this attraction, FALSE to disable.
   *
   * @return \Drupal\pg_attraction\PgAttractionInterface
   *   The called attraction entity.
   */
  public function setStatus($status);

}
