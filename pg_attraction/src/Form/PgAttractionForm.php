<?php

namespace Drupal\pg_attraction\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the attraction entity edit forms.
 */
class PgAttractionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New attraction %label has been created.', $message_arguments));
      $this->logger('pg_attraction')->notice('Created new attraction %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The attraction %label has been updated.', $message_arguments));
      $this->logger('pg_attraction')->notice('Updated new attraction %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.pg_attraction.canonical', ['pg_attraction' => $entity->id()]);
  }

}
