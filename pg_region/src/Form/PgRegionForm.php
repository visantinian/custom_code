<?php

namespace Drupal\pg_region\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the region entity edit forms.
 */
class PgRegionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New region %label has been created.', $message_arguments));
      $this->logger('pg_region')->notice('Created new region %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The region %label has been updated.', $message_arguments));
      $this->logger('pg_region')->notice('Updated new region %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.pg_region.canonical', ['pg_region' => $entity->id()]);
  }

}
