<?php

namespace Drupal\pg_region;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a region entity type.
 */
interface PgRegionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the region title.
   *
   * @return string
   *   Title of the region.
   */
  public function getTitle();

  /**
   * Sets the region title.
   *
   * @param string $title
   *   The region title.
   *
   * @return \Drupal\pg_region\PgRegionInterface
   *   The called region entity.
   */
  public function setTitle($title);

  /**
   * Gets the region creation timestamp.
   *
   * @return int
   *   Creation timestamp of the region.
   */
  public function getCreatedTime();

  /**
   * Sets the region creation timestamp.
   *
   * @param int $timestamp
   *   The region creation timestamp.
   *
   * @return \Drupal\pg_region\PgRegionInterface
   *   The called region entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the region status.
   *
   * @return bool
   *   TRUE if the region is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the region status.
   *
   * @param bool $status
   *   TRUE to enable this region, FALSE to disable.
   *
   * @return \Drupal\pg_region\PgRegionInterface
   *   The called region entity.
   */
  public function setStatus($status);

}
