<?php

namespace Drupal\pg_continent\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the continent entity edit forms.
 */
class PgContinentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New continent %label has been created.', $message_arguments));
      $this->logger('pg_continent')->notice('Created new continent %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The continent %label has been updated.', $message_arguments));
      $this->logger('pg_continent')->notice('Updated new continent %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.pg_continent.canonical', ['pg_continent' => $entity->id()]);
  }

}
