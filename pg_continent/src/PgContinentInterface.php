<?php

namespace Drupal\pg_continent;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a continent entity type.
 */
interface PgContinentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the continent title.
   *
   * @return string
   *   Title of the continent.
   */
  public function getTitle();

  /**
   * Sets the continent title.
   *
   * @param string $title
   *   The continent title.
   *
   * @return \Drupal\pg_continent\PgContinentInterface
   *   The called continent entity.
   */
  public function setTitle($title);

  /**
   * Gets the continent creation timestamp.
   *
   * @return int
   *   Creation timestamp of the continent.
   */
  public function getCreatedTime();

  /**
   * Sets the continent creation timestamp.
   *
   * @param int $timestamp
   *   The continent creation timestamp.
   *
   * @return \Drupal\pg_continent\PgContinentInterface
   *   The called continent entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the continent status.
   *
   * @return bool
   *   TRUE if the continent is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the continent status.
   *
   * @param bool $status
   *   TRUE to enable this continent, FALSE to disable.
   *
   * @return \Drupal\pg_continent\PgContinentInterface
   *   The called continent entity.
   */
  public function setStatus($status);

}
