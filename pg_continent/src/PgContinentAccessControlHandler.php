<?php

namespace Drupal\pg_continent;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the continent entity type.
 */
class PgContinentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        if (!$entity->isEnabled()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished continent');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published continent');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit continent', 'administer continent'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete continent', 'administer continent'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create continent', 'administer continent'], 'OR');
  }

}
