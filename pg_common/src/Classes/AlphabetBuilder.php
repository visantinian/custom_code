<?php

namespace Drupal\pg_common\Classes;


use Drupal\Component\Utility\Unicode as Helper;

class AlphabetBuilder extends Helper {

  /**
   * @param $langcode
   *
   * @return array
   */
  public static function get_alphabet($langcode, $view) {
    $alphabet = [];
    $checked_alphabet = [];
    switch ($langcode) {
      case 'en':
        $alphabet = range('A', 'Z');
        break;
      case 'ru':
        $alphabet = array("А", "Б", "В", "Г", "Д", 'Е', "Ё", "Ж", "З", "И", "Й",
          "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц",
          "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я");
        break;
    }
    if ($alphabet != []) {
      $lm = \Drupal::languageManager();
      $language = $lm->getLanguage($langcode);
      $original_language = $lm->getConfigOverrideLanguage();
      $lm->setConfigOverrideLanguage($language);
      foreach ($alphabet as $letter){
        if (self::if_results_exists($letter, $view, $langcode) != NULL){
          $checked_alphabet[] = $letter;
        }
      }
      $lm->setConfigOverrideLanguage($original_language);
      if ($checked_alphabet != []){
        return $checked_alphabet;
      }
    }
  }

  /**
   * @param $letter
   * @param $view_id
   * Helper function that checks if results exists for a given view with given argument.
   */
  public static function if_results_exists($letter, $view_id, $langcode){
    $result = views_get_view_result($view_id, 'default', $letter);
    if (empty($result)){
      return;
    }
    if (!empty($result)){
      return $letter;
    }
  }

}
