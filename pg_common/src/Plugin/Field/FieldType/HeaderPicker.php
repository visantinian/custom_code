<?php

namespace Drupal\pg_common\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'string' entity field type.
 *
 * @FieldType(
 *   id = "header_with_header_picker",
 *   label = @Translation("Header with header_type picker"),
 *   description = @Translation("A field value for a plain string value of a header + header level picker"),
 *   category = @Translation("Text"),
 *   default_widget = "header_picker_widget",
 *   default_formatter = "header_picker",
 *    column_groups = {
 *   "header_level" = {
 *       "label" = @Translation("Header Level"),
 *       "translatable" = FALSE
 *     },
 *   }
 * )
 */
class HeaderPicker extends StringItem {


  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'header_level' => 'none',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema( $field_definition);
    $schema['columns']['header_level'] = [
        'type' => 'text',
        'length' => 5,
        'description' => 'Header level of the current header',
      ];
    return $schema;
  }
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['header_level'] = DataDefinition::create('string')
      ->setLabel(t('Header Level'))
      ->setDescription(t("String that indicates needed level of header that this component will have on display"));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = parent::storageSettingsForm($form,$form_state, $has_data);

    $elements['header_level'] = [
      '#type' => 'select',
      '#title' => t('Default Header Level'),
      '#default_value' => $this->getSetting('header_level'),
      '#options' => [
        'none' => 'none',
        'h1' => '<h1>',
        'h2' => '<h2>',
        'h3' => '<h3>',
        'h4' => '<h4>',
      ],
      '#description' => t('Default level of the header for a new item'),
    ];

    return $elements;
  }

  public function getHeaderLevel(){
    return $this->values["header_level"];
  }

  public function getHeaderValue(){
    return $this->values["value"];
  }

}
