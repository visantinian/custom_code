<?php

namespace Drupal\pg_common\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin implementation of the 'image with source and description' field type.
 *
 * @FieldType(
 *   id = "image_with_source_and_description",
 *   label = @Translation("Image with source and description"),
 *   description = @Translation("Stores a photo's source, and description"),
 *   category = @Translation("Reference"),
 *   column_groups = {
 *     "file" = {
 *       "label" = @Translation("File"),
 *       "columns" = {
 *         "target_id", "width", "height"
 *       },
 *       "require_all_groups_for_translation" = TRUE
 *     },
 *     "alt" = {
 *       "label" = @Translation("Alt"),
 *       "translatable" = TRUE
 *     },
 *     "title" = {
 *       "label" = @Translation("Title"),
 *       "translatable" = TRUE
 *     },
 *   "photo_source" = {
 *       "label" = @Translation("Photo Source"),
 *       "translatable" = TRUE
 *     },
 *   "photo_description" = {
 *       "label" = @Translation("Photo description"),
 *       "translatable" = TRUE
 *     },
 *   },
 *   default_widget = "pg_image_description_and_source",
 *   default_formatter = "photo_source_and_description" ,
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class PhotoSourceAndDescription extends ImageItem {
  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    $settings = parent::defaultStorageSettings();
    $settings['default_image']['photo_description'] = '';
    $settings['default_image']['photo_source'] = '';

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $settings = parent::defaultFieldSettings();
    $settings['photo_description'] = TRUE;
    $settings['photo_source'] = TRUE;
    $settings['default_image']['photo_description'] = '';
    $settings['default_image']['photo_source'] = '';
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $photo_source = $this->get('photo_source')->getValue();
    return $photo_source === NULL || $photo_source === '';
  }


  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['columns']['photo_description'] = array(
      'description' => 'Description that will be printed with the photo as text',
      'type' => 'text',
      'length' => 255,
    );
    $schema['columns']['photo_source'] = array(
      'description' => 'Additional info of a person, who owns the rights on photo',
      'type' => 'text',
      'length' => 255,
    );
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['photo_description'] = DataDefinition::create('string')
      ->setLabel(t('Photo Description'))
      ->setDescription(t("Description that will be printed with the photo as text"));
    $properties['photo_source'] = DataDefinition::create('string')
      ->setLabel(t('Photo Source'))
      ->setDescription(t("Additional info of a person, who owns the rights on photo"));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    // Get base form from FileItem.
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = $this->getSettings();

    // Add title and alt configuration options.
    $element['photo_description'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Photo Description</em> field'),
      '#default_value' => $settings['photo_description'],
      '#description' => t('Short description of the image that will be shown below phto'),
      '#weight' => 9,
    ];
    $element['photo_source'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable <em>Photo source</em> field'),
      '#default_value' => $settings['photo_source'],
      '#description' => t('The photo source holds information about rightful owner of the photo'),
      '#weight' => 11,
    ];


    return $element;
  }

  public function getSource(){
    return $this->values["photo_source"];
  }

  public function hasNewEntity() {
    return;
  }

  public function getDescription(){
    return $this->values["photo_description"];
  }
}
