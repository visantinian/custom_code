<?php

namespace Drupal\pg_common\Plugin\Field\FieldWidget;

use Drupal\Core\Url;
use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;
use Drupal\paragraphs_browser\Plugin\Field\FieldWidget\ParagraphsBrowserWidgetTrait;
use Drupal\paragraphs_previewer\Plugin\Field\FieldWidget\ParagraphsPreviewerWidgetTrait;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'entity_reference paragraphs' widget for editable translation.
 *
 * We hide add / remove buttons when translating to avoid accidental loss of
 * data because these actions effect all languages.
 *
 * @FieldWidget(
 *   id = "pg_paragraphs_browser_previewer",
 *   label = @Translation("PG Paragraphs Browser Previewer EXPERIMENTAL"),
 *   description = @Translation("An paragraphs inline form widget with a Browser Previewer."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class PGParagraphsBrowserPreviewerWidget extends ParagraphsWidget {

  use ParagraphsPreviewerWidgetTrait, ParagraphsBrowserWidgetTrait {
    ParagraphsBrowserWidgetTrait::defaultSettings insteadof ParagraphsPreviewerWidgetTrait;
    ParagraphsPreviewerWidgetTrait::formElement insteadof ParagraphsBrowserWidgetTrait;
  }

  /**
   * Returns select options for a plugin setting.
   *
   * This is done to allow
   * \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::settingsSummary()
   * to access option labels. Not all plugin setting are available.
   *
   * @param string $setting_name
   *   The name of the widget setting. Supported settings:
   *   - "edit_mode"
   *   - "closed_mode"
   *   - "autocollapse"
   *   - "add_mode",
   *
   * @return array|null
   *   An array of setting option usable as a value for a "#options" key.
   *
   * @see \Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget::settingsSummary()
   */
  protected function getSettingOptions($setting_name) {
    $options = parent::getSettingOptions($setting_name);
    switch ($setting_name) {
      case 'add_mode':
        $options['paragraphs_browser'] = $this->t('Paragraphs Browser');
        break;
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @see \Drupal\content_translation\Controller\ContentTranslationController::prepareTranslation()
   *   Uses a similar approach to populate a new translation.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $field_name = $this->fieldDefinition->getName();
    $parents = $element['#field_parents'];
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraphs_entity */
    $paragraphs_entity = NULL;
    $host = $items->getEntity();
    $widget_state = static::getWidgetState($parents, $field_name, $form_state);

    $entity_type_manager = \Drupal::entityTypeManager();
    $target_type = $this->getFieldSetting('target_type');

    $item_mode = isset($widget_state['paragraphs'][$delta]['mode']) ? $widget_state['paragraphs'][$delta]['mode'] : 'edit';
    $default_edit_mode = $this->getSetting('edit_mode');

    if (isset($widget_state['paragraphs'][$delta]['entity'])) {
      $paragraphs_entity = $widget_state['paragraphs'][$delta]['entity'];
    }
    elseif (isset($items[$delta]->entity)) {
      $paragraphs_entity = $items[$delta]->entity;

      // We don't have a widget state yet, get from selector settings.
      if (!isset($widget_state['paragraphs'][$delta]['mode'])) {

        if ($default_edit_mode == 'open' || $widget_state['items_count'] < $this->getSetting('closed_mode_threshold')) {
          $item_mode = 'edit';
        }
        elseif ($default_edit_mode == 'closed') {
          $item_mode = 'closed';
        }
        elseif ($default_edit_mode == 'closed_expand_nested') {
          $item_mode = 'closed';
          $field_definitions = $paragraphs_entity->getFieldDefinitions();

          // If the paragraph contains other paragraphs, then open it.
          foreach ($field_definitions as $field_definition) {
            if ($field_definition->getType() == 'entity_reference_revisions' && $field_definition->getSetting('target_type') == 'paragraph') {
              $item_mode = 'edit';
              break;
            }
          }
        }
      }
    }
    elseif (isset($widget_state['selected_bundle'])) {
      $entity_type = $entity_type_manager->getDefinition($target_type);
      $bundle_key = $entity_type->getKey('bundle');

      $paragraphs_entity = $entity_type_manager->getStorage($target_type)->create(array(
        $bundle_key => $widget_state['selected_bundle'],
      ));
      $paragraphs_entity->setParentEntity($host, $field_name);
      $item_mode = 'edit';
    }

   // $id_prefix = implode('-', array_merge($parents, array($field_name, $delta)));
    //$button_access = $paragraphs_entity->access('delete');
   // if ($item_mode != 'remove') {
      /*$widget_actions['dropdown_actions']['remove_button'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => $id_prefix . '_remove',
        '#weight' => 501,
        '#submit' => [[get_class($this), 'paragraphsItemSubmit']],
        // Ignore all validation errors because deleting invalid paragraphs
        // is allowed.
        '#limit_validation_errors' => [],
        '#delta' => $delta,
        '#ajax' => [
          'callback' => array(get_class($this), 'itemAjax'),
          'wrapper' => $widget_state['ajax_wrapper_id'],
        ],
        '#access' => $button_access,
        '#paragraphs_mode' => 'remove',
      ];*/

      // Change default remove.
      //$element['top']['actions']['dropdown_actions'] = array_map([$this, 'expandButton'], $widget_actions['dropdown_actions']);
   // }

    // Paragraph previewer.
    $field_name = $this->fieldDefinition->getName();
    $parents = $element['#field_parents'];

    $widget_state = static::getWidgetState($parents, $field_name, $form_state);
    if (!isset($widget_state['paragraphs'][$delta]['mode']) ||
      !isset($widget_state['paragraphs'][$delta]['entity'])) {
      return $element;
    }

    $item_mode = $widget_state['paragraphs'][$delta]['mode'];
    if (!$this->isPreviewerEnabled($item_mode)) {
      return $element;
    }

    $paragraphs_entity = $widget_state['paragraphs'][$delta]['entity'];
    $element_parents = array_merge($parents, [$field_name, $delta]);
    $id_prefix = implode('-', $element_parents);

    $previewer_element = [
      '#type' => 'submit',
      '#value' => t('Preview'),
      '#name' => strtr($id_prefix, '-', '_') . '_previewer',
      '#weight' => -99999,
      '#submit' => [[get_class($this), 'submitPreviewerItem']],
      '#field_item_parents' => $element_parents,
      '#limit_validation_errors' => [
        array_merge($parents, [$field_name, 'add_more']),
      ],
      '#delta' => $delta,
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxSubmitPreviewerItem'],
        'wrapper' => $widget_state['ajax_wrapper_id'],
        'effect' => 'fade',
      ],
      '#access' => $paragraphs_entity->access('view'),
      '#attributes' => [
        'class' => ['link', 'paragraphs-previewer'],
      ],
      '#attached' => [
        'library' => ['paragraphs_previewer/dialog'],
      ],
    ];

    if ($paragraphs_entity->isNewTranslation()) {
      $need_translation = [
        '#type' => 'markup',
        '#markup' => '<div class="paragraph-translation-required">' . $this->t('Нужно перевести!') . '</div>',
      ];
      $element['top']['actions']['actions']['need_translation'] = $need_translation;
    }

    // Set the dialog title.
    if (isset($element['top']['paragraph_type_title']['info']['#markup'])) {
      $previewer_element['#previewer_dialog_title'] = strip_tags($element['top']['paragraph_type_title']['info']['#markup']);
    }
    else {
      $previewer_element['#previewer_dialog_title'] = t('Preview');
    }

    if (isset($element['top']['actions']['actions'])) {
      // Support "paragraphs" widget.
      $element['top']['actions']['actions']['previewer_button'] = $previewer_element;
    }
    elseif (isset($element['top']['links'])) {
      // Support legacy "entity_reference_paragraphs" widget.
      $element['top']['links']['previewer_button'] = $previewer_element;
    }
    else {
      // Place in the top as a fallback.
      $element['top']['previewer_button'] = $previewer_element;
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function getWidgetState(array $parents, $field_name, FormStateInterface $form_state) {
    return NestedArray::getValue($form_state->getStorage(), static::getWidgetStateParents($parents, $field_name));
  }

  /**
   * {@inheritdoc}
   */
  public function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    $elements = parent::formMultipleElements($items, $form, $form_state);
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    // Adding add more.
    //if (($this->realItemCount < $cardinality || $cardinality == FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED) && !$form_state->isProgrammed() && (!$this->isTranslating || $this->fieldDefinition->isTranslatable())) {
      $elements['add_more'] = $this->buildAddActions();
    //}

    // Paragraph browser field widget.
    // If the Paragraph browser hasn't been set, return un-modified form.
    if ($this->getSetting('paragraphs_browser') == '_na') {
      return $elements;
    }

    if (empty($this->uuid)) {
      $this->uuid = \Drupal::service('uuid')->generate();
    }
    $elements['add_more']['add_more_select']['#attributes']['data-uuid'] = $this->uuid;
    $elements['add_more']['add_more_select']['#attributes']['class'][] = 'js-hide';
    $elements['add_more']['add_more_select']['#title_display'] = 'hidden';
    $elements['add_more']['add_more_button']['#attributes']['data-uuid'] = $this->uuid;
    $elements['add_more']['add_more_button']['#attributes']['class'][] = 'js-hide';
    unset($elements['add_more']['add_more_button']['#suffix']);
    unset($elements['add_more']['add_more_button']['#prefix']);

    $elements['#attached']['library'][] = 'paragraphs_browser/modal';

    $storage = $form_state->getStorage();

//    if (($storage['langcode'] !== $storage['entity_default_langcode']) && !(!$this->isTranslating || $this->fieldDefinition->isTranslatable())) {
//      unset($elements['add_more']);
//    }
//    else {
      $elements['add_more']['browse'] = array(
        '#type' => 'submit',
        '#value' => $this->t('Add :title', [':title' => $this->getSetting('title')]),
        '#attributes' => ['class' => ['js-show']],
        '#ajax' => [
          'url' => Url::fromRoute(
            'paragraphs_browser.paragraphs_browser_controller', [
              'field_config' => implode('.', array($items->getEntity()->getEntityTypeId(), $items->getEntity()->bundle(), $this->fieldDefinition->getName())),
              'paragraphs_browser_type' => $this->getSetting('paragraphs_browser'),
              'uuid' => $this->uuid,
            ]
          ),
        ],
      );
    //}

    if (isset($elements['#cardinality']) && $elements['#cardinality'] != -1) {
      $keyCount = count(
        array_filter(
          array_keys($elements),
          'is_numeric'
        )
      );
      if ($elements['#cardinality'] <= $keyCount) {
        unset($elements['add_more']);
      }
    }

    return $elements;
  }

  /**
   * Checks if we can allow reference changes.
   *
   * @return bool
   *   TRUE if we can allow reference changes, otherwise FALSE.
   */
  protected function allowReferenceChanges() {
    //if ($this->fieldDefinition->isTranslatable()) {
      return TRUE;
    //}
    //return !$this->isTranslating;
  }

}
