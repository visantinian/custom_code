<?php

namespace Drupal\pg_common\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image_widget_crop\Plugin\Field\FieldWidget\ImageCropWidget;

/**
 * Plugin implementation of the 'pg_image_description_and_source' widget.
 *
 * @FieldWidget(
 *   id = "pg_image_description_and_source",
 *   label = @Translation("PG description and source"),
 *   description = @Translation("Description and source fields"),
 *   field_types = {
 *     "image_with_source_and_description"
 *   }
 * )
 */
class PhotoSourceAndDescriptionWidget extends ImageCropWidget {

  public static function process($element, FormStateInterface $form_state, $form) {
    $item = $element['#value'];
    if ($item) {
      foreach ($element['#value']['fids'] as $fid) {
        $element['photo_description'] = [
          '#type' => 'textfield',
          '#size' => 60,
          '#title' => t('Photo Description'),
          '#description' => t('Description of the photo.'),
          '#default_value' => isset($item['photo_description']) ? $item['photo_description'] : '',
          '#weight' => -5,
        ];
        $element['photo_source'] = [
          '#type' => 'textfield',
          '#size' => 60,
          '#title' => t('Photo Source'),
          '#description' => t('Nickname or other information about rightful owner of the photo'),
          '#default_value' => isset($item['photo_source']) ? $item['photo_source'] : '',
          '#weight' => -10,
        ];
      }
    }

    return parent::process($element, $form_state, $form);
  }
}
