<?php


namespace Drupal\pg_common\Plugin\Field\FieldWidget;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'string_textfield' widget.
 *
 * @FieldWidget(
 *   id = "header_picker_widget",
 *   label = @Translation("Header + Header Picker"),
 *   field_types = {
 *     "header_with_header_picker"
 *   }
 * )
 */
class HeaderPickerWidget extends StringTextfieldWidget {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items,
                              $delta, array $elements, array &$form,
                              FormStateInterface $form_state) {
    $elements = parent::formElement($items, $delta, $elements, $form, $form_state);
    $elements['header_level'] = [
      '#type' => 'select',
      '#default_value' => isset($items[$delta]->header_level) ? $items[$delta]->header_level : 'none',
      '#options' => [
        'none' => 'none',
        'h1' => '<h1>',
        'h2' => '<h2>',
        'h3' => '<h3>',
        'h4' => '<h4>',
      ],
    ];

    return $elements;
  }
}
