<?php

namespace Drupal\pg_common\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\filter\Entity\FilterFormat;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'read_more_default' formatter.
 *
 * @FieldFormatter(
 *   id = "pg_read_more_link",
 *   label = @Translation("PG read more link"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 */
class PGReadMoreLinkFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Summary Formatter.
   *
   * @var \Drupal\text\Plugin\Field\FieldFormatter\TextSummaryOrTrimmedFormatter
   */
  protected $summaryFormatter;

  /**
   * Default Formatter.
   *
   * @var \Drupal\text\Plugin\Field\FieldFormatter\TextSummaryOrTrimmedFormatter
   */
  protected $defaultFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PluginManagerInterface $pluginManager, RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->summaryFormatter = $pluginManager->createInstance(
      'text_summary_or_trimmed', [
        'field_definition' => $field_definition,
        'settings' => $settings,
        'label' => $label,
        'view_mode' => $view_mode,
        'third_party_settings' => $third_party_settings,
      ]
    );
    $this->defaultFormatter = $pluginManager->createInstance(
      'text_default', [
        'field_definition' => $field_definition,
        'settings' => $settings,
        'label' => $label,
        'view_mode' => $view_mode,
        'third_party_settings' => $third_party_settings,
      ]
    );
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.field.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'trim_length' => '600',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return $this->summaryFormatter->settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return $this->summaryFormatter->settingsSummary();
  }

  /**
   * View elements.
   *
   * @inheritdoc
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $summary = $this->summaryFormatter->viewElements($items, $langcode);
    $description = $this->defaultFormatter->viewElements($items, $langcode);

    $summary_copy = $summary;
    $summaryMarkup = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $this->renderer->render($summary_copy));
    $descriptionMarkup = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $this->renderer->render($description));

    $descriptionMarkupClean = '';
    $link_isset  = FALSE;
    if (!strpos($descriptionMarkup, '<li') && !strpos($descriptionMarkup, '<h3')) {
      $link_isset = (string) $summaryMarkup !== (string) $descriptionMarkup;
      if ($link_isset) {
        $descriptionMarkupClean = trim($this->textWithoutSummary($description[0]['#markup'], 'full_html', $summary[0]['#text_summary_trim_length']));
      }
    }
    else {
      $summaryMarkup = $description;
    }

    $elements[] = [
      '#theme' => 'pg_read_more_link',
      '#summary' => $summaryMarkup,
      '#description' => $descriptionMarkupClean,
      '#link_isset' => $link_isset,
    ];

    return $elements;
  }

  /**
   * @param $text
   * @param null $format
   * @param null $size
   *
   * @return bool|string
   */
  public function textWithoutSummary($text, $format = NULL, $size = NULL) {

    if (!isset($size)) {
      $size = \Drupal::config('text.settings')->get('default_summary_length');
    }

    // Find where the delimiter is in the body
    $delimiter = strpos($text, '<!--break-->');

    // If the size is zero, and there is no delimiter, the entire body is the summary.
    if ($size == 0 && $delimiter === FALSE) {
      return $text;
    }

    // If a valid delimiter has been specified, use it to chop off the summary.
    if ($delimiter !== FALSE) {
      return substr($text, 0, $delimiter);
    }

    // Retrieve the filters of the specified text format, if any.
    if (isset($format)) {
      $filter_format = FilterFormat::load($format);
      // If the specified format does not exist, return nothing. $text is already
      // filtered text, but the remainder of this function will not be able to
      // ensure a sane and secure summary.
      if (!$filter_format || !($filters = $filter_format->filters())) {
        return '';
      }
    }

    // If we have a short body, the entire body is the summary.
    if (mb_strlen($text) <= $size) {
      return $text;
    }

    // If the delimiter has not been specified, try to split at paragraph or
    // sentence boundaries.

    // The summary may not be longer than maximum length specified. Initial slice.
    $summary = Unicode::truncate($text, $size);

    // Store the actual length of the UTF8 string -- which might not be the same
    // as $size.
    $max_rpos = strlen($summary);

    // How much to cut off the end of the summary so that it doesn't end in the
    // middle of a paragraph, sentence, or word.
    // Initialize it to maximum in order to find the minimum.
    $min_rpos = $max_rpos;

    // Store the reverse of the summary. We use strpos on the reversed needle and
    // haystack for speed and convenience.
    $reversed = strrev($summary);

    // Build an array of arrays of break points grouped by preference.
    $break_points = [];

    // A paragraph near the end of sliced summary is most preferable.
    $break_points[] = ['</p>' => 0];

    // If no complete paragraph then treat line breaks as paragraphs.
    $line_breaks = ['<br />' => 6, '<br>' => 4];
    // Newline only indicates a line break if line break converter
    // filter is present.
    if (isset($format) && $filters->has('filter_autop') && $filters->get('filter_autop')->status) {
      $line_breaks["\n"] = 1;
    }
    $break_points[] = $line_breaks;

    // If the first paragraph is too long, split at the end of a sentence.
    $break_points[] = ['. ' => 1, '! ' => 1, '? ' => 1, '。' => 0, '؟ ' => 1];

    // Iterate over the groups of break points until a break point is found.
    foreach ($break_points as $points) {
      // Look for each break point, starting at the end of the summary.
      foreach ($points as $point => $offset) {
        // The summary is already reversed, but the break point isn't.
        $rpos = strpos($reversed, strrev($point));
        if ($rpos !== FALSE) {
          $min_rpos = min($rpos + $offset, $min_rpos);
        }
      }

      // If a break point was found in this group, slice and stop searching.
      if ($min_rpos !== $max_rpos) {
        // Don't slice with length 0. Length must be <0 to slice from RHS.
        $summary = ($min_rpos === 0) ? $summary : substr($summary, 0, 0 - $min_rpos);
        $desc = substr($text, strlen($summary));
        break;
      }
    }

    // If the htmlcorrector filter is present, apply it to the generated summary.
    if (isset($format) && $filters->has('filter_htmlcorrector') && $filters->get('filter_htmlcorrector')->status) {
      $desc = Html::normalize($desc);
    }

    return $desc;
  }


}
