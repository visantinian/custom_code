<?php

namespace Drupal\pg_common\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * Plugin implementation of the 'string' formatter.
 *
 * @FieldFormatter(
 *   id = "header_picker",
 *   label = @Translation("Header Picker"),
 *   field_types = {
 *     "header_with_header_picker",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class HeaderTextFormatter extends StringFormatter {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($items as $delta => $item) {
      $view_value = $this->viewValue($item);
      $item_values = $item->getEntity()->toArray();
      $elements[$delta] = $view_value;
      $elements['header_level'] = $item_values['header_level'];
    }
    return $elements;
  }
}
