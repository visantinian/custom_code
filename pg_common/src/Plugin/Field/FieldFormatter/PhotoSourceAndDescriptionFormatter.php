<?php

/**
 * @file
 * Contains Drupal\pg_common\Plugin\Field\FieldFormatter\PhotoSourceAndDescriptionFormatter
 */

namespace Drupal\pg_common\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\svg_image\Plugin\Field\FieldFormatter\SvgImageFormatter;

/**
 * Plugin implementation of the 'extra_image_field_classes' formatter.
 *
 * @FieldFormatter(
 *   id = "photo_source_and_description",
 *   label = @Translation("Photo Source and Description"),
 *   field_types = {
 *     "image_with_source_and_description"
 *   }
 * )
 */
class PhotoSourceAndDescriptionFormatter extends SvgImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = parent::viewElements($items, $langcode);
    /** @var \Drupal\file\Entity\File[] $files */
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    foreach ($files as $delta => $file) {
      $value = $file->toArray();
      $elements[$delta]['photo_source'] = ['#markup' => $elements[$delta]["#item"]->getSource()];
      $elements[$delta]['photo_description'] = ['#markup' => $elements[$delta]["#item"]->getDescription()];
    }
    return $elements;
  }
}
