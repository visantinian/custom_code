<?php


namespace Drupal\pg_common\Plugin\Field\FieldFormatter;


use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "pg_psd_responsive_image",
 *   label = @Translation("Responsive image customized"),
 *   field_types = {
 *     "image_with_source_and_description",
 *   }
 * )
 */
class PGPSDAdaptive  extends ResponsiveImageFormatter {

}
