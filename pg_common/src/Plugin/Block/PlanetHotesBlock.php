<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\pg_attraction\PgAttractionInterface;
use Drupal\pg_city\PgCityInterface;
use Drupal\pg_country\PgCountryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use HTTP_Request2;

/**
 * Provides PlanetHotelsBlock.
 *
 * @Block(
 *   id = "planet_hotels_block",
 *   admin_label = @Translation("Planet Hotels Block"),
 * )
 */
class PlanetHotesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * RMPromotionalBadgesBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if (!$page_entity = $this->getPageEntity()) {
      return FALSE;
    }
    else {
      $clangcode = $this->languageManager->getCurrentLanguage()->getId();

      if ($clangcode == 'ru'){
        $langcode = '';
      }
      else {
        $langcode = '/' . $clangcode . '/';
      }

      if ($page_entity->hasTranslation($langcode)) {
        $page_entity = $page_entity->getTranslation($langcode);
      }

      // Get hotels on request.
      $id = $page_entity->id();
      $type = $page_entity->getEntityTypeId();
      $cache_key = $type . ':' . $id;
      if ($page_entity instanceof PgCityInterface) {
        $prepared = $this->prepareCity($page_entity, $langcode);
      }
      if ($page_entity instanceof PgCountryInterface) {
        $prepared = $this->prepareCountry($page_entity, $langcode);
      }
      if ($page_entity instanceof PgAttractionInterface) {
        $prepared = $this->prepareArttraction($page_entity, $langcode);
      }

      $hotels_prepared = $this->getRequest(
        $prepared['coordinates'],
        $prepared['url']
      );

      $block = [
        '#theme' => 'planet_hotels_block',
        '#hotels' => $hotels_prepared,
        '#closeword' => $prepared['closeword'],
        '#current_mark' => $prepared['current_mark'],
        '#page_name' => $prepared['page_name'],
        '#cache' => [
          'contexts' => ['languages', 'url.path', 'url.query_args'],
          'keys' => ['block', $this->getPluginId(), $type, $id, $cache_key],
          'tags' => [$cache_key],
        ],
      ];
      return $block;
    }
  }

  /**
   * Helper function for get current page entity.
   *
   * @return array|bool|\Drupal\Core\Entity\EntityInterface|mixed|null
   */
  public function getPageEntity() {
    $page_entity = &drupal_static(__FUNCTION__, NULL);
    if (isset($page_entity)) {
      return $page_entity ?: NULL;
    }
    $current_route = $this->routeMatch;
    foreach ($current_route->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $page_entity = $param;
        break;
      }
    }
    if (!isset($page_entity)) {
      // Some routes don't properly define entity parameters.
      // Thus, try to load them by its raw Id, if given.
      $entity_type_manager = $this->entityTypeManager;
      $types = $entity_type_manager->getDefinitions();
      foreach ($current_route->getParameters()->keys() as $param_key) {
        if (!isset($types[$param_key])) {
          continue;
        }
        if ($param = $current_route->getParameter($param_key)) {
          if (is_string($param) || is_numeric($param)) {
            try {
              $page_entity = $entity_type_manager->getStorage($param_key)
                ->load($param);
            } catch (\Exception $e) {
            }
          }
          break;
        }
      }
    }
    if (!isset($page_entity) || !$page_entity->access('view')) {
      $page_entity = FALSE;
      return NULL;
    }
    return $page_entity;
  }


  public function prepareCountry (&$entity, &$langcode){
    $prepared = [];
    $prepared['url'] = 'https://planet:planet@release.planetofhotels.com'
      . $langcode
      . '/api/guide/hotels'
      . '?apikey=793ac0b561f79e089ab52fdb9dc39496'
      . '&country=' . $entity->id()
      . '&count=12';
    $prepared['page_name'] = $this->getPageName($entity, 'country_name_sec');
    $prepared['coordinates'] = NULL;
    $prepared['current_mark'] = NULL;
    $prepared['closeword'] = $this->t('в');
    return $prepared;
  }

  public function getPageName(&$entity, $field_name){
    $prepared = [];
    $prepared['page_name'] = $entity->label();
    $page_name_value = $entity->get($field_name)->getValue();
    if ($page_name_value != NULL){
      $page_name = $page_name_value[0]['value'];
    }
    return $page_name;
  }

  public function getEntityCoordinates(&$entity, $field_name){
    $coordinates = $entity->get($field_name)->getValue();
    if ($coordinates){
      $prepared['current_latitude'] = $coordinates[0]['lat'];
      $prepared['current_longitude'] = $coordinates[0]['lon'];
    }
    return $prepared;
  }


  public function prepareCity(&$entity, &$langcode){
    $prepared = [];
    $prepared['url'] = 'https://planet:planet@release.planetofhotels.com'
      . $langcode
      . '/api/guide/hotels'
      . '?apikey=793ac0b561f79e089ab52fdb9dc39496'
      . '&country=' . $entity->get('city_country')->getValue()[0]['target_id']
      . '&city=' . $entity->id()
      . '&count=12';
    $prepared['page_name'] = $this->getPageName($entity, 'city_name_sec');
    $prepared['coordinates'] = $this->getEntityCoordinates($entity, 'city_coordinates');
    $prepared['current_mark'] = $this->t('центра');
    $prepared['closeword'] = NULL;
    return $prepared;
  }

  public function prepareArttraction(&$entity, &$langcode){
    $prepared = [];
    $prepared['coordinates'] = $this->getEntityCoordinates($entity, 'attraction_coordinates');
    $prepared['url'] = 'https://planet:planet@release.planetofhotels.com'
      . $langcode
      . '/api/guide/hotels'
      . '?apikey=793ac0b561f79e089ab52fdb9dc39496'
      . '&latitude=' . $prepared['coordinates']['current_latitude']
      . '&longitude=' . $prepared['coordinates']['current_longitude']
      . '&count=12';
    $prepared['page_name'] = $entity->label();
    $prepared['current_mark'] = $prepared['page_name'];
    $prepared['closeword'] = $this->t('возле');
    return $prepared;
  }

  public function getRequest(&$coordinates, &$url){
    $request = new HTTP_Request2();
    $request->setUrl($url);
    $request->setMethod(HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
      'follow_redirects' => TRUE
    ));
    $request->setHeader(array(
      'Authorization' => 'Basic cGxhbmV0OnBsYW5ldA=='
    ));
    try {
      $response = $request->send();
      if ($response->getStatus() == 200) {
        $data = json_decode($response->getBody())->result;
      } else {
        \Drupal::logger('PG Hotel Queue')
          ->error('Unexpected HTTP status: ' . $response->getStatus() . ' ' .
            $response->getReasonPhrase());
      }
    } catch (\HTTP_Request2_Exception $e) {
      \Drupal::logger('PG Hotel Queue')->error('Error: ' . $e->getMessage());
    }
    foreach ($data as $hotel) {
      $vars = get_object_vars($hotel);
      $vars['relative_distance'] = $this->getRelativeDistance(
        $hotel,
        $coordinates['current_latitude'],
        $coordinates['current_longitude']
      );
      $hotels_prepared[] = $vars;
    }
    return $hotels_prepared;
  }

  public function getRelativeDistance(&$hotel, &$current_latitude, &$current_longitude)
  {
    $distance = NULL;
    if ($hotel_longitude = $hotel->longitude != NULL
      && $hotel_latitude = $hotel->latitude != NULL)
    {
      if ($hotel_longitude != '' && $hotel_latitude != '' && $current_longitude != '' && $current_latitude != '') {
        $EARTH_RADIUS = 6372795;
        $hotel_longitude = $hotel->longitude;
        $hotel_latitude = $hotel->latitude;

        $latFrom = deg2rad($current_latitude);
        $lonFrom = deg2rad($current_longitude);
        $latTo = deg2rad($hotel_latitude);
        $lonTo = deg2rad($hotel_longitude);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = asin(sqrt(pow(sin($latDelta / 2), 2) +
          cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)) );
        $distance = $angle * 2 * $EARTH_RADIUS;
        $distance = round($distance);
      }
    }
    return $distance;
  }

}
