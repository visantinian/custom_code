<?php


namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGAttractionCatalogPageBannerBlock.
 *
 * @Block(
 *   id = "pg_city_popular_cities_on_country_block",
 *   admin_label = @Translation("PG City: Popular Cities On Country Block")
 * )
 */
class PGCityPopularCitiesOfCountryBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * @inheritdoc
   */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    }

    /**
     * @inheritdoc
     * @return array|string[]
     */
    public function getCacheContexts()
    {
        return ['route.name'];
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $block = [
      '#theme' => 'pg_city_popular_cities_on_country_block',
      '#cache' => [
        'contexts' => ['languages', 'url.path'],
        'tags' => $this->getCacheTags(),
      ],
    ];

        return $block;
    }

    /**
     * {@inheritdoc}
     */
    public function getPluginId()
    {
        return 'pg_city_popular_cities_on_country_block';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTags()
    {
        parent::getCacheTags();
    }
}
