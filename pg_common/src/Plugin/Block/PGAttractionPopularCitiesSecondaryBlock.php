<?php


namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGAttractionCatalogPageBannerBlock.
 *
 * @Block(
 *   id = "pg_attraction_popular_cities_secondary_block",
 *   admin_label = @Translation("PG Attraction Popular Cities Secondary Block")
 * )
 */
class PGAttractionPopularCitiesSecondaryBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * @inheritDoc
   */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    }

    /**
     * @inheritdoc
     * @return array|string[]
     */
    public function getCacheContexts()
    {
        return ['route.name'];
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $block = [
      '#theme' => 'pg_attraction_popular_cities_secondary_block',
      '#cache' => [
        'contexts' => ['languages', 'url.path'],
        'tags' => $this->getCacheTags(),
      ],
    ];

        return $block;
    }

    /**
     * {@inheritdoc}
     */
    public function getPluginId()
    {
        return 'pg_attraction_popular_cities_secondary_block';
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTags()
    {
        parent::getCacheTags();
    }
}
