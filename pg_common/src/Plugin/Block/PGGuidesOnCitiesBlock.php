<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\pg_attraction\Entity\PgAttraction;
use Drupal\pg_city\Entity\PgCity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGGuidesOnCitiesBlock.
 *
 * @Block(
 *   id = "pg_guides_on_cities_block",
 *   admin_label = @Translation("PG Guides on Cities Block"),
 * )
 */
class PGGuidesOnCitiesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * PGCountryAutoBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if (!$page_entity = $this->getPageEntity()) {
      return FALSE;
    }
    else {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      if ($page_entity->hasTranslation($langcode)) {
        $page_entity = $page_entity->getTranslation($langcode);
      }
      $argument = NULL;

      // Get paragraph components.
      if ($page_entity instanceof PgAttraction) {
        $cache_key = 'pg_guides-' . $page_entity->id();
        if ($page_entity->get('attraction_from_viator')->getValue() == TRUE
          && $page_entity->hasField('attraction_city')) {
          $city_eid =  $page_entity->get('attraction_city')->getValue()[0]['target_id'];
          $city_entity = \Drupal::service('entity_type.manager')->getStorage('pg_city')
            ->load($city_eid);
          $argument = $city_entity->get('viator_id')->getValue()[0]['value'];
        }
      }
      if ($page_entity instanceof PgCity) {
        $cache_key = 'pg_guides-' . $page_entity->id();
        @$argument = $page_entity->get('viator_id')->getValue()[0]['value'];
      }


      $block = [
        '#theme' => 'pg_guides_on_cities_block',
        '#components' => ['view_argument' => $argument],
        '#cache' => [
          'contexts' => ['languages', 'url.path', 'url.query_args'],
          'keys' => ['block', $this->getPluginId(), $cache_key],
          'tags' => [$cache_key],
        ],
      ];

      return $block;
    }
  }

  /**
   * Helper function for get current page entity.
   *
   * @return array|bool|\Drupal\Core\Entity\EntityInterface|mixed|null
   */
  public function getPageEntity() {
    $page_entity = &drupal_static(__FUNCTION__, NULL);
    if (isset($page_entity)) {
      return $page_entity ?: NULL;
    }
    $current_route = $this->routeMatch;
    foreach ($current_route->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $page_entity = $param;
        break;
      }
    }
    if (!isset($page_entity)) {
      // Some routes don't properly define entity parameters.
      // Thus, try to load them by its raw Id, if given.
      $entity_type_manager = $this->entityTypeManager;
      $types = $entity_type_manager->getDefinitions();
      foreach ($current_route->getParameters()->keys() as $param_key) {
        if (!isset($types[$param_key])) {
          continue;
        }
        if ($param = $current_route->getParameter($param_key)) {
          if (is_string($param) || is_numeric($param)) {
            try {
              $page_entity = $entity_type_manager->getStorage($param_key)
                ->load($param);
            } catch (\Exception $e) {
            }
          }
          break;
        }
      }
    }
    if (!isset($page_entity) || !$page_entity->access('view')) {
      $page_entity = FALSE;
      return NULL;
    }
    return $page_entity;
  }

}
