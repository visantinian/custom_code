<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pg_country\Entity\PgCountry;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGCountryAutoBlock.
 *
 * @Block(
 *   id = "pg_country_auto_block",
 *   admin_label = @Translation("PG Country Auto Block"),
 * )
 */
class PGCountryAutoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * PGCountryAutoBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if (!$page_entity = $this->getPageEntity()) {
      return FALSE;
    }
    else {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      if ($page_entity->hasTranslation($langcode)) {
        $page_entity = $page_entity->getTranslation($langcode);
      }

      // Get paragraph components.
      $components = FALSE;
      if ($page_entity instanceof PgCountry) {
        if ($langcode && $page_entity->hasTranslation($langcode)) {
          $page_entity = $page_entity->getTranslation($langcode);
        }

        $cache_key = 'pg_country-' . $page_entity->id();
        $components = $page_entity->get('country_components')->getValue();

        $country_id = $page_entity->id();
        $country_title = $page_entity->get('title')->getString();
        $entity_sec_name = $page_entity->get('country_name_sec')->getString();

        // Map info.
        $map_title = $entity_sec_name != '' ? $this->t('Карта') . ' ' . $entity_sec_name : $country_title . ' - ' . $this->t('на карте');
        $map = $page_entity->get('country_coordinates')->view('full');
        $map[0]['#height'] = '100%';

        // Attractions info.
        $attractions_title = $entity_sec_name != '' ? $this->t('Достопримечательности') . ' ' . $entity_sec_name : $country_title . ' - ' . $this->t('достопримечательности');

      }

      $auto_components = NULL;
      if ($components && count($components) > 0) {

        if (count($components) == 1) {
          $pid = $components[0]['target_id'];
          $paragraph = $this->entityTypeManager->getStorage('paragraph')
            ->load($pid);
          if ($paragraph->bundle() == 'general_information') {
            $auto_components = [
              'general_information' => $page_entity->get('country_components')
                ->view('full'),
              'country_id' => $country_id,
              'country_name' => $country_title,
              'name_sec' => $entity_sec_name,
              'simply_map' => [
                'title' => $map_title,
                'map' => $map,
              ],
              'attractions' => [
                'title' => $attractions_title,
              ],
            ];
          }
        }
      }
      else {

        if ($page_entity instanceof PgCountry) {
          $auto_components = [
            'country_id' => $country_id,
            'country_name' => $country_title,
            'name_sec' => $entity_sec_name,
            'simply_map' => [
              'title' => $map_title,
              'map' => $map,
            ],
            'attractions' => [
              'title' => $attractions_title,
            ],
          ];
        }
      }

      $block = [
        '#theme' => 'pg_country_auto_block',
        '#components' => $auto_components,
        '#cache' => [
          'contexts' => ['languages', 'url.path', 'url.query_args'],
          'keys' => ['block', $this->getPluginId(), $cache_key],
          'tags' => [$cache_key],
        ],
      ];

      return $block;
    }

  }

  /**
   * Helper function for get current page entity.
   *
   * @return array|bool|\Drupal\Core\Entity\EntityInterface|mixed|null
   */
  public function getPageEntity() {
    $page_entity = &drupal_static(__FUNCTION__, NULL);
    if (isset($page_entity)) {
      return $page_entity ?: NULL;
    }
    $current_route = $this->routeMatch;
    foreach ($current_route->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $page_entity = $param;
        break;
      }
    }
    if (!isset($page_entity)) {
      // Some routes don't properly define entity parameters.
      // Thus, try to load them by its raw Id, if given.
      $entity_type_manager = $this->entityTypeManager;
      $types = $entity_type_manager->getDefinitions();
      foreach ($current_route->getParameters()->keys() as $param_key) {
        if (!isset($types[$param_key])) {
          continue;
        }
        if ($param = $current_route->getParameter($param_key)) {
          if (is_string($param) || is_numeric($param)) {
            try {
              $page_entity = $entity_type_manager->getStorage($param_key)
                ->load($param);
            } catch (\Exception $e) {
            }
          }
          break;
        }
      }
    }
    if (!isset($page_entity) || !$page_entity->access('view')) {
      $page_entity = FALSE;
      return NULL;
    }
    return $page_entity;
  }

}
