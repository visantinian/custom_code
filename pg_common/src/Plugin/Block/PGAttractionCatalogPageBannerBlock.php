<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGAttractionCatalogPageBannerBlock.
 *
 * @Block(
 *   id = "pg_attraction_catalog_page_banner_block",
 *   admin_label = @Translation("PG Attraction Catalog Page Banner Block"),
 * )
 */
class PGAttractionCatalogPageBannerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * RMPromotionalBadgesBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Current langcode.
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    // Load catalog settings.
    $banner = NULL;
    $banner_settings = $this->entityTypeManager->getStorage('config_pages');
    $banner_entity = $banner_settings->load('catalog_page_settings');
    if ($banner_entity instanceof ConfigPages) {
      // Loading current language.
      if ($banner_entity->hasTranslation($langcode)) {
        $banner_entity = $banner_entity->getTranslation($langcode);
      }

      $banner = $banner_entity->get('attractions_catalog_banner')->view('full');
    }

    $block = [
      '#theme' => 'pg_attraction_catalog_page_banner_block',
      '#banner' => $banner,
      '#cache' => [
        'contexts' => ['languages', 'url.path'],
        'keys' => ['block', $this->getPluginId()],
      ],
    ];

    return $block;
  }

}
