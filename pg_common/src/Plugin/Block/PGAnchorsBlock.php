<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pg_attraction\PgAttractionInterface;
use Drupal\pg_city\PgCityInterface;
use Drupal\pg_country\PgCountryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides PGAnchorsBlock.
 *
 * @Block(
 *   id = "pg_anchors_block",
 *   admin_label = @Translation("PG Anchors Block"),
 * )
 */
class PGAnchorsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * RMPromotionalBadgesBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    if (!$page_entity = $this->getPageEntity()) {
      return FALSE;
    }
    else {
      $langcode = $this->languageManager->getCurrentLanguage()->getId();

      if ($page_entity->hasTranslation($langcode)) {
        $page_entity = $page_entity->getTranslation($langcode);
      }

      // Get paragraph components.
      $components = FALSE;
      $id = $page_entity->id();
      $type = $page_entity->getEntityTypeId();
      $cache_key = $type . ':' . $id;
      if ($page_entity instanceof PgCityInterface) {
        $components = $page_entity->get('city_components')->getValue();
      }
      if ($page_entity instanceof PgCountryInterface) {
        $components = $page_entity->get('country_components')->getValue();
      }
      if ($page_entity instanceof PgAttractionInterface) {
        $components = $page_entity->get('attraction_components')->getValue();
      }

      $anchors_list = [];
      if ($components && count($components) > 0) {

        // Get all paragraphs ID's and load.
        $paragraphs_revision_ids = array_values(array_column($components ?: [], 'target_revision_id'));
        $paragraphs = $this->entityTypeManager->getStorage('paragraph')
          ->loadMultipleRevisions($paragraphs_revision_ids);

        if (count($paragraphs) > 0) {
          $transliteration = $this->transliteration;
          foreach ($paragraphs as $key => $paragraph) {
            $anchor_link = NULL;
            if ($paragraph instanceof Paragraph && $paragraph->hasField('anchor')) {
              if ($paragraph->hasTranslation($langcode)) {
                $paragraph = $paragraph->getTranslation($langcode);
                $anchor = $paragraph->get('anchor')->getString();
                if ($anchor != '') {
                  // Create anchor link from clear, transliterated string.
                  $anchor_url = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $transliteration->transliterate($anchor, 'en', ''))) . '-' . $paragraph->id();
                  $url = Url::fromUserInput('#c-anchor-' . $anchor_url);
                  $anchor_link = Link::fromTextAndUrl(t($anchor), $url)
                    ->toRenderable();
                  $anchor_link['#attributes'] = ['class' => 'c-anchor__link'];
                  $anchors_list[] = $anchor_link;
                }
              }
            }
          }
        }
      }

      $block = [
        '#theme' => 'pg_anchors_block',
        '#anchors_list' => $anchors_list,
        '#cache' => [
          'contexts' => ['languages', 'url.path', 'url.query_args'],
          'keys' => ['block', $this->getPluginId(), $type, $id, $cache_key],
          'tags' => [$cache_key],
        ],
      ];

      return $block;
    }

  }

  /**
   * Helper function for get current page entity.
   *
   * @return array|bool|\Drupal\Core\Entity\EntityInterface|mixed|null
   */
  public function getPageEntity() {
    $page_entity = &drupal_static(__FUNCTION__, NULL);
    if (isset($page_entity)) {
      return $page_entity ?: NULL;
    }
    $current_route = $this->routeMatch;
    foreach ($current_route->getParameters() as $param) {
      if ($param instanceof EntityInterface) {
        $page_entity = $param;
        break;
      }
    }
    if (!isset($page_entity)) {
      // Some routes don't properly define entity parameters.
      // Thus, try to load them by its raw Id, if given.
      $entity_type_manager = $this->entityTypeManager;
      $types = $entity_type_manager->getDefinitions();
      foreach ($current_route->getParameters()->keys() as $param_key) {
        if (!isset($types[$param_key])) {
          continue;
        }
        if ($param = $current_route->getParameter($param_key)) {
          if (is_string($param) || is_numeric($param)) {
            try {
              $page_entity = $entity_type_manager->getStorage($param_key)
                ->load($param);
            } catch (\Exception $e) {
            }
          }
          break;
        }
      }
    }
    if (!isset($page_entity) || !$page_entity->access('view')) {
      $page_entity = FALSE;
      return NULL;
    }
    return $page_entity;
  }

}
