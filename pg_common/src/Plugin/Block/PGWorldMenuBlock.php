<?php

namespace Drupal\pg_common\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGWorldMenuBlock.
 *
 * @Block(
 *   id = "pg_world_menu_block",
 *   admin_label = @Translation("PG World Menu Block"),
 * )
 */
class PGWorldMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * RMPromotionalBadgesBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

//    $langcode = $this->languageManager->getCurrentLanguage()->getId();
//
//    // Load continents.
//    $continents_array = $this->entityTypeManager->getStorage('pg_continent')
//      ->loadByProperties(['status' => '1']);
//    $continents = $this->entityTypeManager->getViewBuilder('pg_continent')
//      ->viewMultiple($continents_array, 'link', $langcode);
//
//    // Load countries.
//    $countries_array = $this->entityTypeManager->getStorage('pg_country')
//      ->loadByProperties(['status' => '1']);
//    $countries = $this->entityTypeManager->getViewBuilder('pg_country')
//      ->viewMultiple($countries_array, 'link', $langcode);
//
//    // Load cities.
//    $cities_query = $this->entityTypeManager->getStorage('pg_city')->getQuery();
//    $cities_query->condition('viator_id', '0', '>');
//    $cities_query->condition('status', '1');
//    $cities_query->sort('freq', 'DESC');
//    $cities_ids = $cities_query->execute();
//    $cities_array = $this->entityTypeManager->getStorage('pg_city')
//      ->loadMultiple($cities_ids);
//    $cities = $this->entityTypeManager->getViewBuilder('pg_city')
//      ->viewMultiple($cities_array, 'link', $langcode);
//
//    $block = [
//      '#theme' => 'pg_world_menu_block',
//      '#continents' => $continents,
//      '#countries' => $countries,
//      '#cities' => $cities,
//      '#cache' => [
//        'contexts' => ['languages', 'url.path'],
//        'keys' => ['block', $this->getPluginId()],
//      ],
//    ];
//
//    return $block;
    return [];
  }

}
