<?php

namespace Drupal\pg_common\Breadcrumb;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;

/**
 * Adds the current page title to the breadcrumb.
 *
 * Extend PathBased Breadcrumbs to include the current page title as an unlinked
 * crumb. The module uses the path if the title is unavailable and it excludes
 * all admin paths.
 *
 * {@inheritdoc}
 */
class PGBlogPageBreadcrumbBuilder extends PGCommonBreadcrumbBuilderBase {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * RMCommonBreadcrumbBuilderBase constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $routeMatch, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($routeMatch);
    $this->entityTypeManager = $entityTypeManager;
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $node = $this->routeMatch->getParameter('node');
    if ($node instanceof NodeInterface && $node->getType() === 'blog_post') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    $blog_url = Url::fromUserInput('/blog');
    $breadcrumb->addLink(Link::fromTextAndUrl($this->t('Blog'), $blog_url));

    $parameters = $route_match->getParameters()->all();

    $node = $parameters['node'];

    $node_category = $node->get('bp_tags')->getValue();
    $node_category_id = $node_category[0]['target_id'];
    $node_langcode = $node->language()->getId();

    // Add current taxonomy term in breadcrumb.
    if ($node_category_id > 0) {
      // Breadcrumb needs to have terms cacheable metadata as a cacheable
      // dependency even though it is not shown in the breadcrumb because e.g. its
      // parent might have changed.
      $breadcrumb->addCacheableDependency($node);

      // Load all category parents.
      $parents = $this->termStorage->loadAllParents($node_category_id);
      foreach (array_reverse($parents) as $term) {
        if ($term instanceof TermInterface) {
          if ($term->hasTranslation($node_langcode)) {
            $term = $term->getTranslation($node_langcode);
            $breadcrumb->addCacheableDependency($term);
            $breadcrumb->addLink(Link::createFromRoute($term->getName(), 'entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()]));
          }
        }
      }

    }

    // Add current node to breadcrumb.
    if ($node instanceof NodeInterface) {
      $breadcrumb->addLink(Link::createFromRoute($node->label(), '<none>'));
    }

    // Add the full URL path as a cache context, since we will display the
    // current page as part of the breadcrumb.
    $breadcrumb->addCacheContexts(['url.path']);

    return $breadcrumb;
  }

}
