<?php

namespace Drupal\pg_common\Breadcrumb;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class PGBreadcrumbBuilderBase.
 */
abstract class PGCommonBreadcrumbBuilderBase implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * PGCommonBreadcrumbBuilderBase constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The request stack.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function applies(RouteMatchInterface $route_match);

  /**
   * {@inheritdoc}
   */
  abstract public function build(RouteMatchInterface $route_match);

}
