<?php

namespace Drupal\pg_common\Breadcrumb;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\pg_continent\Entity\PgContinent;
use Drupal\pg_country\Entity\PgCountry;

/**
 * Adds the current page title to the breadcrumb.
 *
 * Extend PathBased Breadcrumbs to include the current page title as an unlinked
 * crumb. The module uses the path if the title is unavailable and it excludes
 * all admin paths.
 *
 * {@inheritdoc}
 */
class PGCountryPageBreadcrumbBuilder extends PGCommonBreadcrumbBuilderBase {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * PGCommonBreadcrumbBuilderBase constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $routeMatch, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $language_manager) {
    parent::__construct($routeMatch);
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $pg_country = $this->routeMatch->getParameter('pg_country');
    if ($pg_country instanceof PgCountry && $pg_country->getEntityTypeId() === 'pg_country') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $parameters = $route_match->getParameters()->all();
    $pg_country = $parameters['pg_country'];

    // Add country and continent in breadcrumb.
    if ($pg_country instanceof PgCountry && $pg_country->hasField('country_continent')) {
      $pg_continent_id = $pg_country->get('country_continent')->getString();

      if ($pg_continent_id > 0) {
        // Breadcrumb needs to have cacheable metadata as a cacheable
        // dependency even though it is not shown in the breadcrumb because e.g. its
        // parent might have changed.
        $breadcrumb->addCacheableDependency($pg_country);

        $pg_continent = $this->entityTypeManager->getStorage('pg_continent')
          ->load($pg_continent_id);
        if ($pg_continent instanceof PgContinent && $pg_continent->hasTranslation($langcode)) {
          $pg_continent = $pg_continent->getTranslation($langcode);
        }

        if ($pg_continent instanceof PgContinent) {
          $breadcrumb->addCacheableDependency($pg_continent);
          $breadcrumb->addLink(Link::createFromRoute($pg_continent->label(), 'entity.pg_continent.canonical', ['pg_continent' => $pg_continent->id()]));
        }

      }
    }

    // Add current city to breadcrumb.
    if ($pg_country instanceof PgCountry) {
      $breadcrumb->addLink(Link::createFromRoute($pg_country->label(), '<none>'));
    }

    // Add the full URL path as a cache context, since we will display the
    // current page as part of the breadcrumb.
    $breadcrumb->addCacheContexts(['url.path']);

    return $breadcrumb;
  }

}
