<?php

namespace Drupal\pg_common\Breadcrumb;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\pg_continent\Entity\PgContinent;

/**
 * Adds the current page title to the breadcrumb.
 *
 * Extend PathBased Breadcrumbs to include the current page title as an unlinked
 * crumb. The module uses the path if the title is unavailable and it excludes
 * all admin paths.
 *
 * {@inheritdoc}
 */
class PGContinentPageBreadcrumbBuilder extends PGCommonBreadcrumbBuilderBase {

  /**
   * The request stack.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * PGCommonBreadcrumbBuilderBase constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $routeMatch, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $language_manager) {
    parent::__construct($routeMatch);
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $pg_continent = $this->routeMatch->getParameter('pg_continent');
    if ($pg_continent instanceof PgContinent && $pg_continent->getEntityTypeId() === 'pg_continent') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    $parameters = $route_match->getParameters()->all();
    $pg_continent = $parameters['pg_continent'];

    // Add current continent to breadcrumb.
    if ($pg_continent instanceof PgContinent) {
      $breadcrumb->addLink(Link::createFromRoute($pg_continent->label(), '<none>'));
    }

    // Add the full URL path as a cache context, since we will display the
    // current page as part of the breadcrumb.
    $breadcrumb->addCacheContexts(['url.path']);

    return $breadcrumb;
  }

}
