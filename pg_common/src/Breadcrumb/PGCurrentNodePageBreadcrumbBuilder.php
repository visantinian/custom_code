<?php

namespace Drupal\pg_common\Breadcrumb;

use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Adds the current page title to the breadcrumb.
 *
 * Extend PathBased Breadcrumbs to include the current page title as an unlinked
 * crumb. The module uses the path if the title is unavailable and it excludes
 * all admin paths.
 *
 * {@inheritdoc}
 */
class PGCurrentNodePageBreadcrumbBuilder extends PGCommonBreadcrumbBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $node = $this->routeMatch->getParameter('node');
    if ($node instanceof NodeInterface) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumbs = \Drupal::service('system.breadcrumb.default')->build($route_match);

    $parameters = $route_match->getParameters()->all();

    // Add current node to breadcrumb.
    $node = $parameters['node'];
    if ($node instanceof NodeInterface) {
      $breadcrumbs->addLink(Link::createFromRoute($node->label(), '<none>'));
    }

    // Add the full URL path as a cache context, since we will display the
    // current page as part of the breadcrumb.
    $breadcrumbs->addCacheContexts(['url.path']);

    return $breadcrumbs;
  }

}
