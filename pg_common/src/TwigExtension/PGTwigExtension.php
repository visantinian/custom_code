<?php

namespace Drupal\pg_common\TwigExtension;

/**
 * A class providing Extra Drupal Twig extensions.
 *
 * This provides a Twig extension that registers various Drupal-specific
 * extensions to Twig, specifically Twig functions, filter, and node visitors.
 *
 * @see \Drupal\Core\CoreServiceProvider
 */
class PGTwigExtension extends \Twig_Extension {

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('pg_clear_comments', [
        $this,
        'removeDebugComments',
      ]),
      new \Twig_SimpleFilter('pg_string_to_attr', [
        $this,
        'getAttributeFromString',
      ]),
      new \Twig_SimpleFilter('to_array',
        array($this,
          'to_array')
      ),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   */
  public function getName() {
    return 'pg_common.twig_extension';
  }

  public function to_array($object)
  {
    return get_object_vars($object);
  }

  /**
   * Removes html comments from string.
   */
  public static function removeDebugComments($string) {
    return trim(preg_replace('/<!--(.|\s)*?-->/', '', $string));
  }

  /**
   * Clear string for use as attribute.
   */
  public function getAttributeFromString($string) {
    return strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', \Drupal::transliteration()
      ->transliterate($string, 'en', '')));
  }

}
