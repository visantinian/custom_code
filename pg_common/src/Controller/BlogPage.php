<?php

namespace Drupal\pg_common\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the blog page.
 */
class BlogPage extends ControllerBase {

  /**
   * Returns a blog page.
   *
   * @return array
   *   A renderable array.
   */
  public function view() {
    $content = [];

    return $content;
  }

}
