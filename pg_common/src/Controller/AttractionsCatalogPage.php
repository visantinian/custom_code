<?php

namespace Drupal\pg_common\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Attractions Catalog page.
 */
class AttractionsCatalogPage extends ControllerBase {

  /**
   * Returns a Attractions Catalog page.
   *
   * @return array
   *   A renderable array.
   */
  public function view() {
    $content = [];

    return $content;
  }

}
