<?php

namespace Drupal\pg_search\Controller;

/**
 * @file
 * Contains \Drupal\pg_search\Controller\PGAutocompleteSearchController.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Item;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns autocomplete responses for countries.
 */
class PGAutocompleteSearchController extends ControllerBase {

  /**
   * Returns response for the agencies autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions for countries.
   */
  public function autocomplete(Request $request) {

    $suggestions = [];
    $keys = $request->query->get('q');
    $langcode = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);

    // Get string from search field and query by Country.
    $index = Index::load('country_index_autocomplete');
    $query = $index->query();
    $query->setLanguages([$langcode->getId()]);
    $query->setFulltextFields(['title']);
    $query->setOption('limit', 5);
    $query->keys($keys);
    $result_items = $query->execute()->getResultItems();

    // Get string from search field and query by City.
    $index = Index::load('city_index_autocomplete');
    $query = $index->query();
    $query->setLanguages([$langcode->getId()]);
    $query->setFulltextFields(['title']);
    $query->setOption('limit', 5);
    $query->keys($keys);
    $result_items += $query->execute()->getResultItems();

    // Get string from search field and query by Attractions.
    $index = Index::load('attraction_index_autocomplete');
    $query = $index->query();
    $query->setLanguages([$langcode->getId()]);
    $query->setFulltextFields(['title']);
    $query->setOption('limit', 5);
    $query->keys($keys);
    $result_items += $query->execute()->getResultItems();

    foreach ($result_items as $result_item) {
      if ($result_item instanceof Item) {
        // Get title and create top label part.
        $topLabel = NULL;

        $title_highlighted = $result_item->getExtraData('highlighted_fields');
        if (!empty($title_highlighted) && isset($title_highlighted['title'])) {
          $title = $title_highlighted['title'][0];

          $title_text = $result_item->getField('title')->getValues();
          if (!empty($title_text) && isset($title_text[0])) {
            $title_text = $title_text[0];
          }
        }

        $searchResultsURL = $result_item->getField('url')->getValues();
        if (!empty($searchResultsURL) && isset($searchResultsURL[0])) {
          $url = $searchResultsURL[0];
        }

        if (!empty($title) && isset($title)) {
          // Create html label.
          $label = [
            '#theme' => 'guide__search_results_label',
            '#title' => [
              'highlighted' => $title,
              'title_text' => $title_text
            ],
            '#url' => $url,
          ];

          $suggestions[] = [
            'label' => render($label)
          ];
        }
      }
    }

    return new JsonResponse($suggestions);
  }

}
