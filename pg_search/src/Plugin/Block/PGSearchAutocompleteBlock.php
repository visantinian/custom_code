<?php

namespace Drupal\pg_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Transliteration\TransliterationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides PGSearchAutocompleteBlock.
 *
 * @Block(
 *   id = "pg_search_autocomplete_block",
 *   admin_label = @Translation("PG Search Autocomplete Block"),
 * )
 */
class PGSearchAutocompleteBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The transliteration service.
   *
   * @var \Drupal\Component\Transliteration\TransliterationInterface
   */
  protected $transliteration;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * PGSearchAutocompleteBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TransliterationInterface $transliteration, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->transliteration = $transliteration;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('transliteration'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Get Build PGSearchAutocompleteForm form.
    $formSearch = \Drupal::formBuilder()
      ->getForm('Drupal\pg_search\Form\PGSearchAutocompleteForm');
    $formCacheContexts = $formSearch['#cache']['contexts'];

    $block = [
      '#search_block' => [
        'search_form' => $formSearch,
      ],
      '#theme' => 'pg_search_autocomplete_block',
      '#cache' => [
        'contexts' => Cache::mergeContexts([
          'languages',
          'url.path',
          'url.query_args',
        ], $formCacheContexts),
        'keys' => ['block', $this->getPluginId()],
      ],
    ];

    return $block;
  }

}
