<?php

namespace Drupal\pg_search\Plugin\facets\widget;

use Drupal\facets\Plugin\facets\widget\CheckboxWidget;

/**
 * Widget class that returns a Checkbox widget for facets.
 *
 * @FacetsWidget(
 *   id = "pg_checkbox_widget",
 *   label = @Translation("PG List of Checkbox"),
 *   description = @Translation("A configurable widget that shows a list of
 *   checkboxes."),
 * )
 */
class PGCheckboxWidget extends CheckboxWidget {

  /**
   * {@inheritdoc}
   */
  protected function appendWidgetLibrary(array &$build) {
    $build['#attributes']['class'][] = 'js-facets-checkbox-links';
    $build['#attached']['library'][] = 'pg_search/pg.facets.checkbox-widget';
  }

}
