<?php

namespace Drupal\pg_search\Form;

/**
 * @file
 * Contains \Drupal\pg_search\Form\PGSearchAutocompleteForm.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Search autocomplete form.
 */
class PGSearchAutocompleteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pg_search_autocomplete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'c-form';
    $form['#attributes']['class'][] = 'c-search__form';

    $form['search'] = [
      '#type' => 'search',
      '#title' => '',
      '#placeholder' => $this->t('Куда вы направляетесь?'),
      '#autocomplete_route_name' => 'pg_search_autocomplete.autocomplete',
      '#autocomplete_route_parameters' => [],
      '#theme_wrappers' => [],
    ];

    $form['#theme'] = 'pg_search_autocomplete_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
