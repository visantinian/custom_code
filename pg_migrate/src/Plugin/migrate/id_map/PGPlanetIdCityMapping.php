<?php

namespace Drupal\pg_migrate\Plugin\migrate\id_map;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\id_map\Sql;

/**
 * Defines the sql based ID map implementation.
 *
 * It creates one map and one message table per migration entity to store the
 * relevant information.
 *
 * @PluginID("pg_planet_id_city_mapping")
 */
class PGPlanetIdCityMapping extends Sql {

  /**
   * {@inheritdoc}
   */
  public function lookupDestinationId(array $source_id_values) {
    $results = $this->lookupDestinationIds($source_id_values);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function lookupDestinationIds(array $source_id_values) {
    if (empty($source_id_values)) {
      return [];
    }
    $source_ids = $source_id_values;
    // Now check if there are any existing with same IDs.
    if (isset($source_ids['pl_id'])) {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('pg_city')
        ->loadByProperties([
          'planet_id' => $source_ids['pl_id'],
        ]);
      if ($entities !== NULL) {
        return array_keys($entities);
      }
    }
    // Canonicalize the keys into a hash of DB-field => value.
    $is_associative = !isset($source_id_values[0]);
    $conditions = [];
    foreach ($this->sourceIdFields() as $field_name => $db_field) {
      if ($is_associative) {
        // Associative $source_id_values can have fields out of order.
        if (isset($source_id_values[$field_name])) {
          $conditions[$db_field] = $source_id_values[$field_name];
          unset($source_id_values[$field_name]);
        }
      }
      else {
        // For non-associative $source_id_values, we assume they're the first
        // few fields.
        if (empty($source_id_values)) {
          break;
        }
        $conditions[$db_field] = array_shift($source_id_values);
      }
    }
    if (!empty($source_id_values)) {
      throw new MigrateException("Extra unknown items in source IDs");
    }
    $query = $this->getDatabase()->select($this->mapTableName(), 'map')
      ->fields('map', $this->destinationIdFields());
    if (count($this->sourceIdFields()) === count($conditions)) {
      // Optimization: Use the primary key.
      $query->condition(self::SOURCE_IDS_HASH, $this->getSourceIDsHash(array_values($conditions)));
    }
    else {
      foreach ($conditions as $db_field => $value) {
        $query->condition($db_field, $value);
      }
    }
    $migrated_source_ids = $query->execute()->fetchAll(\PDO::FETCH_NUM);
    if (!empty($migrated_source_ids)) {
      return $migrated_source_ids;
    }
    return [];
  }
}