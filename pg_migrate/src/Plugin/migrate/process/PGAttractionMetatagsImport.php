<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\pg_attraction\PgAttractionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_attraction_metatags_import"
 * )
 */
class PGAttractionMetatagsImport extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $row_source = $row->getSource();
    $planet_id = $row_source['nid'];
    $langcode = $row_source['langcode'] ? $row_source['langcode'] : FALSE;
    $meta_title = $row_source['metatag_title'];
    $meta_desc = $row_source['metatag_desc'];

    $attraction = reset($this->entityTypeManager->getStorage('pg_attraction')
      ->loadByProperties(['planet_id' => $planet_id]));
    if ($attraction instanceof PgAttractionInterface && $langcode && $attraction->hasTranslation($langcode)) {
      $attraction = $attraction->getTranslation($langcode);

      if ($attraction->hasField('metatags')) {
        $existing_val = $attraction->get('metatags')->value;
        if (!empty($existing_val)) {
          $existing_val = unserialize($existing_val);
        }
        else {
          $existing_val = [];
        }
        $existing_val['title'] = $meta_title;
        $existing_val['description'] = $meta_desc;
        $attraction->set('metatags', serialize($existing_val));
        $attraction->save();
      }
    }

    return serialize($existing_val);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
