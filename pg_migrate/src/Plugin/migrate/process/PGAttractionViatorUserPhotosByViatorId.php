<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_get_viator_user_photos_by_viaror_id"
 * )
 */
class PGAttractionViatorUserPhotosByViatorId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $properties = is_string($source) ? [$source] : $source;
    $return = [];

    $row_source = $row->getSource();
    $viator_id = $row_source['va_seoId'];
    $order_img = $row_source['sortOrder'];
    if ($order_img == 1) {

      $combined_by_id = $row->combined_by_id[$viator_id];

      foreach ($properties as $property) {
        if ($property || (string) $property === '0') {
          $result = array_column($combined_by_id, $source);
          $return[] = $result;
        }
        else {
          $return[] = $value;
        }
      }

      if (is_string($source)) {
        $this->multiple = is_array($return[0]);
        return $return[0];
      }

    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
