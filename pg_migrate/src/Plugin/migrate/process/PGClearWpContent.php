<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_clear_wp_content"
 * )
 */
class PGClearWpContent extends ProcessPluginBase {

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $properties = is_string($source) ? [$source] : $source;
    $return = [];

    $value = preg_replace('/\[caption.*?\[\/caption\]/', '', $value, 1);
    $value = preg_replace('/\[caption[^\]]*\]/', '', $value, -1);
    $value = preg_replace('/\[\/caption[^\]]*\]/', '', $value, -1);
    $value = preg_replace('/Фото:/', '<br>Фото:', $value, -1);

    foreach ($properties as $property) {
      if ($property || (string) $property === '0') {
        $result = $row->get($property);
        $result = preg_replace('/\[caption.*?\[\/caption\]/', '', $result, 1);
        $result = preg_replace('/\[caption[^\]]*\]/', '', $result, -1);
        $result = preg_replace('/\[\/caption[^\]]*\]/', '', $result, -1);
        $result = preg_replace('/Фото:/', '<br>Фото:', $result, -1);
        $return[] = $result;
      }
      else {
        $return[] = $value;
      }
    }

    if (is_string($source)) {
      $this->multiple = is_array($return[0]);
      return $return[0];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
