<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\geofield\WktGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pg_country\PgCountryInterface;
use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;

/**
 * Process latitude and longitude and return the value for the D8 geofield.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_geofield_latlon_by_country_id"
 * )
 */
class PGGeofieldLatLonByCountryId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The WktGenerator service.
   *
   * @var \Drupal\geofield\WktGeneratorInterface
   */
  protected $wktGenerator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WktGeneratorInterface $wkt_generator, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->wktGenerator = $wkt_generator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geofield.wkt_generator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $row_source = $row->getSource();
    $name_en = $row_source['name_en'];

    if (is_numeric($value) && $name_en != '') {
      $entity = $this->entityTypeManager->getStorage('pg_country')
        ->load($value);
    }

    // If country exist.
    if ($entity instanceof PgCountryInterface) {

      $general_info = $this->getCountryGeneralInformation($name_en);
      $coordinates = $entity->get('country_coordinates')->getValue();

      if (count($general_info) > 0 && !empty($general_info['lat']) && !empty($general_info['lon'])) {
        if (!empty($coordinates)) {
          throw new MigrateSkipRowException();
        }
        else {
          $lat = $general_info['lat'];
          $lon = $general_info['lon'];
        }
      }
    }

    if (empty($lat) || empty($lon)) {
      return NULL;
    }

    return $this->wktGenerator->WktBuildPoint([$lon, $lat]);
  }

  /*
   * Helper for get Lat and Lon by country name.
   */
  public function getCountryGeneralInformation($country_name) {
    $endpoint = 'https://restcountries.eu/rest/v2/name/' . $country_name;

    usleep(1000000);
    $client = new Client([
      'base_uri' => $endpoint,
      'http_errors' => FALSE,
      'timeout' => 5,
    ]);
    $request = $client->request('GET');

    $general_info = [];
    if ($request->getStatusCode() == 200) {
      $response = Json::decode($request->getBody());

      // Lat and Lon.
      if (isset($response[0]) && isset($response[0]['latlng'])) {
        $general_info['lat'] = $response[0]['latlng'][0];
        $general_info['lon'] = $response[0]['latlng'][1];
      }

    }

    return $general_info;
  }

}
