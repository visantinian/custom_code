<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Component\Serialization\Json;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\pg_api\GTranslateService;
use Drupal\pg_country\PgCountryInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Gets the source value.
 *
 * Value should be country ID.
 * General information paragraph update, for countries what already have
 * content in country_components.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_restcountries_general_information_update"
 * )
 */
class PGRestcountriesGeneralInformationUpdate extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * Google translate service.
   *
   * @var \Drupal\pg_api\GTranslateService
   */
  protected $gTranslate;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager, GTranslateService $g_translate) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
    $this->gTranslate = $g_translate;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('pg_api.gtranslate')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $pid = NULL;

    $row_source = $row->getSource();
    $name_en = $row_source['name_en'];
    $langcode = $row_source['langcode'] ? $row_source['langcode'] : FALSE;

    if (is_numeric($value) && $name_en != '') {
      $entity = $this->entityTypeManager->getStorage('pg_country')
        ->load($value);
    }

    if ($entity instanceof PgCountryInterface) {

      if ($entity->hasTranslation($langcode)) {
        $entity = $entity->getTranslation($langcode);
      }

      $components = $entity->get('country_components')->getValue();

      if ($components && count($components) > 0) {
        foreach ($components as $component) {
          $pid = $component['target_id'];
          $paragraph = $this->entityTypeManager->getStorage('paragraph')
            ->load($pid);
          $general_info = $this->getCountryGeneralInformation($name_en, $langcode);
          if (count($general_info) > 0 && $paragraph instanceof ParagraphInterface && $paragraph->bundle() == 'general_information') {
            if ($paragraph->hasTranslation($langcode)) {
              $paragraph->set('gi_capital', $general_info['capital']);
              $paragraph->set('gi_population', $general_info['population']);
              $paragraph->set('gi_territory', $general_info['area']);
              $paragraph->set('gi_language', $general_info['languages']);
              $paragraph->set('gi_currency', $general_info['currencies']);
              $paragraph->save();
            }
          }
        }
      }

    }
    else {
      throw new MigrateSkipRowException();
    }
    return $pid;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

  public function getCountryGeneralInformation($country_name, $langcode = 'ru') {

    $endpoint = 'https://restcountries.eu/rest/v2/name/' . $country_name;

    $client = new Client([
      'base_uri' => $endpoint,
      'http_errors' => FALSE,
      'timeout' => 5,
    ]);
    $request = $client->request('GET');

    $general_info = [];
    if ($request->getStatusCode() == 200) {
      $response = Json::decode($request->getBody());

      // Capital.
      if (isset($response[0]) && isset($response[0]['capital'])) {
        $general_info['capital'] = $response[0]['capital'];
        /*if ($capital = $this->gTranslate->translate('en', $langcode, $general_info['capital'])) {
          $general_info['capital'] = $capital;
        }*/
      }

      // Population.
      if (isset($response[0]) && isset($response[0]['population'])) {
        $general_info['population'] = $response[0]['population'];
        if (is_numeric($response[0]['population'])) {
          $general_info['population'] = number_format($response[0]['population'], 0, '', ' ');
        }
      }

      // Territory.
      if (isset($response[0]) && isset($response[0]['area'])) {
        $general_info['area'] = $response[0]['area'];
        if (is_numeric($response[0]['area'])) {
          $general_info['area'] = number_format($response[0]['area'], 0, '', ' ');
        }
      }

      // Languages.
      if (isset($response[0]) && isset($response[0]['languages'])) {
        $languages = $response[0]['languages'];
        if (count($languages) > 0) {
          $languages_info = [];
          foreach ($languages as $language) {
            $languages_info[] = $language['name'];
          }
          $general_info['languages'] = implode(', ', $languages_info);
          /*if ($languages = $this->gTranslate->translate('en', $langcode, $general_info['languages'])) {
            $general_info['languages'] = $languages;
          }*/
        }
      }

      // Currencies
      if (isset($response[0]) && isset($response[0]['currencies'])) {
        $currencies = $response[0]['currencies'];
        if (count($currencies) > 0) {
          $currencies_info = [];
          foreach ($currencies as $language) {
            $currencies_info[] = $language['code'];
          }
          $general_info['currencies'] = implode(', ', $currencies_info);
        }
      }

    }

    return $general_info;
  }

}
