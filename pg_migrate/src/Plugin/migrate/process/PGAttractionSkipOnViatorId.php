<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\pg_attraction\PgAttractionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Skip if attraction is planet
 * but mapped to viator.
 * When plant_id and voator_id isset.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_skip_on_viator_id"
 * )
 */
class PGAttractionSkipOnViatorId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $properties = is_string($source) ? [$source] : $source;
    $return = [];

    $attraction_storage = $this->entityTypeManager->getStorage('pg_attraction');
    $attraction_planet_viator = $attraction_storage->getQuery()
      ->condition('viator_id', $value)
      ->condition('planet_id', 0, '>')
      ->count()
      ->execute();
    if ($attraction_planet_viator > 0) {
      throw new MigrateSkipRowException();
    }
    else {
      foreach ($properties as $property) {
        if ($property || (string) $property === '0') {
          $return[] = $value;
        }
        else {
          $return[] = $value;
        }
      }
    }

    if (is_string($source)) {
      $this->multiple = is_array($return[0]);
      return $return[0];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
