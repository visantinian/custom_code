<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\node\Entity\Node;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_clear_wp_metatags"
 * )
 */
class PGClearWpMetatags extends ProcessPluginBase {

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $map_id = $row->getIdMap();
    $nid = $map_id['destid1'];
    $langcode = $map_id['destid2']  ? $map_id['destid2'] : FALSE;

    $node = Node::load($nid);
    if($langcode && $node->hasTranslation($langcode)){
      $node = $node->getTranslation($langcode);
    }
    $row_source = $row->getSource();

    $meta_title = $row_source['metatag_title'];
    $meta_desc = $row_source['metatag_desc'];

    if ($node instanceof Node && $node->hasField('metatags')) {
      $existing_val = $node->get('metatags')->value;
      if (!empty($existing_val)) {
        $existing_val = unserialize($existing_val);
      }
      else {
        $existing_val = [];
      }
      $existing_val['title'] = $meta_title;
      $existing_val['description'] = $meta_desc;
      $node->set('metatags', serialize($existing_val));
      $node->save();
    }

    return serialize($existing_val);
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
