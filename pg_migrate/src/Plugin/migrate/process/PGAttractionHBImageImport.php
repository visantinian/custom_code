<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrateProcessInterface;
use Drupal\migrate\Row;
use Drupal\file\Entity\File;
use Drupal\migrate_file\Plugin\migrate\process\FileImport;

/**
 * Imports an image from an local or external source.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_attraction_hb_image_import"
 * )
 */
class PGAttractionHBImageImport extends FileImport {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, StreamWrapperManagerInterface $stream_wrappers, FileSystemInterface $file_system, MigrateProcessInterface $download_plugin) {
    $configuration += [
      'title' => NULL,
      'alt' => NULL,
      'width' => NULL,
      'height' => NULL,
    ];
    parent::__construct($configuration, $plugin_id, $plugin_definition, $stream_wrappers, $file_system, $download_plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $image = str_replace('public://', 'https://planetofhotels.com/sites/default/files/styles/attractionimageoriginal/public/', $value);
    $image = str_replace('.jpg', '_0.jpg', $image);
    if (!getimagesize($image)) {
      $row_source = $row->getSource();
      $image = str_replace('public://', 'https://planetofhotels.com/sites/default/files/styles/attractionimageoriginal/public/', $row_source['field_image']);
      $image = str_replace('.jpg', '_0.jpg', $image);
    }
    if (getimagesize($image)) {
      $file = parent::transform($image, $migrate_executable, $row, $destination_property);
      $file = $this->getFileFromValue($file, $row);
    }

    return $file;
  }

  /**
   * Return File value with alt and title.
   *
   * @param string $value
   *   Url of the image.
   * @param \Drupal\migrate\Row $row
   *   Row of the migration.
   *
   * @return array
   *   Return the file array.
   */
  public function getFileFromValue($value, Row $row) {
    if ($value) {
      if (!is_array($value)) {
        $data = $value;
        $value = [];
        $value['target_id'] = $data;
      }
      // Add the image field specific sub fields.
      foreach (['title', 'alt', 'width', 'height'] as $key) {
        if ($property = $this->configuration[$key]) {
          if ($property == '!file') {
            $file = File::load($value['target_id']);
            $value[$key] = $file->getFilename();
          }
          else {
            $value[$key] = $this->getPropertyValue($property, $row);
          }
        }
      }
      return $value;
    }
    else {
      return NULL;
    }

  }

}
