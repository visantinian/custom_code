<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Component\Serialization\Json;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_restcountries_population"
 * )
 */
class PGRestcountriesPopulation extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $return = '';

    $endpoint = 'https://restcountries.eu/rest/v2/name/' . $value;

    $client = new Client([
      'base_uri' => $endpoint,
      'http_errors' => FALSE,
      'timeout' => 5,
    ]);
    $request = $client->request('GET');

    if ($request->getStatusCode() == 200) {
      $response = Json::decode($request->getBody());

      if (isset($response[0]) && isset($response[0]['population'])) {
        if (is_numeric($response[0]['population'])) {
          $return = number_format($response[0]['population'], 0, '', ' ');
        }
      }
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
