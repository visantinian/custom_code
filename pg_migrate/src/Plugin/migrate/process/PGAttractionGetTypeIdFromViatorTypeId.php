<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\pg_attraction\Entity\PgAttraction;
use Drupal\pg_attraction\PgAttractionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_attraction_get_type_id_from_viator_type_id"
 * )
 */
class PGAttractionGetTypeIdFromViatorTypeId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $properties = is_string($source) ? [$source] : $source;
    $return = [];

    $row_source = $row->getSource();
    $viator_id = $row_source['va_seoId'];

    $combined_by_id = $row->combined_by_id[$viator_id];
    $viator_type_ids = array_column($combined_by_id, $source);

    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    foreach ($properties as $property) {
      if ($property || (string) $property === '0') {
        $attraction_ids = array_keys($term_storage->loadByProperties(['planet_id' => $viator_type_ids]));
        $return[] = $attraction_ids;
      }
      else {
        $attraction_ids = array_keys($term_storage->loadByProperties(['planet_id' => $viator_type_ids]));
        $return[] = $attraction_ids;
      }
    }

    if (is_string($source)) {
      $this->multiple = is_array($return[0]);
      return $return[0];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
