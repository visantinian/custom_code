<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_attraction_content_img_tag_path"
 * )
 */
class PGAttractionContentImgTagPath extends ProcessPluginBase {

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $properties = is_string($source) ? [$source] : $source;
    $return = [];

    $value = str_replace('src="/sites/default', 'src="https://planetofhotels.com/sites/default', $value);

    foreach ($properties as $property) {
      if ($property || (string) $property === '0') {
        $result = $row->get($property);
        $result = str_replace('src="/sites/default', 'src="https://planetofhotels.com/sites/default', $result);
        $return[] = $result;
      }
      else {
        $return[] = $value;
      }
    }

    if (is_string($source)) {
      $this->multiple = is_array($return[0]);
      return $return[0];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
