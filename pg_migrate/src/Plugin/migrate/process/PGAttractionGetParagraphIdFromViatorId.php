<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\Core\Database\Connection;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_get_paragraph_id_from_viator_id"
 * )
 */
class PGAttractionGetParagraphIdFromViatorId extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];
    $return = [];

    $row_source = $row->getSource();
    $paragraph_bundle = $row_source['paragraph_bundle'] ? $row_source['paragraph_bundle'] : FALSE;

    $paragraph_query = $this->database->select('paragraphs_item_field_data', 'p');
    $paragraph_query->leftjoin('pg_attraction__viator_id', 'a', 'p.parent_id=a.entity_id');
    $paragraph_query->condition('p.parent_type', 'pg_attraction')
      ->condition('p.type', $paragraph_bundle)
      ->condition('a.viator_id_value', $value)
      ->fields('p', ['id', 'parent_id']);
    $paragraph_data = $paragraph_query->execute()->fetchAssoc();

    if (!empty($paragraph_data) && count($paragraph_data) > 0 && $paragraph_data['id'] > 0) {
      $return[] = $paragraph_data['id'];
    }
    else {
      throw new MigrateSkipRowException();
    }

    if (is_string($source)) {
      $this->multiple = is_array($return[0]);
      return $return[0];
    }
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
