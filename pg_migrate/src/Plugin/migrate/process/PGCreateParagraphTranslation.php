<?php

namespace Drupal\pg_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Gets the source value.
 *
 * @MigrateProcessPlugin(
 *   id = "pg_create_paragraph_translation"
 * )
 */
class PGCreateParagraphTranslation extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Flag indicating whether there are multiple values.
   *
   * @var bool
   */
  protected $multiple;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $source = $this->configuration['source'];

    if ($destination_property == 'pg_create_paragraph_translation') {

      // Getting paragraph.
      $destination = $row->getDestination();
      $paragraph_id = is_array($destination['id']) ? $destination['id'][0] : $destination['id'];

      $row_source = $row->getSource();
      $langcode = $destination['langcode'];
      $anchor = $destination['anchor'];

      if ($langcode == '') {
        $langcode = $row_source['langcode'] ? $row_source['langcode'] : FALSE;
      }
      if ($langcode == '') {
        $langcode = 'en';
      }

      $paragraph = $this->entityTypeManager->getStorage('paragraph')
        ->load($paragraph_id);

      if ($paragraph instanceof ParagraphInterface) {

        // Getting fields from specific prepared field value.
        $fields_to_translate = explode(',', $source);
        if (is_array($fields_to_translate) && count($fields_to_translate) > 0) {
          if (!$paragraph->hasTranslation($langcode)) {
            // Create translation if not exist.
            $entity_array = $paragraph->toArray();
            $translated_fields = [];
            foreach ($fields_to_translate as $field) {
              if ($paragraph->hasField($field)) {
                // Preparing value - apply needed process.
                $processed_value = $this->pgValuePreprocess($row, $field, $row_source[$field], $migrate_executable, $langcode);
                $translated_fields[$field] = $processed_value;
              }
            }
            $translated_entity_array = array_merge($entity_array, $translated_fields);
            $paragraph->addTranslation($langcode, $translated_entity_array);
            $paragraph->save();
          }
          else {
            // Updating current translation.
            $paragraph = $paragraph->getTranslation($langcode);
            foreach ($fields_to_translate as $field) {
              if ($paragraph->hasField($field)) {
                $processed_value = $this->pgValuePreprocess($row, $field, $row_source[$field], $migrate_executable, $langcode);
                $paragraph->set($field, $processed_value);
                if ($anchor != '') {
                  $paragraph->set('anchor', $anchor);
                }
              }
            }
            $paragraph->save();
          }
        }

      }

      return TRUE;
    }
  }

  /**
   * Helper value preprocess function.
   */
  function pgValuePreprocess($row, $field, $value, $migrate_executable, $langcode) {

    switch ($field) {
      case 'text_description':
        $process = new PGAttractionContentImgTagPath($this->configuration, $this
          ->pluginId, $this->getPlc-banneruginDefinition());
        $value = [
          'value' => $process->transform($value, $migrate_executable, $row, $field),
          'format' => 'full_html',
        ];
        break;
      case 'simply_map_title':
        $process = new PGAttractionMapTitle($this->configuration, $this
          ->pluginId, $this->getPluginDefinition(), $this->entityTypeManager);
        $value = $process->transform($value, $migrate_executable, $row, $field);
        break;
      case 'hb_image':
        $destination = $row->getDestination();
        $paragraph_id = is_array($destination['id']) ? $destination['id'][0] : $destination['id'];
        $paragraph = $this->entityTypeManager->getStorage('paragraph')
          ->load($paragraph_id);
        $hb_image = $paragraph->get('hb_image')->getValue();
        $target_id = array_column($hb_image, 'target_id');
        $value = $target_id[0];
        break;
      case 'an_title':
        $value = $value . ' - ' . $this->t('достопримечательности рядом', [], ['langcode' => $langcode]);
        break;
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function multiple() {
    return $this->multiple;
  }

}
