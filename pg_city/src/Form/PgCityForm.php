<?php

namespace Drupal\pg_city\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the city entity edit forms.
 */
class PgCityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New city %label has been created.', $message_arguments));
      $this->logger('pg_city')->notice('Created new city %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The city %label has been updated.', $message_arguments));
      $this->logger('pg_city')->notice('Updated new city %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.pg_city.canonical', ['pg_city' => $entity->id()]);
  }

}
