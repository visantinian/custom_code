<?php

namespace Drupal\pg_city;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a city entity type.
 */
interface PgCityInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the city title.
   *
   * @return string
   *   Title of the city.
   */
  public function getTitle();

  /**
   * Sets the city title.
   *
   * @param string $title
   *   The city title.
   *
   * @return \Drupal\pg_city\PgCityInterface
   *   The called city entity.
   */
  public function setTitle($title);

  /**
   * Gets the city creation timestamp.
   *
   * @return int
   *   Creation timestamp of the city.
   */
  public function getCreatedTime();

  /**
   * Sets the city creation timestamp.
   *
   * @param int $timestamp
   *   The city creation timestamp.
   *
   * @return \Drupal\pg_city\PgCityInterface
   *   The called city entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the city status.
   *
   * @return bool
   *   TRUE if the city is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the city status.
   *
   * @param bool $status
   *   TRUE to enable this city, FALSE to disable.
   *
   * @return \Drupal\pg_city\PgCityInterface
   *   The called city entity.
   */
  public function setStatus($status);

}
