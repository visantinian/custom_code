<?php

namespace Drupal\pg_city\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\pg_city\PgCityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the city entity class.
 *
 * @ContentEntityType(
 *   id = "pg_city",
 *   label = @Translation("City"),
 *   label_collection = @Translation("Cities"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pg_city\PgCityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\pg_city\PgCityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\pg_city\Form\PgCityForm",
 *       "edit" = "Drupal\pg_city\Form\PgCityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "pg_city",
 *   data_table = "pg_city_field_data",
 *   revision_table = "pg_city_revision",
 *   revision_data_table = "pg_city_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer city",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/pg-city/add",
 *     "canonical" = "/pg_city/{pg_city}",
 *     "edit-form" = "/admin/content/pg-city/{pg_city}/edit",
 *     "delete-form" = "/admin/content/pg-city/{pg_city}/delete",
 *     "collection" = "/admin/content/pg-city"
 *   },
 *   field_ui_base_route = "entity.pg_city.settings"
 * )
 */
class PgCity extends RevisionableContentEntityBase implements PgCityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new city entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  private function correctTranslation($entity = NULL, $fields = NULL) {
    if (!entity || !$fields){
      return;
    }
    if ($entity && $entity->hasTranslation('ru') && $entity->hasTranslation('en')){
      $values = $entity->getTranslation('ru')->toArray();
      foreach ($fields as $useful_field){
        $needed_translation = $values[$useful_field];
        $target_translation = $entity->getTranslation('en');
        $target_value = $target_translation->$useful_field->value;
        if ($needed_translation != NULL
          && mb_detect_encoding($target_value) == 'UTF-8'
        ){
          $target_translation->set($useful_field, $needed_translation)->save();
        }
      }
      if ($values['notes_note'] != NULL){
        $needed_translation = [];
        foreach($values['notes_note'] as $note_key => $single_note_needed){
          if (mb_detect_encoding($single_note_needed['value']) == 'UTF-8'){
            $target_translation = $entity->getTranslation('en');
            $target_value = $target_translation->get('notes_note')->getValue()[$note_key]['value'];
            $needed_translation[] = $single_note_needed;
          }
        }
        if ($needed_translation == []){
          return;
        }
        $target_translation->set('notes_note', $needed_translation)->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }

      if (self::get('city_components')->getValue() != []
        && self::hasTranslation('en')){
        $array_of_meaningful_fields = [
          'tg_description',
          'tg_title',
          'gi_description',
          'gi_title',
          'anchor',
          'districts_description',
          'districts_title',
          'attractions_description',
          'attractions_title',
          'text_description',
          'text_title',
          'notes_title',
        ];
        $components = self::get('city_components')->getValue();
        foreach ($components as $component) {
          $paragraph = $component['entity'];
          $this->correctTranslation($paragraph, $array_of_meaningful_fields);
        }
      }
      if (self::get('city_banner')->getValue() != []
        && self::hasTranslation('en')){
        $fields = [
          'lb_title',
          'lb_description',
          'lb_subtitle'
        ];
        $banner = self::get('city_banner')->getValue();
        $paragraph = $banner[0]['entity'];
        $this->correctTranslation($paragraph, $fields);
      }
    }

    // If no revision author has been set explicitly,
    // make the rrrrr owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the city entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the city is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the city author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the city was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the city was last edited.'));

    return $fields;
  }

}
