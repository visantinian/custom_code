<?php

namespace Drupal\pg_update_translations\Form;

use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

class TranslationUpdaterForm extends FormBase {

  /**
   * MessengerInterface definition.
   *
   * @var MessengerInterface
   */
  protected $messenger;

  /**
   * Batch Builder.
   *
   * @var BatchBuilder
   */
  protected $batchBuilder;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new Translation Updater Form object.
   */
  public function __construct(
    MessengerInterface $messenger,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
    $this->batchBuilder = new BatchBuilder();
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pg_update_translations';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['entity_types_to_update'] = [
      '#type' => 'select',
      '#title' => $this->t('Custom Entity Types that needs to be updated'),
      '#header' => [
        'label' => $this->t('Entity Label'),
      ],
      '#options' => $this->gatherEntityTypes(),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run batch'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Helper function to get list of all custom entities on the subsite.
   *
   * @return array
   */
  public function gatherEntityTypes() {
    $entity_types = [];
    $current_entities = array_keys($this->entityTypeManager->getDefinitions());
    foreach($current_entities as $entity_type_id){
      if (
        preg_match('/pg_.*>?/m', $entity_type_id) > 0
      ) {
        $entity_types[] =  $entity_type_id;
      }
    }
    return $entity_types;
  }

  /**
   * Helper function that filters all fields on the entity type, to return
   * only those which have type "entity_revision_reference"
   *
   * @param $entity_types - List of entity_types prepared by $this->gatherEntityTypes()
   *
   * @return array
   */
  public function gatherEntityTypesFields($entity_type)
  {
    $list_of_fields = [];
    $list_of_unprepared_fields = $this->entityFieldManager->getFieldStorageDefinitions($entity_type, NULL);
    foreach ($list_of_unprepared_fields as $prepared_field) {
      $property_definitions = $prepared_field->getPropertyDefinitions();
      foreach ($property_definitions as $property_definition){
        if ($property_definition->getDataType() == "entity_revision_reference") {
          $list_of_fields[] = $prepared_field; // FIELD OBJECT
        }
      }
    }
    return $list_of_fields;
  }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

  /**
   * {@inheritdoc}
   */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $selected_option = $form_state->getValue('entity_types_to_update');
        $entity_type = $form['entity_types_to_update']['#options'][$selected_option];

        $selected_fields = $this->gatherEntityTypesFields($entity_type);
        $additional_langcodes = $this->getAdditionalLangcodes();
        $queue = \Drupal::queue('translation_update_queue');
        $queue->createQueue();
        $counter = 0;
        if ($entity_type !== '') {
          foreach ($selected_fields as $field_key => $selected_field) {
            foreach ($additional_langcodes as $langcode) {
              $entities_to_update = $this->getEids($entity_type, $langcode);
              foreach ($entities_to_update as $eid_to_update) {
                $counter++;
                $item = [
                  'eid' => $eid_to_update,
                  'field' => $selected_field->get('field_name'),
                  'langcode' => $langcode,
                  'entity_type' => $entity_type,
                ];
                $queue->createItem($item);
              }
            }
          }
        }
      $this->messenger()->addStatus($this->t('%count of entities of %et were successfully added to queue',
        [
          '%count' => $counter,
          '%et' => $entity_type,
        ]
      )
      );
    }


  /**
   * Finding additional languages
   */
    public function getAdditionalLangcodes()
    {
        $langcodes = array_keys(\Drupal::languageManager()->getLanguages());
        $default_language = \Drupal::languageManager()->getDefaultLanguage()->getId();
        $default_lc_pos = array_search($default_language, $langcodes);
        unset($langcodes[$default_lc_pos]);
        return $langcodes;
    }


  /**
   * Load all eids for specific type.
   */
    public function getEids($type, $langcode)
    {
        $storage = $this->entityTypeManager->getStorage($type);
        $query = $storage
        ->getQuery()
        ->condition('langcode', $langcode);
        return array_values($query->execute());
    }
}
