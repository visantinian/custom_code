<?php
/**
 * @file
 * Contains \Drupal\pg_update_translations\Plugin\QueueWorker\TranslationUpdateQueue.
 */

namespace Drupal\pg_update_translations\Plugin\QueueWorker;


use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes Tasks for Update Translations on untranslated or unrevisioned entitie of certain type.
 *
 * @QueueWorker(
 *   id = "translation_update_queue",
 *   title = @Translation("Translation update worker: entities queue"),
 *   cron = {"time" = 200}
 * )
 */
class TranslationUpdateQueue extends QueueWorkerBase {

  public function processItem($item) {
    // load entity
    $entity = \Drupal::service('entity_type.manager')->getStorage($item['entity_type'])
      ->load($item['eid']);
    $default_language = 'ru';
    //set default value
    if ($entity) {
      if (!$entity->hasTranslation($default_language)) {
        $definitions = array_keys($entity->getFieldDefinitions());
        $values = [];
        foreach ($definitions as $definition) {
          $values[$definition] = $entity->get($definition)->getValue();
        }
        $entity->addTranslation($default_language, $values);
        $entity->setNewRevision(true);
        $entity->setRevisionLogMessage('
        Entity updated. Created new transaltion. Reason: no default language translation existed');
      }
      $default_value = $entity->getTranslation($default_language)->get($item['field'])->getValue();
      if ($default_value == []) {
        $translation_source = $entity->getTranslation('en')->get($item['field'])->getValue();
        $translation = $entity->getTranslation($default_language);
        if ($translation->get($item['field'])->getValue == []) {
          $translation->set($item['field'], $translation_source);
          $entity->setRevisionLogMessage('Entity updated. Reason: no "ru" translation existed');
        }
      } else {
        $translation = $entity->getTranslation($item['langcode']);
        if ($translation->get($item['field'])->getValue() == []) {
          $translation->set($item['field'], $default_value);
          $entity->setRevisionLogMessage(
            '
          Entity updated. Reason: "en" value exists, but @value is empty',
            [
              '@value' => $item['field'],
            ]
          );
        }
      }
      if ($default_value == null) {
        $translation_source = $entity->getTranslation('en')->get($item['field'])->getValue();
        $translation = $entity->getTranslation($default_language);
        if ($translation->get($item['field']) == []) {
          $translation->set($item['field'], $translation_source);
          $entity->setRevisionLogMessage(
            '
          Entity updated. Reason: "ru" translation exists, but @value is empty',
            [
              '@value' => $item['field'],
            ]
          );
        }
      }
      $entity->setNewRevision(true);
      $entity->setRevisionUserId(1);
      $entity->save();
    }
  }
}
