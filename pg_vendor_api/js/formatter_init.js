(function ($, Drupal, drupalSettings) {

	$(document).ready(function() {
    $('html').on('pg_json_results_ready', function(ev, previewEnabled = false) {
      $('div.json_formatter').each(function(){
        $this = jQuery(this);
        if (!$this.has('div.json-formatter-row')) {

        } else {
          var formatter = new JSONFormatter(JSON.parse($this.text()), 1, {hoverPreviewEnabled: previewEnabled});
          $this.html(formatter.render());
        }

      });
    });

    $('html').trigger('pg_json_results_ready');

	});

})(jQuery, Drupal, drupalSettings);
