<?php

/**
 * @file
 * Contains Drupal\pg_api_request_entity\PGApiRequestEntityInterface.
 */

namespace Drupal\pg_api_request_entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a PGApiRequestEntity entity.
 *
 * @ingroup account
 */
interface PGApiRequestEntityInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.
}
