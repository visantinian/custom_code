<?php

namespace Drupal\pg_api_request_entity\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGApiRequestImportForm extends FormBase implements ContainerInjectionInterface {

  protected $vendorManager;

  protected $storageManager;

  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(PGVendorApiManager $pg_vendor_api_manager, PGVendorApiStorageManager $pg_vendor_api_storage_manager, $requestID) {
    $this->vendorManager = $pg_vendor_api_manager;
    $this->storageManager = $pg_vendor_api_storage_manager;
    $this->request = $this->vendorManager->getRequest($requestID);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pg_vendor_api.manager'),
      $container->get('pg_vendor_api.storage'),
      $container->get('request_stack')
        ->getCurrentRequest()
        ->get('pg_api_request_entity')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pg_request_import_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {

    // \Drupal::entityDefinitionUpdateManager()->applyUpdates();

    // $form['pg_request_data_table'][$dataId]['import'] = [
    //   '#type' => 'checkbox',
    //   '#title' => t('Force import'),
    //   '#default_value' => 0,
    // ];

    $form['pg_request_data_table'] = [
      '#type' => 'table',
      '#title' => t('Data in this request'),
      '#empty' => $this->t('There is no data.'),
      '#header' => [
        'import' => t('Import'),
        'title' => t('Vendor'),
        'operations' => t('Operations'),
      ],
    ];

    // $request = $this->vendorManager->getRequest($requestID);
    $relatedData = $this->request->relatedVendorData();

    if ($relatedData) {
      foreach ($relatedData as $dataId => $data) {
        $form['pg_request_data_table'][$dataId]['import'][$data->vendor_id->value] = [
          '#type' => 'checkbox',
          '#title' => t('Import'),
          '#default_value' => !empty($form_state->getValue('pg_request_data_table')) ? $form_state->getValue('pg_request_data_table')[$dataId]['import'] : 1,
          '#title_display' => 'invisible',
        ];

        $form['pg_request_data_table'][$dataId]['title']['#plain_text'] = $data->vendor_name->value . ' (' . $data->vendor_id->value . ')';

        // $form['pg_request_data_table'][$dataId]['operations']['#plain_text'] = 'test';
        $form['pg_request_data_table'][$dataId]['operations'] = [
          '#type' => 'operations',
          '#links' => [
            [
              'type' => 'link',
              'title' => $this->t('View Data'),
              'url' => Url::fromRoute('pg_vendor_api.ajax_data')
                ->setRouteParameters(['dataID' => $dataId]),
              'ajax' => [
                'event' => 'click',
              ],
            ],
          ],
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import selected data'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'pg_vendor_api/jsonFormatter';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('pg_request_data_table');

    $error = TRUE;
    foreach ($values as $key => $value) {
      if (end($value['import']) == 1) {
        $error = FALSE;
      }
    }

    if ($error) {
      $form_state->setErrorByName('', t('Please select at least 1 data'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];
    foreach ($form_state->getValue('pg_request_data_table') as $dataId => $value) {

      $vendorID = key($value['import']);
      $importFunctions = $this->storageManager->storageForVendorID($vendorID)
        ->importBatchFunctions();

      if (is_array($importFunctions)) {
        foreach ($importFunctions as $function) {
          $operations[] = [$function, [$dataId, $vendorID]];
        }
      }
      elseif (is_string($importFunctions)) {
        $function = $importFunctions;
        $operations[] = [$function, [$dataId, $vendorID]];
      }
    }

    $batch = [
      'operations' => $operations,
      'finished' => 'pg_request_entity_import_finished',
      'title' => $this->t('Importing data...'),
      'init_message' => $this->t('Import is starting'),
      'progress_message' => 'Processed @current out of @total reste @percentage%',
      'error_message' => $this->t('It has an error.'),
    ];


    batch_set($batch);
  }

}
