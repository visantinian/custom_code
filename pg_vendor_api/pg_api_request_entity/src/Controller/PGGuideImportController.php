<?php

/**
 * @file
 * Contains
 *   \Drupal\pg_api_request_entity\Controller\PGTechnicalSessionImportController.
 */

namespace Drupal\pg_api_request_entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PGGuideImportController extends ControllerBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(PGVendorApiManager $pg_vendor_api_manager) {
    $this->vendorManager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pg_vendor_api.manager')
    );
  }


  public function importBatch($requestID) {
    $request = $this->vendorManager->getRequest($requestID);
    $relatedData = $request->relatedVendorData();

    $operations = [];
    foreach ($relatedData as $dataId => $data) {
      $importFunction = $this->vendorManager->vendor($data->vendor_id->value)
        ->importBatchFunction();
      if (is_array($importFunction)) {
        foreach ($importFunction as $function) {
          $operations[] = [
            $function,
            [$data->get('data')->getValue()[0], $data->vendor_id->value],
          ];
        }
      }
      else {
        $operations[] = [
          $importFunction,
          [$data->get('data')->getValue()[0], $data->vendor_id->value],
        ];
      }

    }

    $batch = [
      'operations' => $operations,
      'finished' => 'pg_request_entity_import_finished',
      'title' => $this->t('Importing data from request #' . $requestID),
      'init_message' => $this->t('Import is starting'),
      'progress_message' => 'Processed @current out of @total reste @percentage%',
      'error_message' => $this->t('It has an error.'),
    ];

    batch_set($batch);
    return batch_process('/admin/pg/api_request/list');
  }

}
