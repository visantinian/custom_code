<?php

/**
 * @file
 * Contains Drupal\pg_api_request_entity\PGApiRequestEntityAccessControlHandler.
 */

namespace Drupal\pg_api_request_entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class PGApiRequestEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view PGApiRequestEntity entity');
        break;

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit PGApiRequestEntity entity');
        break;

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete PGApiRequestEntity entity');
        break;

    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add PGApiRequestEntity entity');
  }

}
