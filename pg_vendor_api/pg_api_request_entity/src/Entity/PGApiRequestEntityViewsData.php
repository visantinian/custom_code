<?php

/**
 * @file
 * Contains Drupal\pg_api_request_entity\Entity\PGApiRequestEntityViewsData.
 */

namespace Drupal\pg_api_request_entity\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data for the PGApiRequestEntity entity type.
 */
class PGApiRequestEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pg_api_request_entity']['table']['base'] = [
      'field' => 'id',
      'title' => t('PGApiRequestEntity'),
      'help' => t('The pg_api_request_entity entity ID.'),
    ];

    return $data;
  }

}
