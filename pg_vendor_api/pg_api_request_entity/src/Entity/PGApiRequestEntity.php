<?php

/**
 * @file
 * Contains Drupal\pg_api_request_entity\Entity\PGApiRequestEntity.
 */

namespace Drupal\pg_api_request_entity\Entity;

use Drupal;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\pg_api_request_entity\PGApiRequestEntityInterface;
use Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntity;

/**
 * Defines the PGApiRequestEntity entity.
 *
 * @ingroup pg_api_request_entity
 *
 * @ContentEntityType(
 *   id = "pg_api_request_entity",
 *   label = @Translation("PG Api Request"),
 *   handlers = {
 *     "views_data" = "Drupal\pg_api_request_entity\Entity\PGApiRequestEntityViewsData",
 *     "list_builder" = "Drupal\pg_api_request_entity\Entity\Controller\PGApiRequestEntityListController",
 *     "form" = {
 *       "delete" = "Drupal\pg_api_request_entity\Entity\Form\PGApiRequestEntityDeleteForm",
 *     },
 *     "access" = "Drupal\pg_api_request_entity\PGApiRequestEntityAccessControlHandler",
 *   },
 *   base_table = "pg_api_request_entity",
 *   admin_permission = "administer PGApiRequestEntity entity",
 *   translatable = FALSE,
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "delete-form" = "/admin/configs/pg/request/{pg_api_request_entity}/delete",
 *     "collection" = "/admin/configs/pg/request"
 *   }
 * )
 */
class PGApiRequestEntity extends ContentEntityBase implements PGApiRequestEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    foreach ($entities as $entity) {
      $relatadData = $entity->relatedVendorData();
      foreach ($relatadData as $vendorData) {
        $vendorData->delete();
      }
    }
  }

  public function relatedVendorData() {
    $query = Drupal::entityQuery('pg_vendor_data_entity')
      ->condition('pg_request_id', $this->id());
    $ids = $query->execute();

    if (!empty($ids)) {
      return PGVendorDataEntity::loadMultiple($ids);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the PGApiRequestEntity entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the PGApiRequestEntity entity.'))
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

}
