<?php

/**
 * @file
 * Contains
 *   Drupal\pg_api_request_entity\Entity\Controller\PGApiRequestEntityListController.
 */

namespace Drupal\pg_api_request_entity\Entity\Controller;

use Drupal;
use Drupal\Core\Ajax\ISettingsCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;


/**
 * Provides a list controller for PGApiRequestEntity entity.
 *
 * @ingroup pg_api_request_entity
 */
class PGApiRequestEntityListController extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $form = Drupal::formBuilder()
      ->getForm('Drupal\pg_vendor_api\Form\PGVendorApiRequestBatchForm');
    $build['request_form'] = [
      '#type' => 'markup',
      '#markup' => drupal_render($form),
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#empty' => $this->t('There is no data yet.'),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    $build['#attached']['library'][] = 'pg_vendor_api/jsonFormatter';
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('ID');
    $header['date'] = t('Date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id']['#plain_text'] = $entity->id();
    $row['date']['#plain_text'] = format_date($entity->getCreatedTime(), 'short');
    $operations['operations']['data']['#type'] = 'operations';
    $operations['operations']['data']['#links']['import_data'] = [
      'title' => $this->t('Import data'),
      'url' => Url::fromRoute('entity.pg_api_request_entity.canonical')
        ->setRouteParameters(['pg_api_request_entity' => $entity->id()]),
      'weight' => 0,
    ];

    $operations['operations']['data']['#links'] += parent::buildRow($entity)['operations']['data']['#links'];

    return $row + $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this
      ->getStorage()
      ->getQuery()
      ->sort($this->entityType->getKey('id'), 'DESC');

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

}
