<?php

/**
 * @file
 * Contains
 *   Drupal\pg_api_request_entity\Entity\Form\PGApiRequestEntityDeleteForm.
 */

namespace Drupal\pg_api_request_entity\Entity\Form;

use Drupal;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a form for deleting a PGApiRequestEntity entity.
 *
 * @ingroup pg_api_request_entity
 */
class PGApiRequestEntityDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->getCreatedTime()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pg_api_request_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    Drupal::logger('pg_api_request_entity')->notice('@type: deleted %title.',
      [
        '@type' => $this->entity->bundle(),
        '%title' => $this->entity->getCreatedTime(),
      ]);
    $form_state->setRedirect('entity.pg_api_request_entity.collection');
  }

}
