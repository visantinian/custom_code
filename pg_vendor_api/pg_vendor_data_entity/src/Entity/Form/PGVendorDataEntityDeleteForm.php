<?php

/**
 * @file
 * Contains
 *   Drupal\pg_vendor_data_entity\Entity\Form\PGVendorDataEntityDeleteForm.
 */

namespace Drupal\pg_vendor_data_entity\Entity\Form;

use Drupal;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a form for deleting a PGVendorDataEntity entity.
 *
 * @ingroup pg_vendor_data_entity
 */
class PGVendorDataEntityDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete data from %name?', ['%name' => $this->entity->vendor_name->value]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pg_vendor_data_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    Drupal::logger('pg_vendor_data_entity')->notice('@type: deleted %title.',
      [
        '@type' => $this->entity->bundle(),
        '%title' => $this->entity->vendor_name->value,
      ]);
    $form_state->setRedirect('entity.pg_vendor_data_entity.collection');
  }

}
