<?php

/**
 * @file
 * Contains
 *   Drupal\pg_vendor_data_entity\Entity\Controller\PGVendorDataEntityListController.
 */

namespace Drupal\pg_vendor_data_entity\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;


/**
 * Provides a list controller for PGVendorDataEntity entity.
 *
 * @ingroup pg_vendor_data_entity
 */
class PGVendorDataEntityListController extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    // $build['description'] = array(
    //   '#markup' => $this->t('Content Entity implements a PGVendorDataEntity model.'),
    // );
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('PGVendorDataEntityID');
    $header['vendor_name'] = t('Vendor name');
    $header['vendor_id'] = t('Vendor ID');
    $header['pg_request_id'] = t('Request ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['vendor_name'] = $entity->vendor_name->value;
    $row['vendor_id'] = $entity->vendor_id->value;
    $row['pg_request_id'] = $entity->pg_request_id->value;
    return $row + parent::buildRow($entity);
  }

}
