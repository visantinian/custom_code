<?php

/**
 * @file
 * Contains Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntityViewsData.
 */

namespace Drupal\pg_vendor_data_entity\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data for the PGVendorDataEntity entity type.
 */
class PGVendorDataEntityViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pg_vendor_data_entity']['table']['base'] = [
      'field' => 'id',
      'title' => t('PGVendorDataEntity'),
      'help' => t('The pg_vendor_data_entity entity ID.'),
    ];

    return $data;
  }

}
