<?php

/**
 * @file
 * Contains Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntity.
 */

namespace Drupal\pg_vendor_data_entity\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\pg_vendor_data_entity\PGVendorDataEntityInterface;

/**
 * Defines the PGVendorDataEntity entity.
 *
 * @ingroup pg_vendor_data_entity
 *
 * @ContentEntityType(
 *   id = "pg_vendor_data_entity",
 *   label = @Translation("PGVendorDataEntity entity"),
 *   handlers = {
 *     "views_data" =
 *   "Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntityViewsData",
 *     "list_builder" =
 *   "Drupal\pg_vendor_data_entity\Entity\Controller\PGVendorDataEntityListController",
 *
 *     "form" = {
 *       "delete" =
 *   "Drupal\pg_vendor_data_entity\Entity\Form\PGVendorDataEntityDeleteForm",
 *     },
 *     "access" =
 *   "Drupal\pg_vendor_data_entity\PGVendorDataEntityAccessControlHandler",
 *   },
 *   base_table = "pg_vendor_data_entity",
 *   admin_permission = "administer PGVendorDataEntity entity",
 *   fieldable = FALSE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "delete-form" = "/pg_vendor_data_entity/{pg_vendor_data_entity}/delete",
 *     "collection" = "/admin/pg/vendor_data/list"
 *   }
 * )
 */
class PGVendorDataEntity extends ContentEntityBase implements PGVendorDataEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'checksum' => md5($values['data']),
      // 'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the PGVendorDataEntity entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the PGVendorDataEntity entity.'))
      ->setReadOnly(TRUE);

    $fields['vendor_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Vendor ID'))
      ->setDescription(t('Vendor ID'));

    $fields['vendor_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Vendor name'))
      ->setDescription(t('Vendor name'));

    $fields['pg_request_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Requested by'))
      ->setDescription(t('Request ID'))
      ->setSetting('target_type', 'pg_api_request_entity')
      ->setSetting('handler', 'default');

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setDescription(t('Data from vendor'));

    $fields['checksum'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Data checksum'))
      ->setDescription(t('Data checksum'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**reeeee
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }


}
