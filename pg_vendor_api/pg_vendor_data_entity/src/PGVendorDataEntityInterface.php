<?php

/**
 * @file
 * Contains Drupal\pg_vendor_data_entity\PGVendorDataEntityInterface.
 */

namespace Drupal\pg_vendor_data_entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a PGVendorDataEntity entity.
 *
 * @ingroup account
 */
interface PGVendorDataEntityInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.
}
