<?php

/**
 * @file
 * Contains Drupal\pg_vendor_data_entity\PGVendorDataEntityAccessControlHandler.
 */

namespace Drupal\pg_vendor_data_entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class PGVendorDataEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view PGVendorDataEntity entity');
        break;

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit PGVendorDataEntity entity');
        break;

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete PGVendorDataEntity entity');
        break;

    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add PGVendorDataEntity entity');
  }

}
