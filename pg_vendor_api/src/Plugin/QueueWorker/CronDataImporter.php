<?php

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

/**
 * A Data Importer that imports data on CRON run.
 *
 * @QueueWorker(
 *   id = "pg_vendor_api_import_queue",
 *   title = @Translation("Cron Data Importer"),
 *   cron = {"time" = 10}
 * )
 */
class CronDataImporter extends DataImporterBase {

}
