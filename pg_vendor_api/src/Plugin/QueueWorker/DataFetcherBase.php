<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Plugin\QueueWorker\DataFetcherBase.php
 */

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

use Drupal;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides base functionality for the DataFetcher Queue Workers.
 */
abstract class DataFetcherBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   *
   * @var Drupal\pg_vendor_api\Classes\PGVendorApiManager
   */
  protected $vendorManager;

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(PGVendorApiManager $pg_vendor_api_manager, QueueFactory $queue) {
    $this->vendorManager = $pg_vendor_api_manager;
    $this->queueFactory = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('pg_vendor_api.manager'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if ($dataId = $this->fetchData($data)) {
      $queue = $this->queueFactory->get('pg_vendor_api_import_queue');

      $vendorId = $data['vendor'];
      $item = [
        'dataId' => $dataId,
        'vendorId' => $vendorId,
      ];
      if ($queueItemId = $queue->createItem($item)) {
        Drupal::logger('pg_vendor_api')
          ->notice('Added %queue queue item @id for import of %vendor vendor data saved as Data entity @dataId.',
            [
              '%queue' => 'pg_vendor_api_import_queue',
              '@id' => $queueItemId,
              '%vendor' => $vendorId,
              '@dataId' => $dataId,
            ]
          );
      }
      else {
        Drupal::logger('pg_vendor_api')
          ->warning('Unable to add an %queue queue item for import of %vendor vendor data saved as Data entity @dataId.',
            [
              '%queue' => 'pg_vendor_api_import_queue',
              '%vendor' => $vendorId,
              '@dataId' => $dataId,
            ]
          );
      }
    }
    else {
      $vendorId = empty($data['vendor']) ? t('Undefined') : $data['vendor'];
      Drupal::logger('pg_vendor_api')
        ->warning('QueueWorker is unable to fetch %vendor vendor data.',
          [
            '%vendor' => $vendorId,
          ]
        );
    }
  }

  /**
   * Fetch data from vendor API.
   *
   * @param array $data
   *
   * @return boolean
   */
  protected function fetchData($data) {
    if (empty($data['vendor']) || empty($data['requestId'])) {
      return 0;
    }

    $vendorId = $data['vendor'];
    $vendor = $this->vendorManager->vendor($vendorId);
    if (!$vendor) {
      return 0;
    }

    $requestId = $data['requestId'];
    $request = $this->vendorManager->getRequest($requestId);
    if (!$request) {
      return 0;
    }

    return $vendor->fetchData($requestId);
  }

}
