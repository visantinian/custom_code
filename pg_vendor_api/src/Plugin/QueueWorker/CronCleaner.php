<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Plugin\QueueWorker\CronCleaner.php
 */

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

use Drupal;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Cleaner that cleans up database on CRON run.
 *
 * @QueueWorker(
 *   id = "pg_vendor_api_cleanup_queue",
 *   title = @Translation("Cron Cleaner"),
 *   cron = {"time" = 10}
 * )
 */
class CronCleaner extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var Drupal\pg_vendor_api\Classes\PGVendorApiManager
   */
  protected $vendorManager;

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(PGVendorApiManager $pg_vendor_api_manager, QueueFactory $queue) {
    $this->vendorManager = $pg_vendor_api_manager;
    $this->queueFactory = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('pg_vendor_api.manager'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data['requestId'])) {
      return;
    }
    $requestId = $data['requestId'];
    $request = $this->vendorManager->getRequest($requestId);
    if ($request) {
      $request->delete();
      Drupal::logger('pg_vendor_api')
        ->notice('API request @id deleted by %name Cron Queue Worker.', [
          '@id' => $requestId,
          '%name' => __CLASS__,
        ]);
    }
    else {
      Drupal::logger('pg_vendor_api')
        ->notice('Unable to delete API request @id by %name Cron Queue Worker: item does not exist.', [
          '@id' => $requestId,
          '%name' => __CLASS__,
        ]);
    }
  }

}
