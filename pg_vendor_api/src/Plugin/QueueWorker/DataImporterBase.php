<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Plugin\QueueWorker\DataImporterBase.php
 */

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

use Drupal;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides base functionality for the DataFetcher Queue Workers.
 */
abstract class DataImporterBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var Drupal\pg_vendor_api\Classes\PGVendorApiManager
   */
  protected $vendorManager;

  /**
   * @var Drupal\pg_vendor_api\Classes\PGVendorApiStorageManager
   */
  protected $storageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(PGVendorApiManager $pg_vendor_api_manager, PGVendorApiStorageManager $pg_vendor_api_storage_manager) {
    $this->vendorManager = $pg_vendor_api_manager;
    $this->storageManager = $pg_vendor_api_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('pg_vendor_api.manager'),
      $container->get('pg_vendor_api.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if ($number = $this->importData($data)) {
      $dataId = $data['dataId'];
      $vendorId = $data['vendorId'];
      Drupal::logger('pg_vendor_api')
        ->notice('Processed import of Data entity @dataId for %vendor vendor. Number of processed items: @number',
          [
            '@dataId' => $dataId,
            '%vendor' => $vendorId,
            '@number' => $number,
          ]
        );
    }
    else {
      $vendorId = empty($data['vendorId']) ? t('Undefined') : $data['vendorId'];
      Drupal::logger('pg_vendor_api')
        ->warning('QueueWorker is unable to import %vendor vendor data.',
          [
            '%vendor' => $vendorId,
          ]
        );
    }
  }

  /**
   * Fetch data from vendor API.
   *
   * @param array $data
   *
   * @return boolean
   */
  protected function importData($data) {
    if (empty($data['dataId']) || empty($data['vendorId'])) {
      return 0;
    }

    $vendorId = $data['vendorId'];
    $vendor = $this->vendorManager->vendor($vendorId);
    if (!$vendor) {
      return 0;
    }

    $dataId = $data['dataId'];
    $dataEntity = $this->vendorManager->getData($dataId);
    if (!$dataEntity) {
      return 0;
    }

    $dataItems = $dataEntity->get('data')->getValue()[0];
    if (!$dataItems) {
      return 0;
    }

    $storage = $this->storageManager->storageForVendorID($vendorId);
    if (!$storage) {
      return 0;
    }

    $storage->check($dataItems);

    foreach ($dataItems as $dataItem) {
      $storage->saveFromData($dataItem);
    }

    return count($dataItems);
  }

}
