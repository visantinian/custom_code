<?php

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

/**
 * A Data Fetcher that fetches a data from a vendor API via a manual action
 * triggered by an admin.
 *
 * @QueueWorker(
 *   id = "manual_data_fetcher",
 *   title = @Translation("Manual Data Fetcher"),
 * )
 */
class ManualDataFetcher extends DataFetcherBase {

}
