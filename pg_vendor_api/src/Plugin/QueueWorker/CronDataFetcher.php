<?php

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

/**
 * A Data Fetcher that fetches data on CRON run.
 *
 * @QueueWorker(
 *   id = "pg_vendor_api_fetch_queue",
 *   title = @Translation("Cron Node Publisher"),
 *   cron = {"time" = 10}
 * )
 */
class CronDataFetcher extends DataFetcherBase {

}
