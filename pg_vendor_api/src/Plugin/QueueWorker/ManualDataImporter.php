<?php

namespace Drupal\pg_vendor_api\Plugin\QueueWorker;

/**
 * A Data Importer that imports data via a manual action triggered by an admin.
 *
 * @QueueWorker(
 *   id = "manual_data_importer",
 *   title = @Translation("Manual Data Importer"),
 * )
 */
class ManualDataImporter extends DataImporterBase {

}
