<?php

namespace Drupal\pg_vendor_api\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorAPIQueueController extends ControllerBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiManager $pg_vendor_api_manager) {
    $this->vendorManager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager')
    );
  }

  public function createQueueItems($vendor) {
    $vendor = $this->vendorManager->vendor($vendor);
    $data = $vendor->prepareQueue();
  }

}
