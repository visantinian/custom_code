<?php

namespace Drupal\pg_vendor_api\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PGVendorApiRequestInfoController extends ControllerBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiManager $pg_vendor_api_manager) {
    $this->vendorManager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager')
    );
  }

  function viewRequest($requestID) {
    $request = $this->vendorManager->getRequest($requestID);
    $relatedData = $request->relatedVendorData();
    $page = [];
    if ($relatedData) {
      foreach ($relatedData as $dataId => $data) {
        $page['data_' . $dataId] = [
          '#type' => 'details',
          '#title' => $data->vendor_name->value . ' | ' . $data->checksum->value,
        ];

        $page['data_' . $dataId]['json'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => htmlspecialchars(json_encode($data->get('data')
            ->getValue()[0])),
          '#attributes' => ['class' => 'json_formatter'],
        ];
      }
    }

    $content = [
      '#type' => 'markup',
      '#cache' => ['max-age' => 0],
      '#markup' => drupal_render($page),
      '#attached' => [
        'library' => ['pg_vendor_api/jsonFormatter'],
      ],
    ];

    $response = new AjaxResponse();
    $title = t('Requested data');
    $response->addCommand(new OpenModalDialogCommand($title, $content, [
      'width' => '1100',
      'height' => '700',
    ]));
    $response->addCommand(new InvokeCommand('html', 'trigger', [
      'pg_json_results_ready',
      [0],
    ]));
    return $response;
  }

}
