<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Controller\PGVendorApiConfiguration.
 */

namespace Drupal\pg_vendor_api\Controller;

use Drupal\Core\Controller\ControllerBase;


class PGVendorApiConfiguration extends ControllerBase {


  public function build() {
    $output = [
      '#markup' => t('Hello World !! '),
    ];
    return $output;
  }


}
