<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Controller\PGVendorAPIBatchController.
 */

namespace Drupal\pg_vendor_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

class PGVendorAPIBatchController extends ControllerBase {

  /**
   * Publish unpublished nodes and unpublish published nodes using Batch.
   *
   * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function publishAndUnpublishNodeBatch() {
    $batch = [
      'operations' => [],
      'finished' => 'batch_example_finished',
      'title' => $this->t('Example Batch'),
      'init_message' => $this->t('Batch is starting'),
      'progress_message' => 'Processed @current out of @total reste @percentage',
      'error_message' => $this->t('It has an error.'),
    ];
    $nodes = Node::loadMultiple();
    foreach ($nodes as $nid => $node) {
      $batch['operations'][] = ['batch_example_progress', [$nid]];
    }
    batch_set($batch);
    return batch_process('/admin/content');
  }

}
