<?php

namespace Drupal\pg_vendor_api\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorApiTestResultsController extends ControllerBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiManager $pg_vendor_api_manager) {
    $this->vendorManager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager')
    );
  }

  public function testResults($vendor) {
    $vendor = $this->vendorManager->vendor($vendor);
    $data = $vendor->getData()->prepareTestData();

    $endpointInfo = $vendor->endpointInfo();

    $page = [];

    $page['info_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'Request info',
    ];

    $page['info_wrapper']['api_url'] = [
      '#type' => 'item',
      '#title' => t('API URL:'),
      '#markup' => $endpointInfo['url'],
    ];

    $params = [];
    foreach ($endpointInfo['params'] as $key => $value) {
      $params[] = '<strong>' . $key . ': </strong> ' . $value;
    }
    $page['info_wrapper']['api_params'] = [
      '#type' => 'item',
      '#title' => t('Params:'),
      '#markup' => join('<br>', $params),
    ];


    $page['json_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'API response',
    ];

    $page['json_wrapper']['json'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => 'json_formatter'],
      '#value' => $data,
    ];

    return [
      '#type' => 'markup',
      '#markup' => drupal_render($page),
      '#cache' => ['max-age' => 0,],
      '#attached' => [
        'library' => ['pg_vendor_api/jsonFormatter'],
      ],
    ];
  }

  public function testResultsAjax($vendor) {
    $vendor = $this->vendorManager->vendor($vendor);
    $data = $vendor->getData()->prepareTestData();

    $endpointInfo = $vendor->endpointInfo();

    $page = [];

    $page['info_wrapper'] = [
      '#type' => 'details',
      '#title' => 'Request info',
    ];

    $page['info_wrapper']['api_url'] = [
      '#type' => 'item',
      '#title' => t('API URL:'),
      '#markup' => $endpointInfo['url'],
    ];

    $params = [];
    foreach ($endpointInfo['params'] as $key => $value) {
      $params[] = '<strong>' . $key . ': </strong> ' . $value;
    }
    $page['info_wrapper']['api_params'] = [
      '#type' => 'item',
      '#title' => t('Params:'),
      '#markup' => join('<br>', $params),
    ];


    $page['json_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'API response',
    ];

    $page['json_wrapper']['json'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => 'json_formatter'],
      '#value' => $data[0],
    ];

    $content = [
      '#type' => 'markup',
      '#markup' => drupal_render($page),
      '#cache' => ['max-age' => 0,],
      '#attached' => [
        'library' => ['pg_vendor_api/jsonFormatter'],
      ],
    ];

    $response = new AjaxResponse();
    $title = t('Response data');
    $response->addCommand(new OpenModalDialogCommand($title, $content, [
      'width' => '1100',
      'height' => '700',
    ]));
    $response->addCommand(new InvokeCommand('html', 'trigger', [
      'pg_json_results_ready',
      [0],
    ]));
    return $response;
  }

  public function ajaxViewData($dataID) {
    $data = $this->vendorManager->getData($dataID);

    $page['json_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'API response',
    ];

    $page['json_wrapper']['json'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => 'json_formatter'],
      '#value' => htmlspecialchars(json_encode($data->get('data')
        ->getValue()[0])),
    ];

    $content = [
      '#type' => 'markup',
      '#markup' => drupal_render($page),
      '#cache' => ['max-age' => 0,],
      '#attached' => [
        'library' => ['pg_vendor_api/jsonFormatter'],
      ],
    ];

    $response = new AjaxResponse();
    $title = t('Response data');
    $response->addCommand(new OpenModalDialogCommand($title, $content, [
      'width' => '1100',
      'height' => '700',
    ]));
    $response->addCommand(new InvokeCommand('html', 'trigger', [
      'pg_json_results_ready',
      [0],
    ]));
    return $response;
  }

}
