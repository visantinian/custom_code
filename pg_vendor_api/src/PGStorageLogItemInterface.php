<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\PGStorageLogItemInterface.
 */

namespace Drupal\pg_vendor_api;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a PGStorageLogItem entity.
 *
 * @ingroup account
 */
interface PGStorageLogItemInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.
}
