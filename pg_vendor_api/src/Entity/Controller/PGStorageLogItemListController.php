<?php

/**
 * @file
 * Contains
 *   Drupal\pg_vendor_api\Entity\Controller\PGStorageLogItemListController.
 */

namespace Drupal\pg_vendor_api\Entity\Controller;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;


/**
 * Provides a list controller for PGStorageLogItem entity.
 *
 * @ingroup pg_vendor_api
 */
class PGStorageLogItemListController extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Content Entity Example implements a PGStorageLogItem model. These contacts are fieldable entities. You can manage the fields on the <a href="@adminlink">PGStorageLogItem admin page</a>.', [
        '@adminlink' => Drupal::urlGenerator()
          ->generateFromRoute('PGStorageLogItem.settings'),
      ]),
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('PGStorageLogItemID');
    $header['name'] = t('Name');
    $header['first_name'] = $this->t('First Name');
    $header['gender'] = $this->t('Gender');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->id();
    $row['name'] = $entity->link();
    $row['first_name'] = $entity->first_name->value;
    $row['gender'] = $entity->gender->value;
    return $row + parent::buildRow($entity);
  }

}
