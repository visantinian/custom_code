<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Entity\Form\PGStorageLogItemDeleteForm.
 */

namespace Drupal\pg_vendor_api\Entity\Form;

use Drupal;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a form for deleting a PGStorageLogItem entity.
 *
 * @ingroup pg_vendor_api
 */
class PGStorageLogItemDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete log %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pg_storage_log.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    Drupal::logger('pg_vendor_api')->notice('@type: deleted %title.',
      [
        '@type' => $this->entity->bundle(),
        '%title' => $this->entity->label(),
      ]);
    $form_state->setRedirect('entity.pg_storage_log.collection');
  }

}
