<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Entity\Form\PGStorageLogItemSettingsForm.
 */

namespace Drupal\pg_vendor_api\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PGStorageLogItemSettingsForm.
 *
 * @package Drupal\pg_vendor_api\Form
 *
 * @ingroup pg_vendor_api
 */
class PGStorageLogItemSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'PGStorageLogItem_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Define the form used for PGStorageLogItem  settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['PGStorageLogItem_settings']['#markup'] = 'Settings form for PGStorageLogItem. Manage field settings here.';
    return $form;
  }

}
