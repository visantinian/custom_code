<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Entity\PGStorageLogItemViewsData.
 */

namespace Drupal\pg_vendor_api\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data for the PGStorageLogItem entity type.
 */
class PGStorageLogItemViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pg_storage_log']['table']['base'] = [
      'field' => 'id',
      'title' => t('PGStorageLogItem'),
      'help' => t('The PGStorageLogItem entity ID.'),
    ];

    return $data;
  }

}
