<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\Entity\PGStorageLogItem.
 */

namespace Drupal\pg_vendor_api\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\pg_vendor_api\PGStorageLogItemInterface;

/**
 * Defines the PGStorageLogItem entity.
 *
 * @ingroup pg_vendor_api
 *
 * @ContentEntityType(
 *   id = "pg_storage_log",
 *   label = @Translation("PG Storage Log Item"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pg_vendor_api\Entity\Controller\PGStorageLogItemListController",
 *     "views_data" = "Drupal\pg_vendor_api\Entity\PGStorageLogItemViewsData",
 *
 *     "form" = {
 *       "delete" = "Drupal\pg_vendor_api\Entity\Form\PGStorageLogItemDeleteForm",
 *     },
 *     "access" = "Drupal\pg_vendor_api\PGStorageLogItemAccessControlHandler",
 *   },
 *   base_table = "pg_storage_log",
 *   admin_permission = "administer pg_storage_log entity",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "entity_type",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/configs/pg/import-log/{pg_storage_log}",
 *     "delete-form" = "/admin/configs/pg/import-log/{pg_storage_log}/delete",
 *     "collection" = "/admin/configs/pg/import-logs"
 *   },
 *   field_ui_base_route = "entity.pg_storage_log.settings"
 * )
 */
class PGStorageLogItem extends ContentEntityBase implements PGStorageLogItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // $values += array(
    //   'user_id' => \Drupal::currentUser()->id(),
    // );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the PGStorageLogItem entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the PGStorageLogItem entity.'))
      ->setReadOnly(TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ]);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entity ID'))
      ->setSettings([
        'default_value' => 0,
      ]);

    $fields['action'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Action'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

}
