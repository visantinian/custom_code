<?php

namespace Drupal\pg_vendor_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PGVendorAPIStorageConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $vendorManager;

  protected $storageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiManager $pg_vendor_api_manager, PGVendorApiStorageManager $pg_vendor_api_storage) {
    parent::__construct($config_factory);

    $this->vendorManager = $pg_vendor_api_manager;
    $this->storageManager = $pg_vendor_api_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager'),
      $container->get('pg_vendor_api.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pg_vendor_api.storages'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pg_vendor_api_storage_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['pg_vendor_storage_table'] = [
      '#type' => 'table',
      '#empty' => $this->t('There is no vendors yet.'),
      '#header' => ['vendor' => t('Vendor'), 'storage' => t('Storage')],
    ];

    $storageOptions = [];
    foreach ($this->storageManager->storages() as $id => $storage) {
      $storageOptions[$id] = $storage->storageName();
    }

    foreach ($this->vendorManager->vendors() as $id => $vendor) {
      $form['pg_vendor_storage_table'][$id]['vendor']['#plain_text'] = $vendor->vendorTitle();
      $form['pg_vendor_storage_table'][$id]['storage'] = [
        '#type' => 'select',
        '#title' => 'Storage',
        '#title_display' => 'invisible',
        '#options' => $storageOptions,
        '#default_value' => $this->storageManager->storageIDForVendorID($id),
      ];
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storages = [];
    foreach ($form_state->getValue('pg_vendor_storage_table') as $vendorID => $value) {
      $storages[$vendorID] = [
        'vendor' => $vendorID,
        'storage' => $value['storage'],
      ];
    }
    $this->storageManager->saveSettings($storages);

    drupal_set_message($this->t('Settings successfully saved.'));
  }

}
