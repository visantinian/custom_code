<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Form\PGVendorAPIImportQueueForm.
 */

namespace Drupal\pg_vendor_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorAPIImportQueueForm extends FormBase {

  /**
   * @var QueueFactory
   */
  protected $queueFactory;

  /**
   * @var QueueWorkerManagerInterface
   */
  protected $queueManager;


  /**
   * {@inheritdoc}
   */
  public function __construct(QueueFactory $queue, QueueWorkerManagerInterface $queue_manager) {
    $this->queueFactory = $queue;
    $this->queueManager = $queue_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'pg_vendor_api_import_queue_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queue = $this->queueFactory->get('pg_vendor_api_import_queue');

    $form['help'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Submit this form to process the Import Queue.'),
      '#prefix' => '<h3>',
      '#suffix' => '</h3>',
    ];
    $form['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Import Queue contains @number items.', ['@number' => $queue->numberOfItems()]),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process queue'),
      '#button_type' => 'primary',
      '#attributes' => ['disabled' => $queue->numberOfItems() ? FALSE : 'disabled'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var QueueInterface $queue */
    $queue = $this->queueFactory->get('pg_vendor_api_import_queue');
    /** @var QueueWorkerInterface $queue_worker */
    $queue_worker = $this->queueManager->createInstance('manual_data_importer');

    while ($item = $queue->claimItem()) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
      } catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        break;
      } catch (Exception $e) {
        watchdog_exception('pg_vendor_api', $e, 'Unable to process an queue item with error: @message in %function (line %line of %file).');
      }
    }
  }

}
