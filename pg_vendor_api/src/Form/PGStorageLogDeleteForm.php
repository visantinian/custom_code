<?php

/**
 * @file
 * Contains Drupal\pg_ts_entity\Form\PGStorageLogDeleteForm.
 */

namespace Drupal\pg_vendor_api\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a form for deleting a PGTechnicalSession entity.
 *
 * @ingroup pg_vendor_api
 */
class PGStorageLogDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "pg_vendor_api_logs_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to physically delete all Import Logs?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pg_storage_log.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dbConnection = Database::getConnection();

    $tables = [
      'pg_storage_log',
    ];

    foreach ($tables as $table) {
      $dbConnection->truncate($table)->execute();
    }

    drupal_flush_all_caches();

    $form_state->setRedirect('entity.pg_storage_log.collection');
  }

}
