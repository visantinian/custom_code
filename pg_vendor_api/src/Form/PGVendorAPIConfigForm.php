<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Form\PGVendorAPIConfigForm.
 */

namespace Drupal\pg_vendor_api\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorAPIConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiManager $pg_vendor_api_manager) {
    parent::__construct($config_factory);

    $this->vendorManager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pg_vendor_api.vendors'];
  }

  /*
  **
  * Returns a unique string identifying the form.
  *
  * @return string
  *   The unique string identifying the form.
  */
  public function getFormId() {
    return 'pg_vendor_api_config_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->vendorManager->getConfig();

    $form['pg_vendor_services_table'] = [
      '#type' => 'table',
      '#empty' => $this->t('There is no vendors yet.'),
      '#header' => [
        'title' => t('Title'),
        'description' => t('Description'),
        'operations' => t('Operations'),
        'enabled' => t('Enabled'),
      ],
    ];

    foreach ($this->vendorManager->vendors() as $id => $vendor) {
      $form['pg_vendor_services_table'][$id]['title']['#plain_text'] = $vendor->vendorTitle();
      $form['pg_vendor_services_table'][$id]['description']['#plain_text'] = $vendor->vendorDescription();

      $form['pg_vendor_services_table'][$id]['operations'] = [
        '#type' => 'operations',
      ];

      if ($vendor->enableVendor() === TRUE) {
        $form['pg_vendor_services_table'][$id]['operations']['#links'][] = [
          'type' => 'link',
          'title' => $this->t('Test API'),
          'url' => Url::fromRoute('pg_vendor_api.test_results_ajax')
            ->setRouteParameters(['vendor' => $id]),
          'ajax' => [
            'event' => 'click',
          ],
        ];
        $form['pg_vendor_services_table'][$id]['operations']['#links'][] = [
          'type' => 'link',
          'title' => $this->t('Create Queue'),
          'url' => Url::fromRoute('pg_vendor_api.create_queue')
            ->setRouteParameters(['vendor' => $id]),
          'ajax' => [
            'event' => 'click',
          ],
        ];
      }

      if ($settingURL = $vendor->settingsURL()) {
        $form['pg_vendor_services_table'][$id]['operations']['#links'][] = [
          'type' => 'link',
          'title' => $this->t('Settings'),
          'url' => $settingURL,
        ];
      }

      $form['pg_vendor_services_table'][$id]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
        '#default_value' => $config[$id]['enabled'] ?: 0,
        '#title_display' => 'invisible',
      ];

    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',

    ];

    $form['#attached']['library'][] = 'pg_vendor_api/jsonFormatter';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  public function testValidate(array &$form, FormStateInterface $form_state) {
    // do not validate test api call
  }

  public function testButton(array &$form, FormStateInterface $form_state) {
    $vendor = $this->vendorManager->vendor($form_state->getTriggeringElement()['#name']);
    if ($vendor) {
      $url = Url::fromRoute('pg_vendor_api.test_results')
        ->setRouteParameters(['vendor' => $form_state->getTriggeringElement()['#name']]);

      $form_state->setRedirectUrl($url);
    }
  }

  public function testButtonAjax(array &$form, FormStateInterface $form_state) {
    $vendor = $this->vendorManager->vendor($form_state->getTriggeringElement()['#name']);
    if ($vendor) {
      $data = $vendor->getData()->prepareTestData();
      $endpointInfo = $vendor->endpointInfo();

      $page = [];
      $page['info_wrapper'] = [
        '#type' => 'details',
        '#title' => 'Request info',
      ];

      $page['info_wrapper']['api_url'] = [
        '#type' => 'item',
        '#title' => t('API URL:'),
        '#markup' => $endpointInfo['url'],
      ];

      $params = [];
      foreach ($endpointInfo['params'] as $key => $value) {
        $params[] = '<strong>' . $key . ': </strong> ' . $value;
      }
      $page['info_wrapper']['api_params'] = [
        '#type' => 'item',
        '#title' => t('Params:'),
        '#markup' => join('<br>', $params),
      ];

      $page['info_wrapper']['full_view_link'] = [
        '#type' => 'link',
        '#title' => $this->t('Open in full window'),
        '#url' => Url::fromRoute('pg_vendor_api.test_results')
          ->setRouteParameters(['vendor' => $form_state->getTriggeringElement()['#name']]),
      ];


      $page['json_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => 'API response',
      ];

      $page['json_wrapper']['json'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => ['class' => 'json_formatter'],
        '#value' => $data,
      ];

      $content = [
        '#type' => 'markup',
        '#markup' => drupal_render($page),
        '#cache' => ['max-age' => 0,],
        '#attached' => [
          'library' => ['core/drupal.dialog.ajax'],
        ],
      ];


      $title = "Test results for " . $vendor->vendorTitle();
      $response = new AjaxResponse();
      $response->addCommand(new OpenModalDialogCommand($title, $content, [
        'width' => '1100',
        'height' => '700',
      ]));
      $response->addCommand(new InvokeCommand('html', 'trigger', [
        'pg_json_results_ready',
        [0],
      ]));
      return $response;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('pg_vendor_services_table') as $key => $value) {
      if ($value['enabled'] == 1) {
        $vendor = $this->vendorManager->vendor($key);
        if ($vendor) {
          $result = $vendor->enableVendor();
          if ($result === TRUE) {

          }
          else {
            $form_state->setErrorByName('', t('Can`t enable vendor ' . $vendor->vendorTitle() . ': ' . $result));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $services = [];
    foreach ($form_state->getValue('pg_vendor_services_table') as $key => $value) {
      $services[$key] = [
        'vendor' => $key,
        'enabled' => $value['enabled'],
      ];
    }
    $this->vendorManager->saveSettings($services);
    drupal_set_message($this->t('Settings successfully saved.'));
  }

}
