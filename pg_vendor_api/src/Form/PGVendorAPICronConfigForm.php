<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Form\PGVendorAPICronConfigForm.
 */

namespace Drupal\pg_vendor_api\Form;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pg_vendor_api\Classes\PGEmptyStorage;
use Drupal\pg_vendor_api\Classes\PGVendorApiManager;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorAPICronConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $vendorManager;

  protected $storageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    PGVendorApiManager $pg_vendor_api_manager,
    PGVendorApiStorageManager $pg_vendor_api_storage_manager
  ) {
    parent::__construct($config_factory);

    $this->vendorManager = $pg_vendor_api_manager;
    $this->storageManager = $pg_vendor_api_storage_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api.manager'),
      $container->get('pg_vendor_api.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pg_vendor_api.vendors', 'pg_vendor_api.storages'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pg_vendor_api_cron_config_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $cronConfig = Drupal::config('pg_vendor_api.cron');

    $form['pg_vendor_cron_table'] = [
      '#type' => 'table',
      '#empty' => $this->t('There is no vendors yet.'),
      '#header' => [
        'vendor' => t('Vendor'),
        'enabled' => t('Enabled'),
        'config' => t('Cron configuration'),
      ],
    ];

    $storagesConfig = $this->storageManager->getConfig();

    $storages = $this->storageManager->storages();

    $EmptyStorageId = PGEmptyStorage::getStorageID();

    $cronIntervals = [1800, 3600, 10800, 21600, 43200, 86400, 604800];
    $cronOptions = [1 => t('Each Cron Run')] + array_map([
        Drupal::service('date.formatter'),
        'formatInterval',
      ], array_combine($cronIntervals, $cronIntervals));

    foreach ($this->vendorManager->vendors() as $id => $vendor) {
      $form['pg_vendor_cron_table'][$id]['vendor']['#plain_text'] = $vendor->vendorDescription();

      if ($vendor->enableVendor() === TRUE) {
        if ($storagesConfig[$id]['storage'] == $EmptyStorageId) {
          $form['pg_vendor_cron_table'][$id]['enabled'] = [
            '#type' => 'checkbox',
            '#title' => t('Enabled'),
            '#default_value' => FALSE,
            '#title_display' => 'invisible',
            '#disabled' => TRUE,
          ];

          $form['pg_vendor_cron_table'][$id]['config']['#markup'] = t('Incomplete Vendor Storage <a href=":url">settings</a>. ', [':url' => Drupal::url('pg_vendor_api.storage_config')]);
        }
        else {
          $form['pg_vendor_cron_table'][$id]['enabled'] = [
            '#type' => 'checkbox',
            '#title' => t('Enabled'),
            '#default_value' => $cronConfig->get('cron.' . $id . '.enabled') ? $cronConfig->get('cron.' . $id . '.enabled') : FALSE,
            '#title_display' => 'invisible',
          ];

          $form['pg_vendor_cron_table'][$id]['config'] = [
            '#type' => 'select',
            '#title' => t('Import vendor data every'),
            '#description' => t('Storage specified: %storage.', ['%storage' => $storages[$storagesConfig[$id]['storage']]->storageName()]),
            '#default_value' => $cronConfig->get('cron.' . $id . '.interval'),
            '#options' => $cronOptions,
          ];
        }
      }
      else {
        $form['pg_vendor_cron_table'][$id]['enabled'] = [
          '#type' => 'checkbox',
          '#title' => t('Enabled'),
          '#default_value' => FALSE,
          '#title_display' => 'invisible',
          '#disabled' => TRUE,
        ];

        $form['pg_vendor_cron_table'][$id]['config']['#markup'] = t('Incomplete Vendor <a href=":url">settings</a>. ', [':url' => Drupal::url('pg_vendor_api.config')]);
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = Drupal::configFactory()->getEditable('pg_vendor_api.cron');
    foreach ($form_state->getValue('pg_vendor_cron_table') as $key => $value) {
      $config->set('cron.' . $key . '.enabled', $value['enabled'] ? TRUE : FALSE);
      $config->set('cron.' . $key . '.interval', is_numeric($value['config']) ? (int) $value['config'] : 3600);
    }
    $config->save();
  }

}
