<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api\Form\PGVendorAPISettingsForm.
 */

namespace Drupal\pg_vendor_api\Form;

use Drupal;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PGVendorAPISettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $vendorManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pg_vendor_api.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pg_vendor_api_settings_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['settings']['api_requests'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('API Requests'),
    ];

    $cronIntervals = [21600, 43200, 86400, 172800, 259200, 604800, 2592000];
    $cleanupOptions = [0 => t('Preserve all')] + array_map([
        Drupal::service('date.formatter'),
        'formatInterval',
      ], array_combine($cronIntervals, $cronIntervals));

    $offset = $this->config('pg_vendor_api.settings')
      ->get('api_requests.offset');
    if (!$offset) {
      $offset = 0;
    }

    $form['settings']['api_requests']['offset'] = [
      '#type' => 'select',
      '#options' => $cleanupOptions,
      '#title' => $this->t('Clean up all API Requests older than'),
      '#default_value' => $offset,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValue('settings');

    if ($settings) {
      $config = $this->config('pg_vendor_api.settings');
      foreach ($settings as $group => $values) {
        foreach ($values as $key => $value) {
          $config->set($group . '.' . $key, $value);
        }
      }
      $config->save();

      drupal_set_message($this->t('Configuration saved'));
    }
  }

}
