<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\PGStorageLogItemAccessControlHandler.
 */

namespace Drupal\pg_vendor_api;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the ExempleEntity entity.
 *
 * @see \Drupal\pg_vendor_api\Entity\ExempleEntity.
 */
class PGStorageLogItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view pg_storage_log entity');
        break;

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit pg_storage_log entity');
        break;

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete pg_storage_log entity');
        break;

    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add pg_storage_log entity');
  }

}
