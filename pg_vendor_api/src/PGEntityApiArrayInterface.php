<?php

/**
 * @file
 * Contains Drupal\pg_vendor_api\PGEntityApiArrayInterface.
 */

namespace Drupal\pg_vendor_api;

interface PGEntityApiArrayInterface {

  public function toApiArray();

}
