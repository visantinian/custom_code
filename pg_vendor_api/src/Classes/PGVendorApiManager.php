<?php

namespace Drupal\pg_vendor_api\Classes;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\pg_api_request_entity\Entity\PGApiRequestEntity;
use Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntity;

class PGVendorApiManager {

  protected $settings;

  protected $vendors = [];

  public function __construct(ConfigFactoryInterface $settings) {
    $this->settings = $settings->getEditable('pg_vendor_api.vendors');
  }

  public function addVendor(PGVendorApiInterface $vendor) {
    $this->vendors[$vendor->vendorID()] = $vendor;
    return $this;
  }

  public function vendors() {
    return $this->vendors;
  }

  public function vendor($vendorID) {
    if (array_key_exists($vendorID, $this->vendors)) {
      return $this->vendors[$vendorID];
    }

    return FALSE;
  }

  public function saveSettings(array $vendors) {
    $this->settings->set('vendors', $vendors);
    $this->settings->save();
  }

  public function createRequest() {
    $request = PGApiRequestEntity::create([]);
    $request->save();
    return $request->id();
  }

  public function startRequest($requestID) {
    $config = $this->getConfig();
    $operations = [];
    foreach ($this->vendors as $vendorID => $vendor) {
      if ($config[$vendorID] && $config[$vendorID]['enabled'] == 1) {
        $operations[] = [
          'pg_vendor_api_batch_request',
          [$vendorID, $requestID],
        ];
      }
    }

    if (empty($operations)) {
      drupal_set_message(t('There are no enabled providers'), 'error');
      return FALSE;
    }

    $batch = [
      'operations' => $operations,
      'finished' => 'pg_vendor_api_batch_request_finished',
      'title' => t('Loading data'),
      'init_message' => t('Batch is starting'),
      'progress_message' => 'Loaded @current out of @total',
      'error_message' => t('It has an error.'),
    ];
    batch_set($batch);
  }

  public function getConfig() {
    return $this->settings->get('vendors');
  }

  public function getRequest($requestID) {
    return PGApiRequestEntity::load($requestID);
  }

  public function getData($dataID) {
    return PGVendorDataEntity::load($dataID);
  }

}
