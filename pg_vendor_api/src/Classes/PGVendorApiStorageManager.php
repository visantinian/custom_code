<?php

namespace Drupal\pg_vendor_api\Classes;

use Drupal\Core\Config\ConfigFactoryInterface;


class PGVendorApiStorageManager {

  protected $storages = [];

  protected $settings;

  public function __construct(ConfigFactoryInterface $settings) {
    $this->settings = $settings->getEditable('pg_vendor_api.storages');
  }

  public function addStorage(PGVendorApiStorageInterface $storage) {
    $this->storages[$storage->storageID()] = $storage;
    return $this;
  }

  public function storages() {
    return $this->storages;
  }

  public function saveSettings(array $storages) {
    $this->settings->set('storages', $storages);
    $this->settings->save();
  }

  public function getConfig() {
    return $this->settings->get('storages');
  }

  public function storageIDForVendorID($vendorID) {
    $storageID = $this->settings->get('storages')[$vendorID];
    if (empty($storageID)) {
      $storageID = 'pg_empty_storage';
    }

    return $storageID;
  }

  public function storageForVendorID($vendorID) {
    // var_dump($this->settings->get('storages'));
    // \Drupal::logger('PGVendorApiStorageManager.php')->notice("" . print_r($this->storages, 1) . "");
    $storageID = $this->settings->get('storages')[$vendorID]['storage'];
    if (empty($storageID)) {
      $storageID = 'pg_empty_storage';
    }

    return $this->storage($storageID);
  }

  public function storage($storageID) {
    return $this->storages[$storageID];
  }

}
