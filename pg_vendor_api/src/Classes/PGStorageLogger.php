<?php

namespace Drupal\pg_vendor_api\Classes;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class PGStorageLogger {

  protected $entityStorage;

  protected $logs;

  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityStorage = $entity_manager->getStorage('pg_storage_log');
    $this->log = [];
  }

  public function add(string $entityType, $entityID, string $action, $description = '') {
    if ($action != PGStorageLoggerAction::NotChanged) {
      $this->logs[] = $this->entityStorage->create([
        'entity_type' => $entityType,
        'entity_id' => $entityID,
        'action' => $action,
        'description' => $description,
      ]);
    }
  }

  public function save() {
    if (!empty($this->logs)) {
      foreach ($this->logs as $log) {
        $log->save();
      }
      $this->logs = [];
    }
  }

}
