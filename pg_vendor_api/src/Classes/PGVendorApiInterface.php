<?php

namespace Drupal\pg_vendor_api\Classes;

interface PGVendorApiInterface {

  public function vendorData();

  public function vendorID();

  public function vendorTitle();

  public function vendorDescription();

  public function getData();

  public function saveData($requestID);

  public function fetchData($requestID);

  public function enableVendor();

  public function endpointInfo();

  public function settingsURL();

  public function prepareTestData();

}
