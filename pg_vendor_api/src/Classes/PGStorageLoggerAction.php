<?php

namespace Drupal\pg_vendor_api\Classes;

abstract class PGStorageLoggerAction {

  const Created = 'new';

  const Updated = 'updated';

  const NotChanged = 'not changed';

}
