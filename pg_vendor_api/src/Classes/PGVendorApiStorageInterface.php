<?php

namespace Drupal\pg_vendor_api\Classes;

interface PGVendorApiStorageInterface {

  public function saveFromData($data);

  public function storageID();

  public function storageName();

  public function importBatchFunctions();

}
