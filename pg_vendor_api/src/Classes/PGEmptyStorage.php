<?php

namespace Drupal\pg_vendor_api\Classes;

class PGEmptyStorage implements PGVendorApiStorageInterface {

  public static function getStorageID() {
    return 'pg_empty_storage';
  }

  public function saveFromData($data) {
    // do nothing
  }

  public function storageID() {
    return 'pg_empty_storage';
  }

  public function storageName() {
    return 'Do not save';
  }

  public function importBatchFunctions() {
    return [];
  }

}
