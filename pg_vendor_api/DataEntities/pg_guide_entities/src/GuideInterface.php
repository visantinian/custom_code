<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\GuideInterface.
 */

namespace Drupal\pg_guide_entities;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a Guide entity.
 */
interface GuideInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.
}
