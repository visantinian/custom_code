<?php

namespace Drupal\pg_guide_entities\Classes;

use Drupal;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageInterface;


class GuidesStorage implements PGVendorApiStorageInterface {

  protected $logger;

  public function __construct(EntityTypeManagerInterface $entity_manager, $logger) {
    $this->entityManager = $entity_manager;
    $this->logger = $logger;
  }

  public function check($data) {
  }

  public function saveFromData($data) {
  }

  static function checkGuides($data) {
    $checked = $data;
    foreach ($data as $guide_key => $guide) {
        $to_update = [];
      $guide_code = $guide->code;
      $query = \Drupal::entityQuery('pg_guide');
      $exists = $query->condition('viator_code', $guide_code)->execute();
      if (count($exists) > 1){
        self::purgeReplicants($exists);
        $exists = $query->condition('viator_code', $guide_code)->execute();
      }
      if ($exists != []) {
        unset($checked[$guide_key]);
        $entity = \Drupal::entityTypeManager()
          ->getStorage('pg_guide')
          ->load(key($exists));
        if ($entity && $entity->hasTranslation('en')){
          $entity = $entity->getTranslation('en');
        }
        if ($entity){
          if ($guide->currencyCode != @$entity->get('currency_code')->getValue()[0]['value']) {
            $to_update[] = 'currency_code';
          }
          if ($guide->webURL != @$entity->get('viator_url')->getValue()[0]['uri']){
            $to_update[] = 'viator_url';
          }
          if ($guide->onRequestPeriod != @$entity->get('on_request_period')->getValue()[0]['value']){
            $to_update[] = 'on_request_period';
          }
          if ($guide->shortTitle != @$entity->get('title')->getValue()[0]['value']){
            $to_update[] = 'title';
          }
          if ($guide->shortDescription != @$entity->get('short_description')->getValue()[0]['value']){
            $to_update[] = 'short_description';
          }
          if ((string) $guide->price != @$entity->get('viator_price')->getValue()[0]['value']){
            $to_update[] = 'viator_price';
          }
          if ((string) $guide->reviewCount != @$entity->get('review_count')->getValue()[0]['value']){
            $to_update[] = 'review_count';
          }
          if ($guide->duration != @$entity->get('duration')->getValue()[0]['value']) {
            $to_update[] = 'duration';
          }
          if ($entity->hasField('field_rating')){
            if ($guide->rating != @$entity->get('field_rating')->getValue()[0]['value']) {
              $to_update[] = 'field_rating';
            }
          }
          self::updateGuide($guide, $entity, $to_update);
        }
      }
    }
    return $checked;
  }

  static function purgeReplicants($eids){
    $keys = array_keys($eids);
    unset($eids[$keys[0]]);
    foreach ($eids as $eid){
      $entity = Drupal::entityTypeManager()
        ->getStorage('pg_guide')->load($eid);
      if ($entity){
        $entity->delete();
      }
    }
  }

  static function updateGuide($guide, $entity, $field_to_updates = []) {
   foreach ($field_to_updates  as $field_to_update) {
       switch ($field_to_update){
           case 'currency_code':
               $entity->set($field_to_update, $guide->currencyCode);
               break;
           case 'viator_url':
               $entity->set($field_to_update, $guide->webURL);
               break;
           case 'on_request_period':
               $entity->set($field_to_update, $guide->onRequestPeriod);
               break;
           case 'title':
               $entity->set($field_to_update, $guide->shortTitle);
               break;
           case 'short_title':
               $entity->set($field_to_update, $guide->title);
               break;
           case 'short_description':
               $entity->set($field_to_update, $guide->shortDescription);
               break;
           case 'viator_price':
               $entity->set($field_to_update, $guide->price);
               break;
           case 'review_count':
               $entity->set($field_to_update, $guide->reviewCount);
               break;
           case 'duration':
               $entity->set($field_to_update, $guide->duration);
               break;
           case 'field_rating':
               $entity->set($field_to_update, $guide->rating);
               break;
           default:
               break;
       }
   }
   $entity->set('path', strtolower(transliterator_transliterate(
          'Russian-Latin/BGN',
          urldecode($guide->productUrlName)
   )));
   $entity->set('viator_suggested_price', $guide->priceFormatted);
   $entity->set('review_count', $guide->reviewCount);
   $entity->set('viator_destination_id', $guide->primaryDestinationId);
   $entity->set('viator_destination_name', $guide->primaryDestinationName);
   $entity->save();
  }

  static function saveGuideFromData($data) {
    $guide = $data;
    $values = [];
    $values['currency_code'] = $guide->currencyCode;
    if (@$guide->catIds){
      foreach ($guide->catIds as $category) {
        $values['categories'][] = $category;
      }
    }
    if (@$guide->subCatids){
      foreach ($guide->subCatIds as $category) {
        $values['categories'][] = $category;
      }
    }
    $values['langcode'] = 'en';
    $values['viator_url'] = $guide->webURL;
    $values['on_request_period'] = $guide->onRequestPeriod;
    $values['path'] = strtolower(transliterator_transliterate(
      'Russian-Latin/BGN',
      urldecode($guide->productUrlName)
    ));
    $values['title'] = $guide->shortTitle;
    $values['title_full'] = $guide->title;
    $values['short_description'] = $guide->shortDescription;
    $values['viator_price'] = $guide->price;
    $values['viator_suggested_price'] = $guide->priceFormatted;
    $values['review_count'] = $guide->reviewCount;
    $values['viator_destination_id'] = $guide->primaryDestinationId;
    $values['viator_destination_name'] = $guide->primaryDestinationName;
    $values['photo_links'][] = $guide->thumbnailURL;
    $values['photo_links'][] = $guide->thumbnailHiResURL;
    $values['duration'] = $guide->duration;
    $values['viator_code'] = $guide->code;
    $values['viator_attraction_id'] = $guide->seoId;
    $values['status'] = 0;
    $values['field_rating'] = $guide->rating;
    $entity = Drupal::entityTypeManager()
      ->getStorage('pg_guide')
      ->create($values);
    $entity->save();
    unset($values['langcode']);
    $entity->addTranslation('ru', $values)->save();
  }

  public function storageID() {
    return 'pg_guide_storage';
  }

  public function storageName() {
    return 'Guides Storage';
  }

  public function importBatchFunctions() {
    return 'pg_guide_entities_guide_import';
  }

}
