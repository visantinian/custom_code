<?php

namespace Drupal\pg_guide_entities\Classes;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pg_vendor_api\Classes\PGVendorApiStorageInterface;


class GuideReviewsStorage implements PGVendorApiStorageInterface {


  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  public function check($data) {

  }

  public function saveFromData($data) {
  }

  static function checkGuideReviews($data) {
    $checked = $data;
    foreach ($data as $review_key => $review) {
      $review_viator_id = $review->reviewId;
      $query = \Drupal::entityQuery('pg_guide_review');
      $exists = $query->condition('viator_review_id', $review_viator_id)->execute();
      if ($exists != []) {
        unset($checked[$review_key]);
        $entity = \Drupal::entityTypeManager()
          ->getStorage('pg_guide_review')
          ->load(key($exists));
        if ($entity->hasTranslation('en')) {
          $entity = $entity->getTranslation('en');
        }
        if ($review->rating != $entity->get('review')
            ->getValue()[0]['value']
          || $review->productCode != $entity->get('viator_guide_id')
            ->getValue()[0]['value']
          || $review->reviewId != $entity->get('viator_review_id')
            ->getValue()[0]['value']
          || $review->ownerName != $entity->get('viator_reviewer_name')
            ->getValue()[0]['value']
          || $review->ownerCountry != $entity->get('viator_reviewer_country')
            ->getValue()[0]['value']
        ) {
          self::updateGuideReview($review, $entity);
        }
      }
    }
    return $checked;
  }

  static function updateGuideReview($review, $entity){

    $entity->set('review', $review->rating);
    $entity->set('viator_guide_id', $review->productCode );
    $entity->set('viator_review_id', $review->reviewId);
    $entity->set('viator_reviewer_name', $review->ownerName);
    $entity->set('viator_reviewer_country', $review->ownerCountry);
    $entity->save();
  }

  static function saveGuideReviewFromData($data) {
    $guide_review = $data;
    $values = [];
    $values['langcode'] = 'en';
    $values['rating'] = $guide_review->rating;
    $values['review'] = $guide_review->review;
    $vendordate = $guide_review->publishedDate;
    $stringdate = preg_replace('/[T](.+)/m', '', $vendordate);
    $values['submission_date'] = $stringdate;
    $values['viator_guide_id'] = $guide_review->productCode;
    $values['viator_review_id'] = $guide_review->reviewId;
    $values['viator_reviewer_name'] = $guide_review->ownerName;
    $values['viator_reviewer_country'] = $guide_review->ownerCountry;
    $values['status'] = 0;
    $entity = \Drupal::entityTypeManager()
      ->getStorage('pg_guide_review')
      ->create($values);
    unset($values['langcode']);
    $entity->addTranslation('ru', $values)->save();
  }

  public function storageID() {
    return 'pg_guide_reviews_storage';
  }

  public function storageName() {
    return 'Guides Reviews Storage';
  }

  public function importBatchFunctions() {
    return 'pg_guide_entities_guide_reviews_import';
  }

}
