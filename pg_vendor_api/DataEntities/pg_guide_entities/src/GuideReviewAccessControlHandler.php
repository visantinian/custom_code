<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\GuideAccessControlHandler.
 */

namespace Drupal\pg_guide_entities;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Guide Review Entity entity.
 *
 * @see \Drupal\pg_guide_entities\Entity\ExempleEntity.
 */
class GuideReviewAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view GuideReview entity');
        break;

      case 'edit':
        return AccessResult::allowedIfHasPermission($account, 'edit GuideReview entity');
        break;

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete GuideReview entity');
        break;

    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add GuideReview entity');
  }

}
