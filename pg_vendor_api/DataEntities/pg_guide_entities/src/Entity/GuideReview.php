<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\Entity\GuideReview.
 */

namespace Drupal\pg_guide_entities\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;

use Drupal\pg_guide_entities\GuideReviewInterface;

/**
 * Defines the Guide Review entity.
 *
 * @ingroup pg_guide_entities
 *
 * @ContentEntityType(
 *   id = "pg_guide_review",
 *   label = @Translation("Guide Review"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\pg_guide_entities\Entity\GuideReviewEntityViewsData",
 *     "list_builder" = "Drupal\pg_guide_entities\Entity\Controller\GuideReviewListController",
 *
 *     "form" = {
 *       "edit" = "Drupal\pg_guide_entities\Entity\Form\GuideReviewForm",
 *       "delete" = "Drupal\pg_guide_entities\Entity\Form\GuideReviewDeleteForm",
 *     },
 *     "access" = "Drupal\pg_guide_entities\GuideReviewAccessControlHandler",
 *   },
 *   base_table = "pg_guide_review",
 *   data_table = "pg_guide_review_field_data",
 *   revision_table = "pg_guide_review_revision",
 *   revision_data_table = "pg_guide_review_field_revision",
 *   admin_permission = "administer Guide Review entity",
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   show_revision_ui = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "revision" = "revision_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/review/{pg_guide_review_entity}",
 *     "edit-form" = "/review/{pg_guide_review_entity}/edit",
 *     "delete-form" = "/review/{pg_guide_review_entity}/delete",
 *     "collection" = "/admin/content/pg_guide_review"
 *   },
 *   field_ui_base_route = "pg_guide_review.settings"
 * )
 */
class GuideReview extends RevisionableContentEntityBase implements GuideReviewInterface {

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the PG Guide Entity.'))
      ->setReadOnly(TRUE);

    $fields['revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel('Revision ID')
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the PG Guide Entity.'))
      ->setReadOnly(TRUE);

    $fields['rating'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Rating'))
      ->setDescription(t('Rating given by the reviewer'))
      ->setSettings([
        'max_length' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['submission_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Submission Date'))
      ->setDescription(t('date that this review was submitted'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['review'] = self::longTextFieldWithLabel('Review');

    $fields['viator_review_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Viator Review ID'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['viator_guide_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Viator Guide ID'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The submission language code.'));

    $fields['viator_reviewer_name'] = self::stringFieldWithLabel('Reviewer\'s Name');
    $fields['viator_reviewer_country'] = self::stringFieldWithLabel('Reviewer\'s Country');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  protected static function longTextFieldWithLabel($label = '') {
    return BaseFieldDefinition::create('text_long')
      ->setLabel($label)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  protected static function stringFieldWithLabel($label = '', $type = 'string') {
    return BaseFieldDefinition::create($type)
      ->setLabel(t($label))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

}

