<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\Entity\Guide.
 */

namespace Drupal\pg_guide_entities\Entity;

use Drupal;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\pg_guide_entities\GuideInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Guide entity.
 *
 * @ingroup pg_guide_entities
 *
 * @ContentEntityType(
 *   id = "pg_guide",
 *   label = @Translation("Guide"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\pg_guide_entities\Entity\GuideViewsData",
 *     "list_builder" = "Drupal\pg_guide_entities\Entity\Controller\GuideListController",
 *
 *     "form" = {
 *       "edit" = "Drupal\pg_guide_entities\Entity\Form\GuideForm",
 *       "delete" = "Drupal\pg_guide_entities\Entity\Form\GuideDeleteForm",
 *     },
 *     "access" = "Drupal\pg_guide_entities\GuideAccessControlHandler",
 *   },
 *   base_table = "pg_guide",
 *   data_table = "pg_guide_field_data",
 *   revision_table = "pg_guide_revision",
 *   revision_data_table = "pg_guide_field_revision",
 *   admin_permission = "administer Guide entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "revision" = "revision_id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "published" = "status",
 *   },
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   fieldable = TRUE,
 *   links = {
 *     "add-form" = "/admin/content/pg_guide/add",
 *     "canonical" = "/guide/{pg_guide}",
 *     "edit-form" = "/guide/{pg_guide}/edit",
 *     "delete-form" = "/guide/{pg_guide}/delete",
 *     "collection" = "/admin/content/pg_guide"
 *   },
 *   field_ui_base_route = "pg_guide.settings"
 * )
 */
class Guide extends RevisionableContentEntityBase implements GuideInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new guide entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the PG Guide Entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the PG Guide Entity.'))
      ->setReadOnly(TRUE);
    $fields['revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel('Revision ID')
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the attraction author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['currency_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Currency Code'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 10,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language'))
      ->setDescription(t('The submission language code.'));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 900,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['viator_attraction_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Viator Attraction ID'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title_full'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title Full'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 900,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['viator_code'] = self::stringFieldWithLabel('Viator Guide Code');
    $fields['duration'] = self::stringFieldWithLabel('Duration');
    $fields['viator_destination_name'] = self::stringFieldWithLabel('Destination Name');
    $fields['short_description'] = self::longTextFieldWithLabel('Short Description');


    $fields['categories'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Category(ies)'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['guides_categories' => 'guides_categories']])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['subcategories'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Subcategory(ies)'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', ['target_bundles' => ['guides_categories' => 'guides_categories']])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 3,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 200)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['on_request_period'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('On request period'))
      ->setDescription(t('Number of hours before the travel date that this product will be \'on-request\' for'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['viator_price'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Viator Price'))
      ->setDescription(t('Numeric price of this product in the currency indicated by currencyCode'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['viator_suggested_price'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Viator Price Suggested'))
      ->setDescription(t('Numeric price of this product in the currency indicated by currencyCode'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['review_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Review Count'))
      ->setDescription(t('Number of reviews published for this product on viator'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['viator_destination_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Viator destination id'))
      ->setDescription(t('unique numeric identifier of this product\'s primary destination'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['photo_links'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Viator Photo links'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['viator_url'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Viator URL'))
      ->setDescription(t('URL to forward users to in order to complete their purchase on the Viator site'))
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  protected static function stringFieldWithLabel($label = '', $type = 'string') {
    return BaseFieldDefinition::create($type)
      ->setLabel(t($label))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  protected static function longTextFieldWithLabel($label = '') {
    return BaseFieldDefinition::create('text_long')
      ->setLabel($label)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the rrrrr owner the revision author.
    if (!@$this->getRevisionUser()) {
      @$this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

}
