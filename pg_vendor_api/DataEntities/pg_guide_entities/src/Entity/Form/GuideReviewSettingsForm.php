<?php

/**
 * @file
 * Contains
 *   Drupal\pg_guide_reviews\Entity\Form\GuideSettingsForm.
 */

namespace Drupal\pg_guide_entities\Entity\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GuideReviewSettingsForm.
 *
 * @package Drupal\pg_guide_entities\Form
 *
 * @ingroup pg_guide_entities
 */
class GuideReviewSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pg_guide_review_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Define the form used for Guide  settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['pg_guide_review_settings']['#markup'] = 'Settings form for Guide. Manage field settings here.';
    return $form;
  }

}
