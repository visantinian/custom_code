<?php

/**
 * @file
 * Contains
 *   Drupal\pg_guide_reviews\Entity\Form\GuideReviewDeleteForm.
 */

namespace Drupal\pg_guide_entities\Entity\Form;

use Drupal;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


/**
 * Provides a form for deleting a GuideReview entity.
 *
 * @ingroup pg_guide_entities
 */
class GuideReviewDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.pg_guide_review.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    Drupal::logger('pg_guide_reviews')
      ->notice('@type: deleted %title.',
        [
          '@type' => $this->entity->bundle(),
          '%title' => $this->entity->label(),
        ]);
    $form_state->setRedirect('entity.pg_guide_review.collection');
  }

}
