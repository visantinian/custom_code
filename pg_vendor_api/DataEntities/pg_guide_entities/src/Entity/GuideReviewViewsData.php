<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\Entity\GuideViewsData.
 */

namespace Drupal\pg_guide_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data for the GuideReview entity type.
 */
class GuideReviewViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pg_guide_review']['table']['base'] = [
      'field' => 'id',
      'title' => t('Guide Review ID'),
      'help' => t('The pg_guide_review ID.'),
    ];

    return $data;
  }

}
