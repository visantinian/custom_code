<?php

/**
 * @file
 * Contains Drupal\pg_guide_entities\Entity\GuideViewsData.
 */

namespace Drupal\pg_guide_entities\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data for the Guide entity type.
 */
class GuideViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['pg_guide']['table']['base'] = [
      'field' => 'id',
      'title' => t('Guide ID'),
      'help' => t('The pg_guide ID.'),
    ];

    return $data;
  }

}
