<?php

/**
 * @file
 * Contains
 *   Drupal\pg_exh_entities\Entity\Controller\PGProductEntityListController.
 */

namespace Drupal\pg_guide_entities\Entity\Controller;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;


/**
 * Provides a list controller for PGProductEntity entity.
 *
 * @ingroup pg_guide_entities
 */
class GuideReviewListController extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Content Entity Example implements a PGProductEntity model. These contacts are fieldable entities. You can manage the fields on the <a href="@adminlink">PGProductEntity admin page</a>.', [
        '@adminlink' => Drupal::urlGenerator()
          ->generateFromRoute('pg_guide_review.settings'),
      ]),
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('Guide Review ID');
    $header['viator_reviewer_name'] = t('Name of the reviewer');
    $header['viator_reviewer_country'] = $this->t('Country of the reviewer');
    $header['review'] = $this->t('Review Text');
    $header['rating'] = $this->t('Review Rating');
    $header['submission_date'] = $this->t('Submission Date');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $current_langcode = Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();
    $row['id'] = $entity->id();
    if ($entity->hasTranslation($current_langcode)) {
      $row['viator_reviewer_name'] = $entity->getTranslation($current_langcode)
        ->get('viator_reviewer_name')
        ->getValue()[0]['value'];
      $row['viator_reviewer_country'] = $entity->getTranslation($current_langcode)
        ->get('viator_reviewer_country')
        ->getValue()[0]['value'];
      $row['review'] = $entity->getTranslation($current_langcode)
        ->get('review')
        ->getValue()[0]['value'];
      $row['rating'] = $entity->getTranslation($current_langcode)
        ->get('rating')
        ->getValue()[0]['value'];
      $row['submission_date'] = $entity->getTranslation($current_langcode)
        ->get('submission_date')
        ->getValue()[0]['value'];
    }
    if (!$entity->hasTranslation($current_langcode)) {
      $row['viator_reviewer_name'] = $entity->get('viator_reviewer_name')
        ->getValue()[0]['value'];
      $row['viator_reviewer_country'] = $entity->get('viator_reviewer_country')
        ->getValue()[0]['value'];
      $row['review'] = $entity->get('review')->getValue()[0]['value'];
      $row['rating'] = $entity->get('rating')->getValue()[0]['value'];
      $row['submission_date'] = $entity->get('submission_date')->getValue()[0]['value'];
    }
    return $row + parent::buildRow($entity);
  }

}
