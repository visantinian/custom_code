<?php

/**
 * @file
 * Contains
 *   Drupal\pg_exh_entities\Entity\Controller\PGExhibitorEntityListController.
 */

namespace Drupal\pg_guide_entities\Entity\Controller;

use Drupal;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;


/**
 * Provides a list controller for Guide entity.
 *
 * @ingroup pg_guide_entities
 */
class GuideListController extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Content Entity Example implements a Guide model.
        These guides are fieldable entities. You can manage the fields on the
        <a href="@adminlink">Guide admin page</a>.', [
        '@adminlink' => Drupal::urlGenerator()
          ->generateFromRoute('pg_guide.settings'),
      ]),
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = t('Guide ID');
    $header['title'] = t('Title');
    $header['viator_price'] = $this->t('Viator Price');
    $header['review_count'] = $this->t('Review Count');
    $header['average_rating'] = $this->t('Average Rating');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $current_langcode = Drupal::languageManager()
      ->getCurrentLanguage()
      ->getId();
    $row['id'] = $entity->id();
    if ($entity->hasTranslation($current_langcode)) {
      $row['title'] = $entity->getTranslation($current_langcode)
        ->get('title')
        ->getValue()[0]['value'];
      $row['viator_price'] = $entity->getTranslation($current_langcode)
        ->get('viator_price')
        ->getValue()[0]['value'];
      $row['review_count'] = $entity->getTranslation($current_langcode)
        ->get('review_count')
        ->getValue()[0]['value'];
      $row['average_rating'] = $entity->getTranslation($current_langcode)
        ->get('field_rating')
        ->getValue()[0]['value'];
    }
    if (!$entity->hasTranslation($current_langcode)) {
      $row['title'] = $entity->get('title')->getValue()[0]['value'];
      $row['viator_price'] = $entity->get('viator_price')
        ->getValue()[0]['value'];
      $row['review_count'] = $entity->get('review_count')
        ->getValue()[0]['value'];
      $row['average_rating'] = $entity->get('field_rating')
        ->getValue()[0]['value'];
    }
    return $row + parent::buildRow($entity);
  }

}
