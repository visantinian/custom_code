<?php


namespace Drupal\pg_vendor_api_guide\Plugin\QueueWorker;

use Drupal\pg_vendor_api_guide\Classes\PGVendorApiGuideManager;
use Drupal\pg_guide_entities\Classes\GuidesStorage;
use Drupal\Core\Queue\QueueWorkerBase;
use HTTP_Request2;

/**
 * Processes Tasks for getting the quides that needs to be updated.
 *
 * @QueueWorker(
 *   id = "guides_processor_queue",
 *   title = @Translation("Fetcher for guides that needs to be updated")
 * )
 */
class GuidesQueueProcessor extends QueueWorkerBase
{

    public function processItem($viator_id)
    {
        $settings = \Drupal::service('config.factory')->getEditable('pg_vendor_api_guide.settings');
        $request = new HTTP_Request2();
        $url = PGVendorApiGuideManager::API_URL . $settings->get('api_key');
        $request->setUrl($url);
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig([
        'follow_redirects' => true,
        ]);
        $request->setHeader([
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        ]);
        $request->setBody('{"seoId": ' . $viator_id . ' }');
        try {
            $response = $request->send();
            if ($response->getStatus() == 200) {
                $current_data = json_decode($response->getBody())->data;
                foreach ($current_data as $attraction_data) {
                    $attraction_data->seoId = $viator_id;
                }
                $vendorData[] = $current_data;
            } else {
                Drupal::logger('PG Guide Queue')
                ->error('Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                  $response->getReasonPhrase());
            }
        } catch (HTTP_Request2_Exception $e) {
            Drupal::logger('PG Guide Queue')->error('Error: ' . $e->getMessage());
        }
        foreach ($vendorData as $guides) {
          $guides = GuidesStorage::checkGuides($guides);
          foreach ($guides as $guide){
            GuidesStorage::saveGuideFromData($guide);
          }
        }
    }

}
