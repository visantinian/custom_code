<?php

namespace Drupal\pg_vendor_api_guide\Classes;


use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\pg_vendor_api\Classes\PGVendorApiInterface;
use Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntity;
use HTTP_Request2;

class PGVendorApiGuideManager implements PGVendorApiInterface {

  protected $vendorData = [];

  const API_URL = 'https://viatorapi.viator.com/service/search/products?apiKey=';

  protected $entityManager;

  protected $client;

  protected $token;

  public function __construct(ConfigFactoryInterface $settings, EntityTypeManagerInterface $entity_manager) {
    $this->settings = $settings->getEditable('pg_vendor_api_guide.settings');
    $this->entityManager = $entity_manager;
  }

  public function vendorDescription() {
    return $this->vendorTitle() . ' data from http://viator.com';
  }

  public function vendorTitle() {
    return 'Guide';
  }

  public function vendorData() {
    return $this->vendorData;
  }

  public function prepareTestData() {
    return htmlspecialchars($this->vendorData);
  }

  public function fetchData($requestID) {
    return $this->getData()->saveData($requestID);
  }

  public function saveData($requestID) {
    if ($this->vendorData) {
      $data = PGVendorDataEntity::create([
        'vendor_id' => $this->vendorID(),
        'vendor_name' => $this->vendorTitle(),
        'pg_request_id' => $requestID,
        'data' => serialize($this->vendorData),
      ]);
      $data->save();
      return $data->id();
    }
    return 0;
  }

  public function vendorID() {
    return 'pg_guide_api';
  }

  public function prepareQueue() {
    $queue = \Drupal::queue('guides_processor_queue');
    $queue->createQueue();
    $ids = $this->prepareData(NULL);
    if ($ids) {
      $viator_ids = $this->getViatorIds($ids);
    }
    foreach ($viator_ids as $viator_id){
      $queue->createItem($viator_id);
    }
    Drupal::logger('PG Guide Queue Import')->info(count($viator_ids) .
      ' attractions were added to fetch their guides');
  }

  public function getData() {
    $ids = $this->prepareData();
    if ($ids) {
      $viator_ids = $this->getViatorIds($ids);
    }
    $request = new HTTP_Request2();
    $url = PGVendorApiGuideManager::API_URL . $this->settings->get('api_key');
    $request->setUrl($url);
    $request->setMethod(HTTP_Request2::METHOD_POST);
    $request->setConfig([
      'follow_redirects' => TRUE,
    ]);
    $request->setHeader([
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
    ]);
    foreach ($viator_ids as $viator_id) {
      $request->setBody('{"seoId": ' . $viator_id . ' }');
      try {
        $response = $request->send();
        if ($response->getStatus() == 200) {
          $current_data = json_decode($response->getBody())->data;
          foreach ($current_data as $attraction_data) {
            $attraction_data->seoId = $viator_id;
          }
          $vendorData[] = $current_data;
        }
        else {
          Drupal::logger('PG Guide Import')
            ->error('Unexpected HTTP status: ' . $response->getStatus() . ' ' .
              $response->getReasonPhrase());
        }
      } catch (HTTP_Request2_Exception $e) {
        Drupal::logger('PG Guide Import')->error('Error: ' . $e->getMessage());
      }
    }
    $this->vendorData = $vendorData;
    return $this;
  }

  public function prepareData($restriction = 1000) {
    $languages = array_keys(Drupal::languageManager()->getLanguages());
    $checked = [];
    foreach ($languages as $current_langcode) {
      if ($restriction){
        $ids = Drupal::entityQuery('pg_attraction')
          ->condition('langcode', $current_langcode)
          ->condition('status', 1)
          ->range(0, $restriction)
          ->execute();
      }
      if (!$restriction){
        $ids = Drupal::entityQuery('pg_attraction')
          ->condition('langcode', $current_langcode)
          ->condition('status', 1)
          ->execute();
      }
      foreach ($ids as $id) {
        if (!in_array($id, $checked)) {
          $checked[] = $id;
        }
      }
    }
    if ($checked != []) {
      return $checked;
    }
    return FALSE;
  }

  public function getViatorIds(array $checked) {
    $viator_ids_to_update = [];
    foreach ($checked as $checked_id) {
      $entity = Drupal::entityTypeManager()->getStorage('pg_attraction')
        ->load($checked_id);
      if ($entity && $entity->hasField('viator_id')) {
        $field = $entity->get('viator_id');
      }
      if ($field && $field->getValue()) {
        $viator_id = $field->getValue()[0]['value'];
      }
      if ($viator_id) {
        $viator_ids_to_update[] = $viator_id;
      }
    }
    if ($viator_ids_to_update != []) {
      return $viator_ids_to_update;
    }
    return FALSE;
  }

  public function enableVendor() {
    if (!empty($this->settings->get('api_key'))) {
      return TRUE;
    }
    else {
      return t('API key not configured');
    }
  }

  public function saveSettings($api_key) {

  }

  public function settingsURL() {
    return Url::fromRoute('pg_vendor_api_guide.config');
  }

  public function endpointInfo() {
    return [
      'url' => PGVendorApiGuideManager::API_URL,
      'params' => [
        'API-KEY' => $this->settings->get('api_key'),
      ],
    ];
  }

}
