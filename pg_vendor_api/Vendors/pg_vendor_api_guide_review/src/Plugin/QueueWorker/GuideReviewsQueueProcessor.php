<?php


namespace Drupal\pg_vendor_api_guide_review\Plugin\QueueWorker;

use Drupal\pg_guide_entities\Classes\GuideReviewsStorage;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pg_vendor_api_guide_review\Classes\PGVendorApiGuideReviewManager;
use HTTP_Request2;

/**
 * Processes Tasks for getting the quides that needs to be updated.
 *
 * @QueueWorker(
 *   id = "guide_reviews_processor_queue",
 *   title = @Translation("Fetcher for guide reviews that needs to be updated")
 * )
 */
class GuideReviewsQueueProcessor extends QueueWorkerBase {

  public function processItem($viator_code) {
    $settings = \Drupal::service('config.factory')->getEditable('pg_vendor_api_guide_review.settings');
    $request = new HTTP_Request2();
    $url = PGVendorApiGuideReviewManager::API_URL . $settings->get('api_key');
    $request->setMethod(HTTP_Request2::METHOD_GET);
    $request->setConfig(array(
      'follow_redirects' => TRUE
    ));
    $request->setHeader(array(
      'Accept' => 'application/json'
    ));
      $request->setUrl($url . '&code=' . $viator_code . '&showUnavailable=false');
      try {
        $response = $request->send();
        if ($response->getStatus() == 200) {
          $current_data = json_decode($response->getBody())->data;
          $vendorData[] = $current_data;
        } else {
          \Drupal::logger('pg_vendor_api')
            ->error('Unexpected HTTP status: ' . $response->getStatus() . ' ' .
              $response->getReasonPhrase());
        }
      } catch (HTTP_Request2_Exception $e) {
        \Drupal::logger('pg_vendor_api')->error('Error: ' . $e->getMessage());
      }
      if ($vendorData && !empty($vendorData[0])) {
        foreach ($vendorData as $reviews) {
          $vendorData = GuideReviewsStorage::checkGuideReviews($reviews);
          foreach ($reviews as $review){
            GuideReviewsStorage::saveGuideReviewFromData($review);
          }
        }
      }
  }

}
