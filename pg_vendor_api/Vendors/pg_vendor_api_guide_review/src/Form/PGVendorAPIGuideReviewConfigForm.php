<?php

/**
 * @file
 * Contains \Drupal\pg_vendor_api_guide_review\Form\PGVendorAPIGuideReviewConfigForm.
 */

namespace Drupal\pg_vendor_api_guide_review\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\pg_vendor_api_guide_review\Classes\PGVendorApiGuideReviewManager;
use Symfony\Component\DependencyInjection\ContainerInterface;


class PGVendorAPIGuideReviewConfigForm extends ConfigFormBase {

  protected $manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, PGVendorApiGuideReviewManager $pg_vendor_api_manager) {
    parent::__construct($config_factory);
    $this->manager = $pg_vendor_api_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('pg_vendor_api_guide_review.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['pg_vendor_api_guide_review.settings'];
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'pg_vendor_api_guide_review_config_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pg_vendor_api_guide_review.settings');


    $form['wrapper']['api_key'] = [
      '#type' => 'textfield',
      '#title' => 'API key',
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('pg_vendor_api_guide_review.settings');

    $config->set('api_key', $form_state->getValue('api_key'));

    $config->save();

    drupal_set_message(t('Configuration saved.'));

    $url = Url::fromRoute('pg_vendor_api.config');
    $form_state->setRedirectUrl($url);
  }

}
