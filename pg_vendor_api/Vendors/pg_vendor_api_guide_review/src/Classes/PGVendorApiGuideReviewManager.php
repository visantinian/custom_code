<?php

namespace Drupal\pg_vendor_api_guide_review\Classes;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\pg_vendor_api\Classes\PGVendorApiInterface;
use Drupal\pg_vendor_data_entity\Entity\PGVendorDataEntity;
use HTTP_Request2;

class PGVendorApiGuideReviewManager implements PGVendorApiInterface
{

    protected $vendorData = [];

    const API_URL = 'https://viatorapi.viator.com/service/product/reviews?apiKey=';

    protected $entityManager;

    protected $client;

    protected $token;

    public function __construct(ConfigFactoryInterface $settings, EntityTypeManagerInterface $entity_manager)
    {
        $this->settings = $settings->getEditable('pg_vendor_api_guide_review.settings');
        $this->entityManager = $entity_manager;
    }

    public function vendorDescription()
    {
        return $this->vendorTitle() . ' data from http://viator.com';
    }

    public function vendorTitle()
    {
        return 'Guide Review';
    }

    public function vendorData()
    {
        return $this->vendorData;
    }

    public function prepareTestData()
    {
        return htmlspecialchars($this->vendorData);
    }

  public function prepareQueue() {
    $queue = \Drupal::queue('guide_reviews_processor_queue');
    $queue->createQueue();
    $ids = $this->prepareData();
    if ($ids) {
      $viator_ids = $this->getViatorProductIds($ids);
    }
    foreach ($viator_ids as $viator_id){
      $queue->createItem($viator_id);
    }
    Drupal::logger('PG Guide  Reviews Queue Import')->info(count($viator_ids) .
      ' items were added to guides_process_queue');
  }

    public function fetchData($requestID)
    {
        return $this->getData()->saveData($requestID);
    }

    public function saveData($requestID)
    {
        if ($this->vendorData) {
            $data = PGVendorDataEntity::create([
            'vendor_id' => $this->vendorID(),
            'vendor_name' => $this->vendorTitle(),
            'pg_request_id' => $requestID,
            'data' => serialize($this->vendorData),
            ]);
            $data->save();
            return $data->id();
        }
        return 0;
    }

    public function vendorID()
    {
        return 'pg_guide_review_api';
    }

    public function getData()
    {
        $ids = $this->prepareData();
        if ($ids) {
            $viator_codes = $this->getViatorProductIds($ids);
        }
        $request = new HTTP_Request2();
        $url = self::API_URL . $this->settings->get('api_key');
        $request->setMethod(HTTP_Request2::METHOD_GET);
        $request->setConfig(array(
        'follow_redirects' => TRUE
        ));
        $request->setHeader(array(
        'Accept' => 'application/json'
        ));
        foreach ($viator_codes as $viator_code) {
          $request->setUrl($url . '&code=' . $viator_code . '&showUnavailable=false');
            try {
                $response = $request->send();
                if ($response->getStatus() == 200) {
                    $current_data = json_decode($response->getBody())->data;
                    $vendorData[] = $current_data;
                } else {
                    Drupal::logger('pg_vendor_api')
                    ->error('Unexpected HTTP status: ' . $response->getStatus() . ' ' .
                      $response->getReasonPhrase());
                }
            } catch (HTTP_Request2_Exception $e) {
                Drupal::logger('pg_vendor_api')->error('Error: ' . $e->getMessage());
            }
        }
        $this->vendorData = $vendorData;
        return $this;
    }

    public function prepareData()
    {
        return \Drupal::entityQuery('pg_guide')->execute();
    }

    public function getViatorProductIds(array $guides)
    {
        $viator_codes_to_update = [];
        foreach ($guides as $guide_id) {
            $entity = Drupal::entityTypeManager()->getStorage('pg_guide')
            ->load($guide_id);
            if ($entity && $entity->hasField('viator_code')) {
                $field = $entity->get('viator_code');
            }
            if ($field && $field->getValue()) {
                $viator_code = $field->getValue()[0]['value'];
            }
            if ($viator_code) {
                $viator_codes_to_update[] = $viator_code;
            }
        }
        if ($viator_codes_to_update != []) {
            return $viator_codes_to_update;
        }
        return false;
    }

    public function enableVendor()
    {
        if (!empty($this->settings->get('api_key'))) {
            return true;
        } else {
            return t('API key not configured');
        }
    }

    public function saveSettings($api_key)
    {
    }

    public function settingsURL()
    {
        return Url::fromRoute('pg_vendor_api_guide_review.config');
    }

    public function endpointInfo()
    {
        return [
        'url' => self::API_URL,
        'params' => [
        'API-KEY' => $this->settings->get('api_key'),
        ],
        ];
    }
}
