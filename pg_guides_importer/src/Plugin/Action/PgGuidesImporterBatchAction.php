<?php


namespace Drupal\pg_guides_importer\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Pg attractions guide updater action for vbo module.
 *
 * @Action(
 *   id = "pg_guides_importer_batch_importer_action",
 *   label = @Translation("Some action label"),
 *   type = "pg_attraction",
 *   confirm = TRUE,
 *   requirements = {
 *     "_permission" = "run global update"
 *   },
 * )
 */
class PgGuidesImporterBatchImporterAction  extends ViewsBulkOperationsActionBase {

  /**
   * MessengerInterface definition.
   *
   * @var MessengerInterface
   */
  protected $messenger;

  /**
   * Batch Builder.
   *
   * @var BatchBuilder
   */
  protected $batchBuilder;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new Translation Updater Form object.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $viator_seoId = $entity->get('viator_id')->getValue[0]['value'];
    // Do some processing..


    // Don't return anything for a default completion message, otherwise return translatable markup.
    return $this->t('Some result');
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }
}
