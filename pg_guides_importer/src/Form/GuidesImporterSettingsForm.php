<?php

namespace Drupal\pg_update_translations\Form;

use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

class GuidesImporterSettingsForm extends FormBase {

  /**
   * MessengerInterface definition.
   *
   * @var MessengerInterface
   */
  protected $messenger;

  /**
   * Batch Builder.
   *
   * @var BatchBuilder
   */
  protected $batchBuilder;

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new Translation Updater Form object.
   */
  public function __construct(
    MessengerInterface $messenger,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager
  ) {
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
    $this->batchBuilder = new BatchBuilder();
    $this->languageManager = $language_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pg_guides_importer_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['entities_to_update'] = [
      '#type' => 'select',
      '#title' => $this->t('Custom Entity Types that needs to be updated'),
      '#header' => [
        'label' => $this->t('Entity Label'),
      ],
      '#options' => $this->gatherEntityTypes(),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run batch'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Helper function to get list of all custom entities on the subsite.
   *
   * @return array
   */
  public function gatherEntityTypes() {
    $entity_types = [];
    $current_entities = array_keys($this->entityTypeManager->getDefinitions());
    foreach($current_entities as $entity_type_id){
      if (
        preg_match('/pg_.*>?/m', $entity_type_id) > 0
      ) {
        $entity_types[] =  $entity_type_id;
      }
    }
    return $entity_types;
  }

  /**
   * Helper function that filters all fields on the entity type, to return
   * only those which have type "entity_revision_reference"
   *
   * @param $entity_types - List of entity_types prepared by $this->gatherEntityTypes()
   *
   * @return array
   */
  public function gatherEntityTypesFields($entity_type) {
    $list_of_fields = [];
    $list_of_unprepared_fields = $this->entityFieldManager->getFieldStorageDefinitions($entity_type, NULL);
    foreach ($list_of_unprepared_fields as $prepared_field) {
      $property_definitions = $prepared_field->getPropertyDefinitions();
      foreach ($property_definitions as $property_definition){
        if ($property_definition->getDataType() == "entity_revision_reference") {
          $list_of_fields[] = $prepared_field; // FIELD OBJECT
        }
      }
    }
    return $list_of_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_option = $form_state->getValue('entity_types_to_update');
    $entity_type = $form['entity_types_to_update']['#options'][$selected_option];

    $selected_fields = $this->gatherEntityTypesFields($entity_type);
    $additional_langcodes = $this->getAdditionalLangcodes();
    if ($entity_type !== '') {
      foreach ($selected_fields as $field_key => $selected_field){
        foreach ($additional_langcodes as $langcode){
          $entities_to_update = $this->getEids($entity_type, $langcode);
          foreach($entities_to_update as $eid_to_update){
            $entities[] = [
              'eid' => $eid_to_update,
              'field' => $selected_field->get('field_name'),
              'langcode' => $langcode,
              'entity_type' => $entity_type,
            ];
          }
        }
      }
    }
    $this->batchBuilder
      ->setTitle($this->t('Updating empty translations'))
      ->setInitMessage($this->t('Initializing.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('An error has occurred.'));

    $this->batchBuilder->setFile(
      drupal_get_path('module', 'pg_update_translations') . '/src/Form/TranslationUpdaterForm.php');
    $this->batchBuilder->addOperation([$this, 'processItems'], [$entities]);
    $this->batchBuilder->setFinishCallback([$this, 'finished']);

    batch_set($this->batchBuilder->toArray());
  }

  /**
   * Processor for batch operations.
   */
  public function processItems($items, array &$context) {
    // Elements per operation.
    $limit = 10;
    $default_language = \Drupal::languageManager()->getDefaultLanguage()->getId();

    // Set default progress values.
    if (empty($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($items);
    }

    // Save items to array which will be changed during processing.

    if (empty($context['sandbox']['items'])) {
      $context['sandbox']['items'] = $items;
    }

    $counter = 0;
    if (!empty($context['sandbox']['items'])) {
      // Remove already processed items.
      if ($context['sandbox']['progress'] != 0) {
        array_splice($context['sandbox']['items'], 0, $limit);
      }

      foreach ($context['sandbox']['items'] as $item) {
        if ($counter != $limit) {
          $this->processItem($item, $default_language);

          $counter++;
          $context['sandbox']['progress']++;

          $context['message'] = $this->t('Now processing node :progress of :count', [
            ':progress' => $context['sandbox']['progress'],
            ':count' => $context['sandbox']['max'],
          ]);

          // Increment total processed item values. Will be used in finished
          // callback.
          $context['results']['processed'] = $context['sandbox']['progress'];
        }
      }
    }

    // If not finished all tasks, we count percentage of process. 1 = 100%.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Finding additional languages
   */
  public function getAdditionalLangcodes() {
    $langcodes = array_keys(\Drupal::languageManager()->getLanguages());
    $default_language = \Drupal::languageManager()->getDefaultLanguage()->getId();
    $default_lc_pos = array_search($default_language, $langcodes);
    unset($langcodes[$default_lc_pos]);
    return $langcodes;
  }

  /**
   * Process single item.
   */
  public function processItem($item, $default_language) {
    // load entity
    $entity = $this->entityTypeManager->getStorage($item['entity_type'])
      ->load($item['eid']);
    //set default value
    if ($entity){
      if (!$entity->hasTranslation($default_language)){
        $definitions = array_keys($entity->getFieldDefinitions());
        $values = [];
        foreach ($definitions as $definition){
          $values[$definition] = $entity->get($definition)->getValue();
        }
        $entity->addTranslation($default_language, $values)->save();
      }
      $default_value = $entity->getTranslation($default_language)->get($item['field'])->getValue();
      if ($default_value == []) {
        $translation_source = $entity->getTranslation('en')->get($item['field'])->getValue();
        $translation = $entity->getTranslation($default_language);
        if ($translation->get($item['field'])->getValue == []){
          $translation->set($item['field'], $translation_source)->save();
        }
      } else {
        $translation = $entity->getTranslation($item['langcode']);
        if ($translation->get($item['field'])->getValue() == []) {
          $translation->set($item['field'], $default_value)->save();
        }
      }
      if ($default_value == NULL) {
        $translation_source = $entity->getTranslation('en')->get($item['field'])->getValue();
        $translation = $entity->getTranslation($default_language);
        if ($translation->get($item['field']) == []){
          $translation->set($item['field'], $translation_source)->save();
        }
      }
    }
  }

  /**
   * Finished callback for batch.
   */
  public function finished($success, $results, $operations) {
    $message = $this->t('Number of custom entities affected by batch: @count', [
      '@count' => $results['processed'],
    ]);
    $this->messenger()
      ->addStatus($message);
  }

  /**
   * Load all eids for specific type.
   */
  public function getEids($type, $langcode) {
    $storage = $this->entityTypeManager->getStorage($type);
    $query = $storage
      ->getQuery()
      ->condition('langcode', $langcode);
    return array_values($query->execute());
  }

}
