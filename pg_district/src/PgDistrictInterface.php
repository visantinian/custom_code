<?php

namespace Drupal\pg_district;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a district entity type.
 */
interface PgDistrictInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the district title.
   *
   * @return string
   *   Title of the district.
   */
  public function getTitle();

  /**
   * Sets the district title.
   *
   * @param string $title
   *   The district title.
   *
   * @return \Drupal\pg_district\PgDistrictInterface
   *   The called district entity.
   */
  public function setTitle($title);

  /**
   * Gets the district creation timestamp.
   *
   * @return int
   *   Creation timestamp of the district.
   */
  public function getCreatedTime();

  /**
   * Sets the district creation timestamp.
   *
   * @param int $timestamp
   *   The district creation timestamp.
   *
   * @return \Drupal\pg_district\PgDistrictInterface
   *   The called district entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the district status.
   *
   * @return bool
   *   TRUE if the district is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the district status.
   *
   * @param bool $status
   *   TRUE to enable this district, FALSE to disable.
   *
   * @return \Drupal\pg_district\PgDistrictInterface
   *   The called district entity.
   */
  public function setStatus($status);

}
