<?php

namespace Drupal\pg_district;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the district entity type.
 */
class PgDistrictAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        if (!$entity->isEnabled()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished district');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published district');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit district', 'administer district'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete district', 'administer district'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create district', 'administer district'], 'OR');
  }

}
