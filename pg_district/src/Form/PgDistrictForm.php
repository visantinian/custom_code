<?php

namespace Drupal\pg_district\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the district entity edit forms.
 */
class PgDistrictForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New district %label has been created.', $message_arguments));
      $this->logger('pg_district')->notice('Created new district %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The district %label has been updated.', $message_arguments));
      $this->logger('pg_district')->notice('Updated new district %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.pg_district.canonical', ['pg_district' => $entity->id()]);
  }

}
